Function duni()

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 random number generator based on the universal random number
! generator of marsaglia, zaman and tsang
! (stats and prob. lett. 8 (1990) 35-39.)
!
! It must be called once to initialise parameters u,c,cd,cm
!
! copyright - daresbury laboratory
! author    - w.smith july 1992
! amended   - i.t.todorov august 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  Use kinds_f90
  use comms_mpi_module, only : idnode

  Implicit None

  Logical,           Save :: newjob
  Data                       newjob /.true./
  Integer,           Save :: ir,jr
  Integer                 :: i,ii,j,jj,k,l,m
  Real( Kind = wp ), Save :: c,cd,cm,u(1:97)
  Real( Kind = wp )       :: s,t,duni

! initialise parameters u,c,cd,cm

  If (newjob) Then
     newjob = .false.

! initial values of i,j,k must be in range 1 to 178 (not all 1)
! initial value of l must be in range 0 to 168

     i =  12
     j =  34
     k =  56
     l =  78

     ir = 97
     jr = 33

     Do ii=1,97

        s = 0.0_wp
        t = 0.5_wp

        Do jj=1,24

           m = Mod(Mod(i*j,179)*k,179)
           i = j
           j = k
           k = m
           l = Mod(53*l+1,169)
           If (Mod(l*m,64) >= 32) s = s+t
           t = 0.5_wp*t

        End Do

        u(ii)=s

     End Do 

     c  =   362436.0_wp/16777216.0_wp
     cd =  7654321.0_wp/16777216.0_wp
     cm = 16777213.0_wp/16777216.0_wp

  End If

! calculate random number

  duni=u(ir)-u(jr)
  If (duni < 0.0_wp) duni = duni + 1.0_wp

  u(ir)=duni

  ir=ir-1
  If (ir == 0) ir = 97

  jr=jr-1
  If (jr == 0) jr = 97

  c = c-cd
  If (c < 0.0_wp) c = c+cm

  duni = duni-c

  If (duni < 0.0_wp) duni = duni + 1.0_wp

End Function duni

