! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module thblist_module

    use kinds_f90

    implicit none

contains

!> Allocates the arrays in a give three-body list. 
subroutine alloc_thblist_arrays(nat, nn, list)

    use thblist_type

    implicit none

        !> The three-body list to allocate
    type(thblist), intent(inout) :: list

        !> The (maximum) number of atoms in the molecule the list is for
    integer, intent(in) :: nat

        !> The maximum number of triplets an atom in the molecule
        !> could have associated with it
    integer, intent(in) :: nn

    list%maxtrip = nn
    allocate(list%numtrip(nat))

    allocate(list%thb_triplets(nat, nn, 5))

    list%numtrip = 0
    list%thb_triplets = 0

end subroutine

subroutine zero_atomthblist(i, list)
    use thblist_type

    implicit none

    type(thblist), intent(inout) :: list
    integer, intent(in) :: i

    list%numtrip(i) = 0
    list%thb_triplets(i,:,:) = 0

end subroutine

end module
