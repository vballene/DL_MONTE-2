!***************************************************************************
!   Copyright (C) 2015 by A.V.Brukhno                                      *
!   andrey.brukhno[@]stfc.ac.uk                                            *
!                                                                          *
!***************************************************************************

!> @brief
!> - Everything related to array manipulation: (re-/de-) allocation, re-sizing, moving bounds etc
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `constants_module, only : uout`

!> @modulefor array manipulation: (re-/de-) allocation, re-sizing etc

module arrays_module

    use kinds_f90
    use constants_module, only : uout

    implicit none

    !AB: making it independent module, no need for constants_module.f90
    !integer, parameter :: uout = 6 ! output file 


!AB: *** GENERIC INTERFACES TO ROUTINES FOR (RE-/DE-) ALLOCATING GIVEN ARRAYS - START ***


  Interface reallocate_1d
     Module Procedure reallocate_1d_int
     Module Procedure reallocate_1d_flt
  End Interface reallocate_1d

  Interface deallocate_1d
     Module Procedure deallocate_1d_int
     Module Procedure deallocate_1d_flt
  End Interface deallocate_1d

  Interface reallocating_1d_ok
     Module Procedure reallocating_1d_int_ok
     Module Procedure reallocating_1d_flt_ok
  End Interface reallocating_1d_ok

  Interface deallocating_1d_ok
     Module Procedure deallocating_1d_int_ok
     Module Procedure deallocating_1d_flt_ok
  End Interface deallocating_1d_ok

  Interface reallocate_2d
     Module Procedure reallocate_2d_int
     Module Procedure reallocate_2d_flt
  End Interface reallocate_2d

  Interface deallocate_2d
     Module Procedure deallocate_2d_int
     Module Procedure deallocate_2d_flt
  End Interface deallocate_2d

  Interface reallocating_2d_ok
     Module Procedure reallocating_2d_int_ok
     Module Procedure reallocating_2d_flt_ok
  End Interface reallocating_2d_ok

  Interface deallocating_2d_ok
     Module Procedure deallocating_2d_int_ok
     Module Procedure deallocating_2d_flt_ok
  End Interface deallocating_2d_ok

!AB: generalisation

  Interface reallocate_quiet
     Module Procedure reallocate_1d_int
     Module Procedure reallocate_1d_flt
     Module Procedure reallocate_2d_int
     Module Procedure reallocate_2d_flt

     !AB: not implemented - needed ???
     !Module Procedure reallocate_3d_int
     !Module Procedure reallocate_3d_flt
  End Interface reallocate_quiet

  Interface deallocate_quiet
     Module Procedure deallocate_1d_int
     Module Procedure deallocate_1d_flt
     Module Procedure deallocate_2d_int
     Module Procedure deallocate_2d_flt

     !AB: not implemented - needed ???
     !Module Procedure deallocate_3d_int
     !Module Procedure deallocate_3d_flt
  End Interface deallocate_quiet

  Interface reallocating_safe
     Module Procedure reallocating_1d_int_ok
     Module Procedure reallocating_1d_flt_ok
     Module Procedure reallocating_2d_int_ok
     Module Procedure reallocating_2d_flt_ok

     !AB: not implemented - needed ???
     !Module Procedure reallocating_3d_int_ok
     !Module Procedure reallocating_3d_flt_ok
  End Interface reallocating_safe

  Interface deallocating_safe
     Module Procedure deallocating_1d_int_ok
     Module Procedure deallocating_1d_flt_ok
     Module Procedure deallocating_2d_int_ok
     Module Procedure deallocating_2d_flt_ok

     !AB: not implemented - needed ???
     !Module Procedure deallocating_3d_int_ok
     !Module Procedure deallocating_3d_flt_ok
  End Interface deallocating_safe


!AB: *** GENERIC INTERFACES TO ROUTINES FOR (RE-/DE-) ALLOCATING GIVEN ARRAYS - END ***


!AB: *** GENERIC INTERFACES TO ROUTINES FOR RESIZING GIVEN ARRAYS AND/OR MOVING BOUNDS - START ***


  Interface resize_upto
     Module Procedure resize_upp1d_int
     Module Procedure resize_upp1d_flt
     Module Procedure resize_upp2d_int
     Module Procedure resize_upp2d_flt

     !AB: not implemented - needed ???
     !Module Procedure resize_upp3d_int
     !Module Procedure resize_upp3d_flt
  End Interface resize_upto

  Interface resize_by
     Module Procedure resize_1d_int_by
     Module Procedure resize_1d_flt_by
     Module Procedure resize_2d_int_by
     Module Procedure resize_2d_flt_by

     !AB: not implemented - needed ???
     !Module Procedure resize_3d_int_by
     !Module Procedure resize_3d_flt_by
  End Interface resize_by

  Interface resize_dim_by
     Module Procedure resize_1d_int_by
     Module Procedure resize_1d_flt_by
     Module Procedure resize_dim2d_int_by
     Module Procedure resize_dim2d_flt_by

     !AB: not implemented - needed ???
     !Module Procedure resize_3d_int_by
     !Module Procedure resize_3d_flt_by
  End Interface resize_dim_by

  Interface resize_dim_to
     Module Procedure resize_dim1d_int
     Module Procedure resize_dim1d_flt
     Module Procedure resize_dim2d_int
     Module Procedure resize_dim2d_flt

     !AB: not implemented - needed ???
     !Module Procedure resize_dim3d_int
     !Module Procedure resize_dim3d_flt
  End Interface resize_dim_to

  !AB: searching in normal/fixed dimension arrays (via assumed size)
  Interface is_val_found_in_dim
     Module Procedure is_int_found_in_1dim
     Module Procedure is_flt_found_in_1dim
  End Interface is_val_found_in_dim

  !AB: alternative for searching in allocatable (pre-allocated) arrays
  Interface is_val_found_in_arr
     Module Procedure is_int_found_in_arr1d
     Module Procedure is_flt_found_in_arr1d
  End Interface is_val_found_in_arr


!AB: *** GENERIC INTERFACES TO ROUTINES FOR RESIZING GIVEN ARRAYS AND/OR MOVING BOUNDS - END ***


contains


!AB: *** GENERIC ROUTINES FOR RESIZING GIVEN ARRAYS AND/OR MOVING BOUNDS - START ***


!> simple checks of routines for (re-)sizing given 1D/2D array while keeping the index lower bound
subroutine resize_checks()

    !AB: arrays debugging

    integer              :: istat,k,ko
    integer, allocatable :: dummy1(:),dummy1c(:)
    integer, allocatable :: dummy2(:,:),dummy2c(:,:)

    logical :: fail(5)

    fail = .false.

    allocate(dummy1(-2:0), stat=istat)
    allocate(dummy1c(-2:0), stat=istat)
    allocate(dummy2(0:2,-2:0), stat=istat)
    allocate(dummy2c(0:2,-2:0), stat=istat)

    do ko=-2,0
       dummy1(ko) = 2*ko + 5
    do k=0,2
       dummy2(k,ko) = 2*ko + k + 10
    end do
    end do

    dummy1c = dummy1
    dummy2c = dummy2

    call resize_dim_to(dummy1,4,istat)
    call resize_dim_to(dummy2,5,4,istat)
    !write(uout,*)

    call resize_by(dummy1,-1,istat)
    call resize_by(dummy2,-2,-1,istat)
    !write(uout,*)

    if( any( dummy1c /= dummy1 ) ) write(*,*)"Error1 (1) - failed to resize dummy1 back correctly!"
    if( any( dummy2c /= dummy2 ) ) write(*,*)"Error1 (2) - failed to resize dummy2 back correctly!"

    call resize_upto(dummy1,4,istat)
    call resize_upto(dummy2,5,4,istat)
    !write(uout,*)

    call resize_dim_by(dummy1,-4,istat)
    call resize_dim_by(dummy2,-3,-4,istat)
    !write(uout,*)

    fail(1) = size(dummy2c,dim=1) /= size(dummy2,dim=1)
    fail(2) = size(dummy2c,dim=2) /= size(dummy2,dim=2)
    fail(3) = size(dummy1c,dim=1) /= size(dummy1,dim=1)

    if( fail(3) ) write(*,*)"Error2 (1) - failed to resize dummy1 back correctly!"

    if( any( fail(:2) ) ) write(*,*)"Error2 (2) - failed to resize dummy2 back correctly!"

    if( any( dummy1 /= dummy1c ) .or. any( dummy1c /= dummy1 ) ) & 
       write(*,*)"Error3 (1) - failed to resize dummy1 back correctly!"

    if( any( dummy2 /= dummy2c ) .or. any( dummy2c /= dummy2 ) ) & 
       write(*,*)"Error3 (2) - failed to resize dummy2 back correctly!"

    !AB: no bugs found up to now

    !...!

end subroutine resize_checks


!> finding out if a given integer is found in a given 1D array (int)
logical function is_int_found_in_1dim(array1d,id,id_pos)

    !use constants_module, only : uout

        !> in-coming array to be searched through
    integer, intent(in) :: array1d(:)

        !> integer id to be found in the in-coming array
    integer, intent(in) :: id

        !> index of the id in the in-coming array if found
    integer, intent(inout) :: id_pos

    integer :: nlower, nupper, i

    nlower = lbound(array1d, dim=1)
    nupper = ubound(array1d, dim=1)

    is_int_found_in_1dim = .false.

    !if( allocated(array1d) ) then

        do i = nlower,nupper

            if( id == array1d(i) ) then

                id_pos = i
                is_int_found_in_1dim = .true.

                return

            end if

        end do

    !else

    !    call cry(uout,'',"ERROR: Trying to search unallocated 1D array (int)",999)

    !end if

end function is_int_found_in_1dim


!> finding out if a given integer is found in a given 1D array (int)
logical function is_flt_found_in_1dim(array1d,id,id_pos)

    !use constants_module, only : uout

        !> in-coming array to be searched through
    real(kind=wp), intent(in) :: array1d(:)

        !> integer id to be found in the in-coming array
    integer, intent(in) :: id

        !> index of the id in the in-coming array if found
    integer, intent(inout) :: id_pos

    integer :: nlower, nupper, i

    nlower = lbound(array1d, dim=1)
    nupper = ubound(array1d, dim=1)

    is_flt_found_in_1dim = .false.

    !if( allocated(array1d) ) then

        do i = nlower,nupper

            if( id == array1d(i) ) then

                id_pos = i
                is_flt_found_in_1dim = .true.

                return

            end if

        end do

    !else

    !    call cry(uout,'',"ERROR: Trying to search unallocated 1D array (int)",999)

    !end if

end function is_flt_found_in_1dim


!> finding out if a given integer is found in a given 1D array (int)
logical function is_int_found_in_arr1d(array1d,id,id_pos)

    !use constants_module, only : uout

        !> in-coming array to be searched through
    integer, allocatable, intent(in) :: array1d(:)

        !> integer id to be found in the in-coming array
    integer, intent(in) :: id

        !> index of the id in the in-coming array if found
    integer, intent(inout) :: id_pos

    integer :: nlower, nupper, i

    nlower = lbound(array1d, dim=1)
    nupper = ubound(array1d, dim=1)

    is_int_found_in_arr1d = .false.

    if( allocated(array1d) ) then

        do i = nlower,nupper

            if( id == array1d(i) ) then

                id_pos = i
                is_int_found_in_arr1d = .true.

                return

            end if

        end do

    else

        call cry(uout,'',"ERROR: Trying to search unallocated 1D array (int)",999)

    end if

end function is_int_found_in_arr1d


!> finding out if a given floating point figure is found in a given 1D array (flt)
logical function is_flt_found_in_arr1d(array1d, flt, flt_pos)

    !use constants_module, only : uout

        !> in-coming array to be searched through
    real(kind=wp), allocatable, intent(in) :: array1d(:)

        !> integer id to be found in the in-coming array
    real(kind=wp), intent(in) :: flt

        !> index of the id in the in-coming array if found
    integer, intent(inout) :: flt_pos

    integer :: nlower, nupper, i

    nlower = lbound(array1d, dim=1)
    nupper = ubound(array1d, dim=1)

    is_flt_found_in_arr1d = .false.

    if( allocated(array1d) ) then

        do i = nlower,nupper

            if( flt == array1d(i) ) then

                flt_pos = i
                is_flt_found_in_arr1d = .true.

                return

            end if

        end do

    else

        call cry(uout,'',"ERROR: Trying to search unallocated 1D array (flt)",999)

    end if

end function is_flt_found_in_arr1d


!> (re-)sizing a given 1D array while keeping the index lower bound - quiet but with possible check
subroutine resize_upp1d_int(array1d, mitems, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    integer, allocatable, intent(inout) :: array1d(:)

        !> requested increment for number of items/elements (moving upper bound!)
    integer, intent(in) :: mitems

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

    !AB: make sure dimensions become 'zero-size' if the upper bound becomes less than the lower bound

    call resize_dim1d_int(array1d, max(size(array1d)+mitems-ubound(array1d,dim=1),0), fail)

    !call resize_dim1d_int(array1d, size(array1d)+inc, fail)

end subroutine resize_upp1d_int


!> (re-)sizing a given 1D array while keeping the index lower bound - quiet but with possible check
subroutine resize_upp1d_flt(array1d, mitems, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    real(kind=wp), allocatable, intent(inout) :: array1d(:)

        !> requested increment for number of items/elements (moving upper bound!)
    integer, intent(in) :: mitems

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

    !AB: make sure dimensions become 'zero-size' if the upper bound becomes less than the lower bound

    call resize_dim1d_flt(array1d, max(size(array1d)+mitems-ubound(array1d,dim=1),0), fail)

    !call resize_dim1d_flt(array1d, size(array1d)+inc, fail)

end subroutine resize_upp1d_flt


!> (re-)sizing a given 2D array while keeping the indices lower bounds - quiet but with possible check
subroutine resize_upp2d_int(array2d, mcols, mrows, fail)

        !> in-coming array to be resized
    integer, allocatable, intent(inout) :: array2d(:,:)

        !> requested (new) numbers of columns & rows \n
        !> NOTE: these are the upper bounds of the indices!
    integer, intent(in) :: mcols, mrows

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

    call resize_2d_int_by(array2d, mcols-ubound(array2d,dim=1), mrows-ubound(array2d,dim=2), fail)

end subroutine resize_upp2d_int


!> (re-)sizing a given 2D array while keeping the indices lower bounds - quiet but with possible check
subroutine resize_upp2d_flt(array2d, mcols, mrows, fail)

        !> in-coming array to be resized
    real(kind=wp), allocatable, intent(inout) :: array2d(:,:)

        !> requested (new) numbers of columns & rows \n
        !> NOTE: these are the upper bounds of the indices!
    integer, intent(in) :: mcols, mrows

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

    call resize_2d_flt_by(array2d, mcols-ubound(array2d,dim=1), mrows-ubound(array2d,dim=2), fail)

end subroutine resize_upp2d_flt


!> (re-)sizing a given 1D array while keeping the index lower bound - quiet but with possible check
subroutine resize_1d_int_by(array1d, inc, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    integer, allocatable, intent(inout) :: array1d(:)

        !> requested increment for number of items/elements (moving upper bound!)
    integer, intent(in) :: inc

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

    !AB: make sure dimensions become 'zero-size' if the upper bound becomes less than the lower bound

    call resize_dim1d_int(array1d, max(size(array1d)+inc,0), fail)

end subroutine resize_1d_int_by


!> (re-)sizing a given 1D array while keeping the index lower bound - quiet but with possible check
subroutine resize_1d_flt_by(array1d, inc, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    real(kind=wp), allocatable, intent(inout) :: array1d(:)

        !> requested increment for number of items/elements (moving upper bound!)
    integer, intent(in) :: inc

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

    !AB: make sure dimensions become 'zero-size' if the upper bound becomes less than the lower bound

    call resize_dim1d_flt(array1d, max(size(array1d)+inc,0), fail)

end subroutine resize_1d_flt_by


!> (re-)sizing a given 2D array while keeping the indices lower bounds - quiet but with possible check
subroutine resize_dim2d_int_by(array2d, icols, irows, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    integer, allocatable, intent(inout) :: array2d(:,:)

        !> requested increments for columns & rows (may be negative) \n
    integer, intent(in) :: icols, irows

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

    !AB: make sure dimensions become 'zero-size' if the upper bound becomes less than the lower bound

    call resize_dim2d_int(array2d, max(size(array2d,dim=1)+icols,0), max(size(array2d,dim=2)+irows,0), fail)

end subroutine resize_dim2d_int_by


!> (re-)sizing a given 2D array while keeping the indices lower bounds - quiet but with possible check
subroutine resize_dim2d_flt_by(array2d, icols, irows, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    real(kind=wp), allocatable, intent(inout) :: array2d(:,:)

        !> requested increments for columns & rows (may be negative) \n
    integer, intent(in) :: icols, irows

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

    !AB: make sure dimensions become 'zero-size' if the upper bound becomes less than the lower bound

    call resize_dim2d_flt(array2d, max(size(array2d,dim=1)+icols,0), max(size(array2d,dim=2)+irows,0), fail)

end subroutine resize_dim2d_flt_by


!> (re-)sizing a given 2D array while keeping the indices lower bounds - quiet but with possible check
subroutine resize_2d_int_by(array2d, icols, irows, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    integer, allocatable, intent(inout) :: array2d(:,:)

        !> requested increments for columns & rows (may be negative) \n
    integer, intent(in) :: icols, irows

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

        ! temporary array to store the current data in the in-coming array to be resized
    integer, allocatable :: array2t(:,:)

        ! current or min sizes of the in-coming & temporary arrays
    integer :: nsize1, nsize2 !, ntemp

        ! current bounds of the in-coming array (to keep the lower bound intact)
    integer :: nlower1, nupper1, nlower2, nupper2, k1, k2

    if( allocated(array2d) ) then
 
        !AB: debugging - remove later on
        !nsize1  = size(array2d, dim=1)
        !nsize2  = size(array2d, dim=2)
        !write(uout,*)
        !write(uout,*)"Requested to resize allocated 2D array (int) [", &
        !             nsize1,nsize2,"] -> [",nsize1+icols,nsize2+irows,"]"

        nlower1 = lbound(array2d,dim=1)
        nupper1 = ubound(array2d,dim=1)

        nlower2 = lbound(array2d,dim=2)
        nupper2 = ubound(array2d,dim=2)

        !AB: if decrementing, minimize the overhead of temporary storage and assingments
        !AB: make sure dimensions become 'zero-size' if the upper bound becomes less than the lower bound
        if( icols < 0 ) nupper1 = max(nupper1+icols,nlower1-1)
        if( irows < 0 ) nupper2 = max(nupper2+irows,nlower2-1)

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Allocating temporary 2D array (int) [", &
        !             nlower1," : ",nupper1," , ",nlower2," : ",nupper2,"]"

        allocate( array2t(nlower1:nupper1, nlower2:nupper2), stat=fail )

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not allocate temporary 2D array (int) !!! - Out of memory ???"
            return
            STOP
        end if

        !AB: let the compiler do the assignment job

        array2t(:,:) = array2d(:nupper1,:nupper2)

        deallocate(array2d)

        !AB: redefine nsize# as new upper bounds (since nupper# can't be reset)
        !AB: dimensions become 'zero-size' if the upper bound becomes less than the lower bound

        nsize1 = max(ubound(array2d,dim=1)+icols,nlower1-1)
        nsize2 = max(ubound(array2d,dim=2)+irows,nlower2-1)

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Allocating newly resized 2D array (int) [", &
        !             nlower1," : ",nsize1," , ",nlower2," : ",nsize2,"]"

        allocate(array2d(nlower1:nsize1,nlower2:nsize2), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate 2D array resized by addition (int) !!! - Out of memory ???"
            return
            STOP
        end if

        do k2=nlower2,nupper2

            !AB: let the compiler do the assignment job

            array2d(:nupper1,k2) = array2t(:,k2)

            !AB: if incrementing, pad the added columns with zeros
            if( icols > 0 ) array2d(nupper1+1:,k2) = 0

        end do

        deallocate(array2t)

        !AB: if incrementing, pad the added rows with zeros

        !AB: let the compiler do the assignment job
        if( irows > 0 ) array2d(:,nupper2+1:) = 0

    else ! .not. allocated(array2d)

        write(uout,*)
        write(uout,*)"Requested to resize by addition unallocated 2D array (int) - allocating from scratch !!!"

        allocate(array2d(icols,irows), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not allocate unallocated 2D array resized by addition (int) !!! - Out of memory ???"
            return
            STOP
        end if

        array2d = 0

    end if ! allocated(array2d) - ?

end subroutine resize_2d_int_by


!> (re-)sizing a given 2D array while keeping the indices lower bounds - quiet but with possible check
subroutine resize_2d_flt_by(array2d, icols, irows, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    real(kind=wp), allocatable, intent(inout) :: array2d(:,:)

        !> requested increments for columns & rows (may be negative) \n
    integer, intent(in) :: icols, irows

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

        ! temporary array to store the current data in the in-coming array to be resized
    real(kind=wp), allocatable :: array2t(:,:)

        ! current or min sizes of the in-coming & temporary arrays
    integer :: nsize1, nsize2 !, ntemp

        ! current bounds of the in-coming array (to keep the lower bound intact)
    integer :: nlower1, nupper1, nlower2, nupper2, k1, k2

    if( allocated(array2d) ) then
 
        !AB: debugging - remove later on
        !nsize1  = size(array2d, dim=1)
        !nsize2  = size(array2d, dim=2)
        !write(uout,*)
        !write(uout,*)"Requested to resize allocated 2D array (flt) [", &
        !             nsize1,nsize2,"] -> [",nsize1+icols,nsize2+irows,"]"

        nlower1 = lbound(array2d,dim=1)
        nupper1 = ubound(array2d,dim=1)

        nlower2 = lbound(array2d,dim=2)
        nupper2 = ubound(array2d,dim=2)

        !AB: if decrementing, minimize the overhead of temporary storage and assingments
        !AB: make sure dimensions become 'zero-size' if the upper bound becomes less than the lower bound
        if( icols < 0 ) nupper1 = max(nupper1+icols,nlower1-1)
        if( irows < 0 ) nupper2 = max(nupper2+irows,nlower2-1)

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Allocating temporary 2D array (flt) [", &
        !             nlower1," : ",nupper1," , ",nlower2," : ",nupper2,"]"

        allocate( array2t(nlower1:nupper1, nlower2:nupper2), stat=fail )

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not allocate temporary 2D array (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        !AB: let the compiler do the assignment job

        array2t(:,:) = array2d(:nupper1,:nupper2)

        deallocate(array2d)

        !AB: redefine nsize# as new upper bounds (since nupper# can't be reset)
        !AB: dimensions become 'zero-size' if the upper bound becomes less than the lower bound

        nsize1 = max(ubound(array2d,dim=1)+icols,nlower1-1)
        nsize2 = max(ubound(array2d,dim=2)+irows,nlower2-1)

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Allocating newly resized 2D array (flt) [", &
        !             nlower1," : ",nsize1," , ",nlower2," : ",nsize2,"]"

        allocate(array2d(nlower1:nsize1,nlower2:nsize2), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate 2D array resized by addition (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        do k2=nlower2,nupper2

            !AB: let the compiler do the assignment job

            array2d(:nupper1,k2) = array2t(:,k2)

            !AB: if incrementing, pad the added columns with zeros
            if( icols > 0 ) array2d(nupper1+1:,k2) = 0

        end do

        deallocate(array2t)

        !AB: if incrementing, pad the added rows with zeros

        !AB: let the compiler do the assignment job
        if( irows > 0 ) array2d(:,nupper2+1:) = 0

    else ! .not. allocated(array2d)

        write(uout,*)
        write(uout,*)"Requested to resize by addition unallocated 2D array (flt) - allocating from scratch !!!"

        allocate(array2d(icols,irows), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not allocate unallocated 2D array resized by addition (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        array2d = 0

    end if ! allocated(array2d) - ?

end subroutine resize_2d_flt_by


!> (re-)sizing a given 2D array while keeping the indices lower bounds - quiet but with possible check
subroutine resize_dim2d_int(array2d, ncols, nrows, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    integer, allocatable, intent(inout) :: array2d(:,:)

        !> requested (new) numbers of columns & rows \n
        !> NOTE: these are the array absolute dimensions, not the upper bounds of the indices!
    integer, intent(in) :: ncols, nrows

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

        ! temporary array to store the current data in the in-coming array to be resized
    integer, allocatable :: array2t(:,:)

        ! current or min sizes of the in-coming & temporary arrays
    integer :: nsize1, nsize2, ntemp

        ! current bounds of the in-coming array (to keep the lower bound intact)
    integer :: nlower1, nupper1, nlower2, nupper2, k1, k2

    if( allocated(array2d) ) then 

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Requested to resize allocated 2D array (int) [", &
        !             size(array2d, dim=1),size(array2d, dim=2),"] -> [",ncols,nrows,"]"

        nsize1  = min(size(array2d, dim=1),ncols)
        nsize2  = min(size(array2d, dim=2),nrows)

        nlower1 = lbound(array2d, dim=1)
        nupper1 = nlower1+nsize1-1

        nlower2 = lbound(array2d, dim=2)
        nupper2 = nlower2+nsize2-1

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Allocating temporary 2D array (int) [", &
        !             nlower1," : ",nupper1," , ",nlower2," : ",nupper2,"]"

        allocate( array2t(nlower1:nupper1, nlower2:nupper2), stat=fail )

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not allocate temporary 2D array (int) !!! - Out of memory ???"
            return
            STOP
        end if

        !AB: let the compiler do the assignment job

        array2t = array2d(:nupper1,:nupper2)

        deallocate(array2d)

        !AB: redefine nsize# as new upper bounds (since nupper# can't be reset)
        !AB: dimensions become 'zero-size' if the upper bound becomes less than the lower bound (ncols=0 or nrows=0)

        nsize1 = nlower1+ncols-1
        nsize2 = nlower2+nrows-1

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Allocating newly resized 2D array (int) [", &
        !             nlower1," : ",nsize1," , ",nlower2," : ",nsize2,"]"

        allocate(array2d(nlower1:nsize1,nlower2:nsize2), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate resized 2D array (int) !!! - Out of memory ???"
            return
            STOP
        end if

        nsize1 = nupper1-nlower1+1

        do k2=nlower2,nupper2

            !AB: let the compiler do the assignment job

            array2d(:nupper1,k2) = array2t(:,k2)

            !AB: if incrementing, pad the added columns with zeros
            if( ncols > nsize1 ) array2d(nupper1+1:,k2) = 0

        end do

        deallocate(array2t)

        !AB: if incrementing, pad the added rows with zeros

        !AB: let the compiler do the assignment job
        if( nrows > nupper2-nlower2+1 ) array2d(:,nupper2+1:) = 0

    else ! .not. allocated(array2d)

        write(uout,*)
        write(uout,*)"Requested to resize unallocated 2D array (int) - allocating from scratch !!!"

        allocate(array2d(ncols,nrows), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate resized 2D array (int) !!! - Out of memory ???"
            return
            STOP
        end if

        array2d = 0

    end if ! allocated(array2d) - ?

end subroutine resize_dim2d_int


!> (re-)sizing a given 2D array while keeping the indices lower bounds - quiet but with possible check
subroutine resize_dim2d_flt(array2d, ncols, nrows, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    real(kind=wp), allocatable, intent(inout) :: array2d(:,:)

        !> requested new numbers of columns & rows \n
        !> NOTE: these are the array absolute dimensions, not the upper bounds of the indices!
    integer, intent(in) :: ncols, nrows

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

        ! temporary array to store the current data in the in-coming array to be resized
    real(kind=wp), allocatable :: array2t(:,:)

        ! current or min sizes of the in-coming & temporary arrays
    integer :: nsize1, nsize2, ntemp

        ! current bounds of the in-coming array (to keep the lower bound intact)
    integer :: nlower1, nupper1, nlower2, nupper2, k1, k2

    if( allocated(array2d) ) then 

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Requested to resize allocated 2D array (flt) [", &
        !             size(array2d, dim=1),size(array2d, dim=2),"] -> [",ncols,nrows,"]"

        nsize1  = min(size(array2d, dim=1),ncols)
        nsize2  = min(size(array2d, dim=2),nrows)

        nlower1 = lbound(array2d, dim=1)
        nupper1 = nlower1+nsize1-1

        nlower2 = lbound(array2d, dim=2)
        nupper2 = nlower2+nsize2-1

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Allocating temporary 2D array (flt) [", &
        !             nlower1," : ",nupper1," , ",nlower2," : ",nupper2,"]"

        allocate( array2t(nlower1:nupper1, nlower2:nupper2), stat=fail )

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not allocate temporary 2D array (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        !AB: let the compiler do the assignment job

        array2t = array2d(:nupper1,:nupper2)

        deallocate(array2d)

        nsize1 = nlower1+ncols-1
        nsize2 = nlower2+nrows-1

        !AB: debugging - remove later on
        !write(uout,*)
        !write(uout,*)"Allocating newly resized 2D array (flt) [", &
        !             nlower1," : ",nsize1," , ",nlower2," : ",nsize2,"]"

        allocate(array2d(nlower1:nsize1,nlower2:nsize2), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate resized 2D array (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        nsize1 = nupper1-nlower1+1

        do k2=nlower2,nupper2

            !AB: let the compiler do the assignment job

            array2d(:nupper1,k2) = array2t(:,k2)

            !AB: if incrementing, pad the added columns with zeros
            if( ncols > nsize1 ) array2d(nupper1+1:,k2) = 0

        end do

        deallocate(array2t)

        !AB: if incrementing, pad the added rows with zeros

        !AB: let the compiler do the assignment job
        if( nrows > nupper2-nlower2+1 ) array2d(:,nupper2+1:) = 0

    else ! .not. allocated(array2d)

        write(uout,*)
        write(uout,*)"Requested to resize unallocated 2D array (flt) - allocating from scratch !!!"

        allocate(array2d(ncols,nrows), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate resized 2D array (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        array2d = 0

    end if ! allocated(array2d) - ?

end subroutine resize_dim2d_flt


!> (re-)sizing a given 1D array while keeping the index lower bound - quiet but with possible check
subroutine resize_dim1d_int(array1d, nitems, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    integer, allocatable, intent(inout) :: array1d(:)

        !> requested new number of items/elements \n
        !> NOTE: these is the array absolute dimension, not the index upper bound!
    integer, intent(in) :: nitems

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

        ! temporary array to store the current data in the in-coming array to be resized
    integer, allocatable :: array1t(:)

        ! current or min size of the in-coming & temporary arrays
    integer :: ntemp

        ! current bounds of the in-coming array (to keep the lower bound intact)
    integer :: nlower, nupper, k
    
    if( allocated(array1d) ) then 

        ntemp  = min(size(array1d),nitems)

        nlower = lbound(array1d, dim=1)
        nupper = nlower+ntemp-1

        allocate(array1t(nlower:nupper), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not allocate temporary 1D array (int) !!! - Out of memory ???"
            return
            STOP
        end if

        !AB: let the compiler do the assignment job

        array1t(:) = array1d(:nupper)

        deallocate(array1d)

        allocate(array1d(nlower:(nlower+nitems-1)), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate resized 1D array (int) !!! - Out of memory ???"
            return
            STOP
        end if

        !AB: let the compiler do the assignment job

        array1d(:nupper) = array1t(:)

        deallocate(array1t)

        !AB: if incrementing, pad with zeros

        !AB: let the compiler do the assignment job

        if( nitems > nupper-nlower+1 ) array1d(nupper+1:) = 0

    else ! .not. allocated(array1d)

        write(uout,*)
        write(uout,*)"Requested to resize unallocated 1D array (int) - allocating from scratch !!!"

        allocate(array1d(nitems), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate resized 1D array (int) !!! - Out of memory ???"
            return
            STOP
        end if

        array1d = 0

    end if ! allocated(array1d) - ?

end subroutine resize_dim1d_int


!> (re-)sizing a given 1D array while keeping the index lower bound - quiet but with possible check
subroutine resize_dim1d_flt(array1d, nitems, fail)

    !use constants_module, only : uout

        !> in-coming array to be resized
    real(kind=wp), allocatable, intent(inout) :: array1d(:)

        !> requested new number of items/elements \n
        !> NOTE: these is the array absolute dimension, not the index upper bound!
    integer, intent(in) :: nitems

        !> fail(>0)/success(0) flag for external checks
    integer, intent(inout) :: fail

        ! temporary array to store the current data in the in-coming array to be resized
    real(kind=wp), allocatable :: array1t(:)

        ! current or min size of the in-coming & temporary arrays
    integer :: ntemp

        ! current bounds of the in-coming array (to keep the lower bound intact)
    integer :: nlower, nupper, k
    
    if( allocated(array1d) ) then 

        ntemp  = min(size(array1d),nitems)

        nlower = lbound(array1d, dim=1)
        nupper = nlower+ntemp-1

        allocate(array1t(nlower:nupper), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not allocate temporary 1D array (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        !AB: let the compiler do the assignment job

        array1t(:) = array1d(:nupper)

        deallocate(array1d)

        allocate(array1d(nlower:(nlower+nitems-1)), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate resized 1D array (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        !AB: let the compiler do the assignment job

        array1d(:nupper) = array1t(:)

        deallocate(array1t)

        !AB: if incrementing, pad with zeros

        !AB: let the compiler do the assignment job

        if( nitems > nupper-nlower+1 ) array1d(nupper+1:) = 0

    else ! .not. allocated(array1d)

        write(uout,*)
        write(uout,*)"Requested to resize unallocated 1D array (flt) - allocating from scratch !!!"

        allocate(array1d(nitems), stat=fail)

        !if( fail > 0 ) return
        if( fail > 0 ) then
            write(uout,*)
            write(uout,*)"Could not (re-)allocate resized 1D array (flt) !!! - Out of memory ???"
            return
            STOP
        end if

        array1d = 0

    end if ! allocated(array1d) - ?

end subroutine resize_dim1d_flt


!AB: *** GENERIC ROUTINES FOR RESIZING GIVEN ARRAYS AND/OR MOVING BOUNDS - END ***


!AB: *** GENERIC ROUTINES FOR (RE-/DE-) ALLOCATING GIVEN ARRAYS VIA INTERFACE - START ***


!> (re-)allocating a given 1D array - quiet but with possible check
subroutine reallocate_1d_int(array1d, nitems, fail)

    !use constants_module, only : uout

    integer, allocatable, intent(inout) :: array1d(:)

    integer, intent(in) :: nitems

    integer, intent(inout) :: fail

    call deallocate_quiet(array1d)

    fail = 0

    allocate( array1d(nitems), stat = fail )

end subroutine reallocate_1d_int


!> deallocating a given 1D array - quiet & without any check
subroutine deallocate_1d_int(array1d)

    integer, allocatable, intent(inout) :: array1d(:)

    if (allocated(array1d)) deallocate(array1d)

end subroutine deallocate_1d_int


!> (re-)allocating a given 1D array - verbose & with compulsory check
logical function reallocating_1d_int_ok(array1d, nitems)

    !use constants_module, only : uout

    integer, allocatable, intent(inout) :: array1d(:)

    integer, intent(in) :: nitems

    integer :: fail

    reallocating_1d_int_ok = deallocating_safe(array1d)

    if( reallocating_1d_int_ok ) then

        fail = 0

        allocate( array1d(nitems), stat = fail )

        if( fail > 0 ) then

            write(uout,*)
            write(uout,*)"Could not (re-)allocate 1D array (int) !!!"

            reallocating_1d_int_ok = .false.

        end if

    else

        write(uout,*)
        write(uout,*)"Could not de-allocate 1D array (int) !!! - Might be in use ???"

    end if

end function reallocating_1d_int_ok


!> deallocating a given 1D array - quiet but with compulsory check
logical function deallocating_1d_int_ok(array1d)

    integer, allocatable, intent(inout) :: array1d(:)

    integer :: zero

    zero = 0

    if (allocated(array1d)) deallocate(array1d, stat = zero)

    deallocating_1d_int_ok = (zero == 0)

end function deallocating_1d_int_ok



!> (re-)allocating a given 1D array - quiet but with possible check
subroutine reallocate_1d_flt(array1d, nitems, fail)

    !use constants_module, only : uout

    real(kind=wp), allocatable, intent(inout) :: array1d(:)

    integer, intent(in) :: nitems

    integer, intent(inout) :: fail

    call deallocate_quiet(array1d)

    fail = 0

    allocate( array1d(nitems), stat = fail )

end subroutine reallocate_1d_flt


!> deallocating a given 1D array - quiet & without any check
subroutine deallocate_1d_flt(array1d)

    real(kind=wp), allocatable, intent(inout) :: array1d(:)

    if (allocated(array1d)) deallocate(array1d)

end subroutine deallocate_1d_flt


!> (re-)allocating a given 1D array - verbose & with compulsory check
logical function reallocating_1d_flt_ok(array1d, nitems)

    !use constants_module, only : uout

    real(kind=wp), allocatable, intent(inout) :: array1d(:)

    integer, intent(in) :: nitems

    integer :: fail

    reallocating_1d_flt_ok = deallocating_safe(array1d)

    if( reallocating_1d_flt_ok ) then

        fail = 0

        allocate( array1d(nitems), stat = fail )

        if( fail > 0 ) then

            write(uout,*)
            write(uout,*)"Could not (re-)allocate 1D array (flt) !!!"

            reallocating_1d_flt_ok = .false.

        end if

    else

        write(uout,*)
        write(uout,*)"Could not de-allocate 1D array (flt) !!! - Might be in use ???"

    end if

end function reallocating_1d_flt_ok


!> deallocating a given 1D array - quiet but with compulsory check
logical function deallocating_1d_flt_ok(array1d)

    real(kind=wp), allocatable, intent(inout) :: array1d(:)

    integer :: zero

    zero = 0

    if (allocated(array1d)) deallocate(array1d, stat = zero)

    deallocating_1d_flt_ok = (zero == 0)

end function deallocating_1d_flt_ok



!> (re-)allocating a given 2D array - quiet but with possible check
subroutine reallocate_2d_int(array2d, ncols, nrows, fail)

    !use constants_module, only : uout

    integer, allocatable, intent(inout) :: array2d(:,:)

    integer, intent(in) :: ncols, nrows

    integer, intent(inout) :: fail

    call deallocate_quiet(array2d)

    fail = 0

    allocate( array2d(ncols,nrows), stat = fail )

end subroutine reallocate_2d_int


!> deallocating a given 2D array - quiet & without any check
subroutine deallocate_2d_int(array2d)

    integer, allocatable, intent(inout) :: array2d(:,:)

    if (allocated(array2d)) deallocate(array2d)

end subroutine deallocate_2d_int


!> (re-)allocating a given 2D array - verbose & with compulsory check
logical function reallocating_2d_int_ok(array2d, ncols, nrows)

    !use constants_module, only : uout

    integer, allocatable, intent(inout) :: array2d(:,:)

    integer, intent(in) :: ncols, nrows

    integer :: fail

    reallocating_2d_int_ok = deallocating_safe(array2d)

    if( reallocating_2d_int_ok ) then

        fail = 0

        allocate( array2d(ncols,nrows), stat = fail )

        if( fail > 0 ) then

            write(uout,*)
            write(uout,*)"Could not (re-)allocate 2D array (int) !!!"

            reallocating_2d_int_ok = .false.

        end if

    else

        write(uout,*)
        write(uout,*)"Could not de-allocate 2D array (int) !!! - Might be in use ???"

    end if

end function reallocating_2d_int_ok


!> deallocating a given 1D array - quiet but with compulsory check
logical function deallocating_2d_int_ok(array2d)

    integer, allocatable, intent(inout) :: array2d(:,:)

    integer :: zero

    zero = 0

    if (allocated(array2d)) deallocate(array2d, stat = zero)

    deallocating_2d_int_ok = (zero == 0)

end function deallocating_2d_int_ok



!> (re-)allocating a given 2D array - quiet but with possible check
subroutine reallocate_2d_flt(array2d, ncols, nrows, fail)

    !use constants_module, only : uout

    real(kind=wp), allocatable, intent(inout) :: array2d(:,:)

    integer, intent(in) :: ncols, nrows

    integer, intent(inout) :: fail

    call deallocate_quiet(array2d)

    fail = 0

    allocate( array2d(ncols,nrows), stat = fail )

end subroutine reallocate_2d_flt


!> deallocating a given 2D array - quiet & without any check
subroutine deallocate_2d_flt(array2d)

    real(kind=wp), allocatable, intent(inout) :: array2d(:,:)

    if (allocated(array2d)) deallocate(array2d)

end subroutine deallocate_2D_flt


!> (re-)allocating a given 2D array - verbose & with compulsory check
logical function reallocating_2d_flt_ok(array2d, ncols, nrows)

    !use constants_module, only : uout

    real(kind=wp), allocatable, intent(inout) :: array2d(:,:)

    integer, intent(in) :: ncols, nrows

    integer :: fail

    reallocating_2d_flt_ok = deallocating_safe(array2d)

    if( reallocating_2d_flt_ok ) then

        fail = 0

        allocate( array2d(ncols,nrows), stat = fail )

        if( fail > 0 ) then

            write(uout,*)
            write(uout,*)"Could not (re-)allocate 2D array (flt) !!!"

            reallocating_2d_flt_ok = .false.

        end if

    else

        write(uout,*)
        write(uout,*)"Could not de-allocate 2D array (flt) !!! - Might be in use ???"

    end if

end function reallocating_2d_flt_ok


!> deallocating a given 1D array - quiet but with compulsory check
logical function deallocating_2d_flt_ok(array2d)

    real(kind=wp), allocatable, intent(inout) :: array2d(:,:)

    integer :: zero

    zero = 0

    if (allocated(array2d)) deallocate(array2d, stat = zero)

    deallocating_2d_flt_ok = (zero == 0)

end function deallocating_2d_flt_ok


!AB: *** GENERIC ROUTINES FOR (RE-/DE-) ALLOCATING GIVEN ARRAYS VIA INTERFACE - END ***


end module arrays_module

