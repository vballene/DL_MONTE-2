! ***************************************************************************
! *   Copyright (C) 2015 by A.V.Brukhno                                     *
! *   andrey.brukhno[@]stfc.ac.uk                                           *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Everything related to generic FED calculations: Umbrella Sampling, Expanded Ensemble, Wang-Landau etc
!> @usage 
!> - most of the control is done via `<fed_interface_type>` instance `'fed'` normally
!> - reading of the input and interaction with extra FED flavors is via `'fed_interface_module.f90'`
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `fed_interface_type`

!> @modulefor FED calculations: Umbrella Sampling, Expanded Ensemble, Wang-Landau etc

module fed_calculus_module

    use kinds_f90
    use arrays_module
    use fed_interface_type
    use comms_mpi_module, only : master
    use parallel_loop_module, only : idgrp

    implicit none

        !> FED control structure
    type(fed_interface),save :: fed

        !> FED names container
    type(fed_interface_name),save :: fed_name

        !> generic FED order parameter vector
    real(kind=wp), allocatable, save :: parm_vec(:)

        !> generic FED histogram vector
    real(kind=wp), allocatable, save :: hist_vec(:)

        !> generic FED bias vector
    real(kind=wp), allocatable, save :: bias_vec(:)

        !> numbers of attempted & accepted FED moves (for each box/config)
    integer, allocatable, save :: num_attempted_fedmoves(:), num_accepted_fedmoves(:)

        !> previous order parameter value
    real(kind=wp), save :: param_old

        !> current order parameter value
    real(kind=wp), save :: param_cur

        !> previous and current ID/index of (sub)state in FED sampling
    integer, save :: id_old, id_cur

        !> running number of attempted FED moves
    integer, save :: num_total_fedmoves

        !> running number of accepted FED moves
    integer, save :: num_successful_fedmoves


        !> Collection matrix for transition-matrix method.
        !> There are two possible shapes for this, depending on the value of
        !> fed%tm_tridiagonal. If the 'full' matrix format is to be used, then
        !> tm_matrix(i,j) is the inferred number of transitions from
        !> FED state 'i' to 'j'. If the 'tridiagonal' matrix format is to be
        !> used, then tm_matrix(i,1) is the number of transitions from 'i' to
        !> state 'i-1'; tm_matrix(i,2) is the number of transitions from 'i' to
        !> 'i'; tm_matrix(i,3) is the number of transitions from 'i' to
        !> 'i-1'; and tm_matrix(i,4) is the number of transitions from 'i' to
        !> any other state.
    real(kind=wp), allocatable, save :: tm_matrix(:,:)

        !> Running number of transition-matrix updates
    integer, save :: num_tm_updates


contains


!> increment the FED histogram by default value `fed%upd_val`
subroutine fed_hist_inc(id)

    use constants_module

        !> the id/index of the (sub)state in FED calculation
    integer, intent(in) :: id

    hist_vec(id) = hist_vec(id) + 1.0_wp

    if( fed % method == FED_WL ) call fed_bias_inc(id)

end subroutine fed_hist_inc


!> increment the FED histogram by a given value (not 1 when histogram reweighting)
subroutine fed_hist_add(id,dh)

        !> the id/index of the (sub)state in FED calculation
    integer, intent(in) :: id

        !> histogram increment value (not 1 when histogram reweighting)
    real(kind=wp), intent(in) :: dh

    hist_vec(id) = hist_vec(id) + dh

    !AB: this is for histogram re-weighting (not ready)
    !if( fed % flavor == FED_HR .and. fed % method == FED_WL ) &
    !    call fed_bias_add( id_cur, (dh*fed%upd_val) )

end subroutine fed_hist_add


!> increment the FED bias function by default value `fed%upd_val`
subroutine fed_bias_inc(id)

        !> the id/index of the (sub)state in FED calculation
    integer, intent(in) :: id

    bias_vec(id) = bias_vec(id) + fed % upd_val

end subroutine fed_bias_inc


!> increment the FED bias function by a given value
subroutine fed_bias_add(id,db)

        !> the id/index of the (sub)state in FED calculation
    integer, intent(in) :: id

        !> bias increment value (e.g. WL drop)
    real(kind=wp), intent(in) :: db

    bias_vec(id) = bias_vec(id) + db

end subroutine fed_bias_add


!> returns the difference of FED bias function for two (sub)states defined by their ids/indices
real(kind=wp) function fed_bias_delta(id1,id2)

        !> the id/index of the (sub)state in FED calculation
    integer, intent(in) :: id1,id2

    fed_bias_delta = bias_vec(id2)-bias_vec(id1)

end function fed_bias_delta


!> returns the difference of FED bias function for two (sub)states defined by their ids/indices
real(kind=wp) function fed_ums_delta(par1,par2)

        !> the id/index of the (sub)state in FED calculation
    real(kind=wp), intent(in) :: par1,par2

    fed_ums_delta = fed%ums_frc * ( (par2-fed%ums_mid)**2 - (par1-fed%ums_mid)**2 )

end function fed_ums_delta


!> Increment 'tm_matrix' to reflect the fact that a move from (sub)state 'id_old' to 'id_cur'
!> with acceptance probability (canonical, i.e., without biasing) has just been proposed
subroutine fed_tm_inc(pacc)

        !> The canonical probability of the move being accepted
    real(kind=wp), intent(in) :: pacc

    integer :: id_diff
    
    num_tm_updates = num_tm_updates + 1

    if( fed%tm_tridiag ) then

        id_diff = id_cur - id_old

        !TU: See the comment describing the significance of the elements of
        !TU: 'tm_matrix' at the top of this file to help disentangle the below
        
        select case(id_diff)
        case(-1,0,1)
            
            id_diff = id_diff + 2
            
            tm_matrix(id_old,id_diff) = tm_matrix(id_old,id_diff) + pacc
            tm_matrix(id_old,2) = tm_matrix(id_old,2) + 1.0_wp - pacc
            
        case default

            tm_matrix(id_old,4) = tm_matrix(id_old,4) + pacc
            tm_matrix(id_old,2) = tm_matrix(id_old,2) + 1.0_wp - pacc

        end select
        
    else

        !TU: Faster to have a separate if(idtrial==idcur) branch?
        tm_matrix(id_old,id_cur) = tm_matrix(id_old,id_cur) + pacc
        tm_matrix(id_old,id_old) = tm_matrix(id_old,id_old) + 1.0_wp - pacc

    end if

end subroutine fed_tm_inc


!> increment the FED bias function by a given value
subroutine fed_bias_smooth()

    use constants_module, only : uout

    real(kind=wp), allocatable :: bias_avr(:)
    !real(kind=wp) :: bias_avr(fed%numstates)

    real(kind=wp) :: wgt2, bias_val

    integer :: kmin, kmax, k !, l, kavr

    integer :: fail

    fail = 0
    call reallocate_quiet(bias_avr,fed%numstates,fail)
    if( fail > 0 ) then
        call cry(uout,'',"Could not (re-)allocate auxilary FED bias array!!!",999)
    end if

    bias_avr(:) = bias_vec(:)

    wgt2 = (1.0_wp-fed%wgt_smooth)/2.0_wp

    !AB: must be odd number for the running average to be centered
    !kavr = 3

    kmin = fed%nbeg_smooth - 1
    kmax = fed%nend_smooth + 1

    do  k=1,fed%numstates

        if( k > kmin .and. k < kmax ) &
            bias_avr(k) = wgt2*bias_vec(k-1) + fed%wgt_smooth*bias_vec(k) + wgt2*bias_vec(k+1)

    end do

    bias_vec(:) = bias_avr(:)

    call deallocate_quiet(bias_avr)

end subroutine fed_bias_smooth


!> increment the FED bias function by a given value
subroutine fed_bias_upd(scale, iter, is_hist_zero, is_par_sum)

    use constants_module, only : uout, FED_US, FED_EE, FED_WL, FED_TM
    use comms_mpi_module, only : master, idnode, gsync, gsum
    use parallel_loop_module, only : idgrp

        !> the scaling factor for the FED bias strength (e.w. WL-drop or EE-scale) - not used for TM
    real(kind=wp), intent(in) :: scale

        !> the MC iteration number
    integer, intent(in) :: iter

        !> flag to switch on/off histogram zeroing
    logical, intent(in) :: is_hist_zero
    
    logical, intent(in) :: is_par_sum

    real(kind=wp) :: hist0, histMin, bias0, biasMax

    real(kind=wp) :: parm, upd_val

    integer :: k, kmin

    integer, save :: iter0 = 0

    if( fed % method == FED_EE ) then
    !AB: the case of EE - first update the bias function, and shift it so its min = 0
    !AB: the weighting factor for bias further updates is scaled below

      if( is_par_sum ) then

          call fed_data_outsum(iter-iter0)
          goto 101

      else

        hist0   = 0.0_wp
        bias0   = 0.0_wp

        histMin = 1.e10_wp
        biasMax =-1.e10_wp

        do k=1,fed%numstates

           hist0 = hist_vec(k)

           if( hist0 > 0.0_wp .and. hist0 < histMin ) histMin = hist0

        end do

        !AB: find the max value of accumulated bias and subtract it from the bias line
        !AB: this way the FED estimate = -bias(param) is kept positive with its minimum = 0

        do k=1,fed%numstates

            !bias0 = bias_vec(k)

            if( hist_vec(k) > 0.0_wp ) then 

                !bias0 = bias0 + fed%upd_val*dlog(hist_vec(k)/histMin)
                !bias_vec(k) = bias0

                bias_vec(k) = bias_vec(k) + fed%upd_val*dlog(hist_vec(k)/histMin)

            end if

        end do

        do k=1,fed%nitr_smooth

           call fed_bias_smooth()

        end do

        do k=1,fed%numstates

           bias0 = bias_vec(k)

           if( bias0 > biasMax ) biasMax = bias0

        end do

        do k=1,fed%numstates

            bias_vec(k) = bias_vec(k) - biasMax

        end do

      end if

    else if( fed % method == FED_WL ) then
    !AB: the case of WL - shift the bias function so its min = 0

        !AB: find the max value of accumulated bias and subtract it from the bias line
        !AB: this way the FED estimate = -bias(param) is kept positive with its minimum = 0

        bias0   = 0.0_wp
        biasMax =-1.e10_wp

        do k=1,fed%nitr_smooth

           call fed_bias_smooth()

        end do

        do k=1,fed%numstates

           bias0 = bias_vec(k)

           if( bias0 > biasMax ) biasMax = bias0

        end do

        do k=1,fed%numstates

           bias_vec(k) = bias_vec(k) - biasMax

        end do

    else if( fed % method == FED_TM ) then

        call fed_bias_upd_tm()

        !TU: Output the transition matrix to 'TMATRX.???'
        if( master ) call fed_tm_matrix_output(iter)

    else if( fed % method == FED_US ) then

        if( master ) call fed_data_output(iter)
        if( is_par_sum ) call fed_data_outsum(iter)

        return

    else
    !AB: this routine is only applicable in the cases of EE, WL and TM

        return

    end if

    if( master ) call fed_data_output(iter-iter0)
    !if( is_par_sum ) call fed_data_outsum(iter)

101 iter0 = iter

    if( is_hist_zero ) then 

        do k=1,fed%numstates

           hist_vec(k) = 0.0_wp

        end do

    else
    !AB: do not update feedback scale at the last step (iteration) - see the call!

        return

    end if

    !TU: The below stuff to do with the scale factor is not required in TM
    if( fed % method == FED_TM ) return

    upd_val = fed%upd_val*scale

    if( upd_val < fed%upd_val .or. upd_val < 1.0_wp ) then

        if( master ) then
            write(uout,*)
            write(uout,*)"fed_bias_upd:: scaling the FED bias feedback update: ", &
                          fed%upd_val," -> ",upd_val
            write(uout,*)
        end if

        fed%upd_val = upd_val

        fed%numupdskip = fed%numupdskip*2

    else

        if( master ) then
            write(uout,*)
            write(uout,*)"fed_bias_upd:: NO need for the FED bias feedback scaling : ", &
                          fed%upd_val," -/-> ",upd_val
            write(uout,*)
        end if

    end if

end subroutine fed_bias_upd


!> Update the FED bias function for the TM method without output to FEDDAT.???_??? or smoothing
subroutine fed_bias_upd_tm()

    real(kind=wp) :: biasMax
    
    integer :: k
    
    !TU: Update the bias via the shooting method...

    bias_vec(:) = 0.0_wp
    
    do k=1,fed%numstates-1

        if( fed%tm_tridiag ) then

            !TU: Is this the best way to do this to avoid numerical issues?
            bias_vec(k+1) = bias_vec(k) + log( &
                ( (tm_matrix(k,3)+1.0_wp) / (tm_matrix(k+1,1)+1.0_wp) ) * &
                ( (sum(tm_matrix(k+1,:))+1.0_wp) / (sum(tm_matrix(k,:))+1.0_wp) ) )

            
        else

            !TU: Is this the best way to do this to avoid numerical issues?
            bias_vec(k+1) = bias_vec(k) + log( &
                ( (tm_matrix(k,k+1)+1.0_wp) / (tm_matrix(k+1,k)+1.0_wp) ) * &
                ( (sum(tm_matrix(k+1,:))+1.0_wp) / (sum(tm_matrix(k,:))+1.0_wp) ) )

        end if
        
    end do
    
    biasMax = maxval(bias_vec)
    
    bias_vec = bias_vec - biasMax

end subroutine fed_bias_upd_tm


!> write out the obtained FED data
subroutine fed_data_output(iter)

    use constants_module, only : BOLTZMAN, uout, ufed, FED_US, FED_PAR_TEMP, FED_PAR_DENS
    use parse_module, only : int_2_char3
    !use parallel_loop_module, only : idgrp

        !> the MC iteration number (to add to the file-name)
    integer, intent(in) :: iter

            !TU: Note that if fed%is_bias_input==.true. then the bias function is
            !TU: overwritten below by that in FEDDAT.000! FEDDAT.000 has precedence! 

            !AB: NOTE: FEDDAT.000 is only used once - for reading in the initial bias vector 'bias_vec'
            !AB: whereas 'bias_out' is a temporary local array which never overrides data in 'bias_vec'

        !> local array to store and manipulate the currecnt data in 'bias_vec' (for a nice output)
    real(kind=wp) :: bias_out(fed%numstates)

    real(kind=wp) :: param, hsum, hbin, dvol0, dvol, biasEnd

    character*3  :: char_num1, char_num2

    integer, save :: kout = 0

    integer :: k, kmin, kmax, ktail

    logical :: safe

    call int_2_char3(idgrp, char_num1, safe)

    if( .not.safe ) then 

         write(uout,*)
         write(uout,*)"The node workgroup ID is too large: ",idgrp," (>999) - skipping FED output!"

         return

    end if

    !AB: the output counter
    kout = kout + 1

    call int_2_char3(kout, char_num2, safe)

    if( .not.safe ) then 

         write(uout,*)
         write(uout,*)"The FED update number is too large: ",kout," (>999) - skipping FED output!"

         return

    end if
    
    kmin = 1
    kmax = fed%numstates

    bias_out(:) = bias_vec(:)

    biasEnd = 0.0_wp

    if( fed%method == FED_US ) then

        !kmin = maxloc(hist_vec,dim=1)

        hsum = sum(hist_vec)

        !if( hist_vec(kmin) < 1.0_wp ) then
        if( hsum < 1.0_wp ) then

            write(uout,*)
            write(uout,*)"No FED data has been collected at iteration ",iter

            return

        end if

        !biasEnd = dlog(max(hist_vec,wp)/hsum)

        !kmin = fed%numstates+1
        !kmax = 0

        do k=1,fed%numstates

           bias_out(k) = fed%ums_frc * (parm_vec(k)-fed%ums_mid)**2

           if( hist_vec(k) > 0.0_wp ) then
               !bias_out(k) = bias_out(k) + dlog(hist_vec(k)/hist_vec(kmin))
               bias_out(k) = bias_out(k) + dlog(hist_vec(k)/hsum)
           !else
           !    if(kmin > k) kmin = k+1
           !    if(kmax < k) kmax = k-1
           end if

        end do

        !biasEnd = minval(bias_out)
        !bias_out(:) = bias_out(:) - biasEnd

        !write(uout,*)
        !write(uout,*)"FED-US data ready for writing: Kf, Rmin, Bmin = ",fed%ums_frc,", ",fed%ums_mid,", ",biasEnd

!   end if
    else if( fed%par_corr > 0 ) then

        if( fed%par_corr == 2 ) then
        !AB: correct for the entropy in spherical bin volume 
        !AB: works with linear parameter growth only!

            param = parm_vec(1)
            dvol0 = fed%par_inc*( param**2+fed%par_inc**2/12.0_wp )
            
            !kmin = fed%numstates+1
            !kmax = 0

            do k=1,fed%numstates

               if( hist_vec(k) > 0.0_wp ) then 
               !AB: in this case(s) order parameter is some distance in 3D
               !AB: so bare FED sampling requires a correction ...

                   param = parm_vec(k)
                   dvol  = fed%par_inc*( param**2 + fed%par_inc**2/12.0_wp )

                   bias_out(k) = bias_out(k) - dlog(dvol/dvol0)
               !else
               !    if(kmin > k) kmin = k+1
               !    if(kmax < k) kmax = k-1
               end if

            end do

        end if

        ktail = max(1,fed%numstates-9)

        do k=ktail,fed%numstates

           biasEnd = biasEnd + bias_out(k)

        end do

        biasEnd = biasEnd / (fed%numstates - ktail + 1)

        bias_out(:) = bias_out(:)-biasEnd

    end if

    write(uout,*)
    write(uout,*)"Writing out FED data file '",'FEDDAT.'//trim(char_num1)//'_'//trim(char_num2),"'"

    !AB: write FEDDAT file

    open( ufed, file='FEDDAT.'//trim(char_num1)//'_'//trim(char_num2) )

    write(ufed,'(a,i10,a,f20.10)') &
                     "# FED data after a block of ",iter," MC iterations at bias update rate ",fed%upd_val
    write(ufed,'(a)')"# "
    write(ufed,'(a)')"# param   bias   hist"

    kmin = max(kmin,1)
    kmax = min(kmax,fed%numstates)

    do k=1,fed%numstates

       param = parm_vec(k)

       if( fed % par_kind == FED_PAR_TEMP ) param = 1.0_wp/(param*BOLTZMAN)

       write(ufed,'(2f20.10,f20.5)') param,-bias_out(k),hist_vec(k)
       !write(ufed,'(2f20.10,f20.5)') param,-(bias_out(k)-biasEnd),hist_vec(k)

    end do

    close(ufed)

end subroutine fed_data_output


!> write out the obtained FED data
subroutine fed_data_outsum(iter)

    use constants_module, only : BOLTZMAN, uout, ufed, FED_US, FED_EE, FED_WL, FED_TM, FED_PAR_TEMP
    use parse_module, only : int_2_char3
    use comms_mpi_module, only : master, idnode, gsum_world, gsync_world
    use parallel_loop_module, only : wkgrp_size, base_temp, repx_dtemp, open_nodefiles

        !> the MC iteration number (to add to the file-name)
    integer, intent(in) :: iter

        !> local array to store and manipulate the currecnt data in 'bias_vec' (for a nice output)
    real(kind=wp) :: bias_out(fed%numstates), hist_out(fed%numstates)

    real(kind=wp) :: param, hsum, hbin, dvol0, dvol, biasEnd

    real(kind=wp) :: hist0, histMin, bias0, biasMax

    character*3  :: char_num1, char_num2

    integer, save :: kout = 0

    integer :: k, kmin, kmax, ktail

    logical :: safe
    
    if( fed%method == FED_TM ) return

    bias_out(:) = bias_vec(:)
    hist_out(:) = hist_vec(:)

    call gsync_world

    !if( fed%method == FED_US .or. fed%method == FED_EE ) then

        call gsum_world(hist_out)
        hist_out(:) = hist_out(:)/real(wkgrp_size,wp)

    !else 
    if( fed%method == FED_WL ) then

        call gsum_world(bias_out)
        bias_out(:) = bias_out(:)/real(wkgrp_size,wp)

    end if

    if( fed % method == FED_EE ) then
    !AB: the case of EE - first update the bias function, and shift it so its min = 0
    !AB: the weighting factor for bias further updates is scaled below

        hist0   = 0.0_wp
        bias0   = 0.0_wp

        histMin = 1.e10_wp
        biasMax =-1.e10_wp

        do k=1,fed%numstates

           hist0 = hist_out(k)

           if( hist0 > 0.0_wp .and. hist0 < histMin ) histMin = hist0

        end do

        !AB: find the max value of accumulated bias and subtract it from the bias line
        !AB: this way the FED estimate = -bias(param) is kept positive with its minimum = 0

        do k=1,fed%numstates

            if( hist_out(k) > 0.0_wp ) then 

                bias_out(k) = bias_out(k) + fed%upd_val*dlog(hist_out(k)/histMin)

            end if

        end do

        do k=1,fed%nitr_smooth

           call fed_bias_smooth()

        end do

        do k=1,fed%numstates

           bias0 = bias_out(k)

           if( bias0 > biasMax ) biasMax = bias0

        end do

        do k=1,fed%numstates

            bias_out(k) = bias_out(k) - biasMax

        end do

        bias_vec(:) = bias_out(:)

    !else if( fed % method == FED_WL ) then
    end if

    if( idnode > 0 ) return

    kmin = 1
    kmax = fed%numstates

    !AB: the output counter
    kout = kout + 1

    call int_2_char3(kout, char_num2, safe)

    if( .not.safe ) then 

         write(uout,*)
         write(uout,*)"The FED update number is too large: ",kout," (>999) - skipping FED output!"

         return

    end if

    biasEnd = 0.0_wp

    if( fed%method == FED_US ) then

        hsum = sum(hist_out)

        if( hsum < 1.0_wp ) then

            write(uout,*)
            write(uout,*)"No FED data has been collected at iteration ",iter

            return

        end if

        do k=1,fed%numstates

           bias_out(k) = fed%ums_frc * (parm_vec(k)-fed%ums_mid)**2

           if( hist_out(k) > 0.0_wp ) then

               bias_out(k) = bias_out(k) + dlog(hist_out(k)/hsum)

               if(kmin == 1) kmin = k
               kmax = k

           end if

        end do

        biasEnd = minval(bias_out)
        bias_out(:) = bias_out(:) - biasEnd

!    end if
    else if( fed%par_corr > 0 ) then

        if( fed%par_corr == 2 ) then
        !AB: correct for the entropy in spherical bin volume 
        !AB: works with linear parameter growth only!

            param = parm_vec(1)
            dvol0 = fed%par_inc*( param**2+fed%par_inc**2/12.0_wp )

            do k=1,fed%numstates

               if( hist_out(k) > 0.0_wp ) then 
               !AB: in this case(s) order parameter is some distance in 3D
               !AB: so bare FED sampling requires a correction ...

                   param = parm_vec(k)
                   dvol  = fed%par_inc*( param**2 + fed%par_inc**2/12.0_wp )

                   bias_out(k) = bias_out(k) - dlog(dvol/dvol0)

                   if(kmin == 1) kmin = k
                   kmax = k

               end if

            end do

        end if

        ktail = max(1,fed%numstates-9)

        do k=ktail,fed%numstates

           biasEnd = biasEnd + bias_out(k)

        end do

        biasEnd = biasEnd / (fed%numstates - ktail + 1)

        bias_out(:) = bias_out(:)-biasEnd

    end if

    write(uout,*)
    write(uout,*)"Writing out FED data file '",'FEDDAT.TOT_'//trim(char_num2),"'"

    !AB: write FEDDAT file

    open( ufed, file='FEDDAT.TOT_'//trim(char_num2) )

    write(ufed,'(a,i10,a,f20.10)') &
                     "# FED data after a block of ",iter," MC iterations at bias update rate ",fed%upd_val
    write(ufed,'(a)')"# "
    write(ufed,'(a)')"# param   bias   hist"

    kmin = max(kmin,1)
    kmax = min(kmax,fed%numstates)

    do k=kmin,kmax

       param = parm_vec(k)

       if( fed % par_kind == FED_PAR_TEMP ) param = 1.0_wp/(param*BOLTZMAN)

       write(ufed,'(2f20.10,f20.5)') param,-bias_out(k),hist_out(k)

    end do

    close(ufed)
    
end subroutine fed_data_outsum


!TU: Added by me...
!> Output 'tm_matrix' to the file 'TMATRIX.???', where '???' is the process group number. 
!> This procedure should only be called by master processes. Note that there are two
!> possible formats, 'tridiagonal' and 'full'. Which is used depends on the value of
!> 'fed%tm_tridiag'
subroutine fed_tm_matrix_output(iter)

    use constants_module, only : uout, utm
    use parallel_loop_module, only : idgrp
    use parse_module, only : int_2_char3

      !> the MC iteration number (for the header of the output file)
    integer, intent(in) :: iter

    integer :: i, j

    character*3  :: char_num1

    logical :: safe

    call int_2_char3(idgrp, char_num1, safe)

    if( .not.safe ) then 

         write(uout,*)
         write(uout,*)"The node workgroup ID is too large: ",idgrp," (>999) - skipping TM matrix output!"

         return

    end if

    !TU: For the full TM matrix, output to 'TMATRX.???' is only supported if fed%numstates <= 100000 (see below)
    if( (.not. fed%tm_tridiag) .and. (fed%numstates >= 100000) ) then

         write(uout,*)
         write(uout,*) "The number of FED states is too large: ",fed%numstates," (>100000) - skipping TM matrix output!"

         return

    end if

    write(uout,*)
    write(uout,'(a,i10,a)') " Writing out FED TM matrix to file 'TMATRX."//trim(char_num1)//"' (iteration ",iter,")"

    open(utm, file='TMATRX.'//trim(char_num1))

    write(utm,'(a,i10,a)') "# FED TM matrix after ",iter," MC iterations"

    if( fed%tm_tridiag ) then

        write(utm,'(a,i8,a)')  "# This matrix is for ",fed%numstates," FED states - TRIDIAGONAL format"
        
    else

        write(utm,'(a,i8,a)')  "# This matrix is for ",fed%numstates," FED states - FULL format"

    end if

    do i=1, fed%numstates

        if( fed%tm_tridiag ) then

            write(utm,'(100000e26.17)') ( tm_matrix(i,j), j=1,4 )

        else
            
            !TU: The 100000 is the maximum size which can be output using the following line
            write(utm,'(100000e26.17)') ( tm_matrix(i,j), j=1,fed%numstates )

        end if

    end do
    
    close(utm)

end subroutine fed_tm_matrix_output


!> write out the obtained FED data
subroutine fed_moves_output(nconfig)

    use constants_module, only : uout, FED_PAR_DIST2, FED_TM
    !use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

        !> number of boxes/configurations/replicas
    integer, intent(in) :: nconfig

    integer :: i

    !AB: Output FED move counters.
    if( master ) then

        if( nconfig > 1 ) then

            do i = 1, nconfig

               write(uout,"(/,1x,'attempted  FED moves (box) = ',i10,' (',i3,')')") &
                     num_attempted_fedmoves(i),i
               write(uout,"(  1x,'successful FED moves (box) = ',i10,' (',i3,')')") &
                     num_accepted_fedmoves(i),i

            end do

        end if

        write(uout, "(/,1x,'attempted  FED moves (tot) = ',i10)") num_total_fedmoves
        write(uout, "(  1x,'successful FED moves (tot) = ',i10)")  num_successful_fedmoves

        if( fed % method == FED_TM ) then

            write(uout, "(/,1x,'transition matrix updates = ',i10)") num_tm_updates

        end if

    end if

end subroutine fed_moves_output


!> returns the difference of hamiltonian for FED - the cases of T or lambda-coupling only
real(kind=wp) function fed_ham_delta(energy,id1,id2)

    use constants_module

        !> total energy or part thereof to be rescaled for FED \n
        !> for ex., it can be the extra interaction due to a molecule being inserted  (U_1-U_0) \n
        !> it can also be the energy difference when one system is being transformed into the other (U_1-U_0)
    real(kind=wp), intent(in) :: energy

        !> the ids/indeces of two (sub)states in FED calculation \n
    integer, intent(in) :: id1,id2

    fed_ham_delta = ( parm_vec(id2) - parm_vec(id1) )*energy

end function fed_ham_delta


!> attempt to hop in T (temperature) within FED calculation
subroutine fed_attempt_temp(ib, job, beta, betainv, energytot, iter)

    use kinds_f90
    use constants_module
    use control_type
    use random_module, only : duni

        !> configuration id/index
    integer, intent(in) :: ib,iter

        !> simulation control structure
    type(control), intent(inout) :: job

        !> self-explanatory
    real(kind = wp), intent(inout) :: beta, betainv, energytot(:)

    real(kind=wp) :: temp_sys, temp_par, arg, dbU, dbB, deltav, engtotal

    integer :: id0, id1, npower

    !if( fed % par_kind /= FED_PAR_TEMP .and. fed % par_kind /= FED_PAR_BETA ) return

    !AB: for reference:
    !parm_vec(k) = fed % par_min + delta * real(k-1,wp)**power

    if( fed % par_kind == FED_PAR_TEMP ) then

        arg = ( betainv / BOLTZMAN - fed % par_min ) / fed % par_inc

    else if( fed % par_kind == FED_PAR_BETA ) then

        arg = ( beta - fed % par_min ) / fed % par_inc

    else
        return
    end if

    temp_sys = job%systemp
    temp_par = 1.0_wp/(parm_vec(id_cur)*BOLTZMAN)

    !if( temp_sys /= temp_par ) then 
    if( abs(temp_sys - temp_par) > 1.0e-8_wp ) then 

        write(uout,*)
        write(uout,*)"fed_attempt_temp(): check T = ",temp_sys," =/= ",temp_par

        call cry(uout,'',&
                 "ERROR: FED attempted T-variation - inconsistent Temperature!!!",999)

    !else if( beta /= parm_vec(id_cur) ) then 
    else if( abs(beta - parm_vec(id_cur)) > 1.0e-8_wp ) then 

        write(uout,*)
        write(uout,*)"fed_attempt_temp(): check 1/(RgT) = ",beta," =/= ",parm_vec(id_cur)

        call cry(uout,'',&
                 "ERROR: FED attempted T-variation - inconsistent beta = 1/(RgT)!!!",999)

    end if

    num_attempted_fedmoves(ib) = num_attempted_fedmoves(ib) + 1
    num_total_fedmoves = num_total_fedmoves + 1

    npower = fed % inc_kind

    if( npower > 1 ) arg = arg**(1.0_wp/real(npower,wp))

    !AB: increment id0 to get within [1,...,fed % num_states]
    id0 = nint(arg)+1

    !AB: start with attempting to jump up in the FED parameter range
    id1 = id0+1

    if( id_cur /= id0 .and. master ) then 

        write(uout,*)
        write(uout,*)"fed_attempt_temp(): check current index = ",id_cur," =/= ",id0

        call cry(uout,'',&
                 "ERROR: FED attempted T-variation - inconsistent parameter index!!!",999)

    end if

    id_old = id_cur

    if( duni() < 0.5_wp ) then 

        id1 = id0-1

        ! reject moving outside the range
        if( id1 < 1 ) then

            !AB: bias is also updated if needed (e.g. WL)
            call fed_hist_inc(id_cur)

            !AB: for debugging only
            !if( master ) write(uout,*)"FED rejected  T-variation: attempted move ",id_cur, &
            !             " (",id0,parm_vec(id0),betainv/BOLTZMAN,") -> ",id1

            return 

        end if

    else 

        ! reject moving outside the range
        if( id1 > fed % numstates ) then

            !AB: bias is also updated if needed (e.g. WL)
            call fed_hist_inc(id_cur)

            !AB: for debugging only
            !if( master ) write(uout,*)"FED rejected  T-variation: attempted move ",id_cur, &
            !             " (",id0,parm_vec(id0),betainv/BOLTZMAN,") -> ",id1

            return

        end if

    end if

    !AB: get the associated energetic term variation
    dbU = fed_ham_delta(energytot(ib),id0,id1)

    !AB: get the associated bias function variation
    dbB = fed_bias_delta(id0,id1)

    !AB: sum up the terms for the Metropolis test
    arg = dbU+dbB

    if( arg < job%toler .and. duni() < exp(-arg) ) then

        !AB: for debugging only
        !if( master ) write(uout,*)"FED accepted  T-variation: ",id_cur," -> ",id1, &
        !             " and T : ",betainv/BOLTZMAN," -> ",1.0_wp/parm_vec(id1)/BOLTZMAN, &
        !             "; dbB,dbU,dbTot = ",dbB,dbU,arg,arg/cfgs%number_of_atoms

        !accept the temperature update

        id_cur  = id1

        beta    = parm_vec(id_cur)
        betainv = 1.0_wp/beta

        !AB: update the system temperature
        job % systemp = betainv / BOLTZMAN

        !AB: increment the counters
        num_accepted_fedmoves(ib) = num_accepted_fedmoves(ib) + 1
        num_successful_fedmoves   = num_successful_fedmoves + 1

    !else

        !AB: for debugging only
        !if( master ) write(uout,*)"FED rejected  T-variation: ",id_cur," -> ",id1, &
        !             " and T : ",betainv/BOLTZMAN," -> ",1.0_wp/parm_vec(id1)/BOLTZMAN, &
        !             "; dbB,dbU,dbTot = ",dbB,dbU,arg,arg/cfgs%number_of_atoms

    endif

    !AB: bias is also updated if needed (e.g. WL)
    call fed_hist_inc(id_cur)

end subroutine fed_attempt_temp


!> some other routine
!subroutine some_routine(params)
!
!end subroutine


!AB: *** (RE-/DE-) ALLOCATING SPECIFIC FED VECTORS & ARRAYS - START ***


!> (re-)allocating compulsory generic FED vectors (main 1D arrays), and 'tm_matrix' (2D) if the method is TM.
subroutine fed_reallocate_vectors(nconfigs)

    use constants_module, only : uout, FED_TM
    !use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

        !> number of configurations/boxes present in simulation
    integer, intent(in) :: nconfigs

    integer, dimension(4) :: fail

        fail = 0

        !AB: main FED vectors - parameter, histogram and bias function

        call reallocate_quiet( parm_vec, fed%numstates, fail(1) )
        call reallocate_quiet( hist_vec, fed%numstates, fail(2) )
        call reallocate_quiet( bias_vec, fed%numstates, fail(3) )

        if ( any(fail > 0) ) then

            call cry(uout,'', &
                 "Could not allocate generic FED arrays for main vectors!!!",999)

            !write(uout,*)
            !write(uout,*)"Could not allocate generic FED arrays for main vectors!!!"
            !call error(999)

        end if

        parm_vec = 0.0_wp
        bias_vec = 0.0_wp
        hist_vec = 0.0_wp

        !AB: main FED counters - attempted & accepted FED moves in every MC box

        call reallocate_quiet( num_attempted_fedmoves, nconfigs, fail(1) )
        call reallocate_quiet( num_accepted_fedmoves,  nconfigs, fail(2) )

        if ( any(fail > 0) ) then

            call cry(uout,'', &
                 "Could not allocate generic FED arrays for stats counters!!!",999)

            !write(uout,*)
            !write(uout,*)"Could not allocate generic FED arrays for stats counters!!!"
            !call error(999)

        end if

        num_attempted_fedmoves = 0
        num_accepted_fedmoves  = 0

        !TU: Allocate 'tm_matrix' if we are using TM
        if( fed % method == FED_TM ) then

            if( fed%tm_tridiag ) then

                call reallocate_quiet( tm_matrix, fed%numstates, 4, fail(1) )

            else
                
                call reallocate_quiet( tm_matrix, fed%numstates, fed%numstates, fail(1) )


            end if

            if ( any(fail > 0) ) then

                call cry(uout,'', &
                     "Could not allocate collection matrix for TM!!!",999)

            end if

            tm_matrix = 0.0_wp
            num_tm_updates = 0

        end if

end subroutine fed_reallocate_vectors


subroutine fed_deallocate_vectors()

        use constants_module, only : FED_TM

        call deallocate_quiet(parm_vec)
        call deallocate_quiet(hist_vec)
        call deallocate_quiet(bias_vec)
        call deallocate_quiet(num_attempted_fedmoves)
        call deallocate_quiet(num_accepted_fedmoves)

        !TU: Deallocate 'tm_matrix' if we are using TM
        if( fed % method == FED_TM ) then

            call deallocate_quiet(tm_matrix)

        end if

end subroutine fed_deallocate_vectors


!AB: *** (RE-/DE-) ALLOCATING SPECIFIC FED VECTORS & ARRAYS - END ***


end module fed_calculus_module
