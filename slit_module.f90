!***************************************************************************
!   Copyright (C) 2015-2017 by A. V. Brukhno                               *
!   andrey.brukhno[@]stfc.ac.uk                                            *
!                                                                          *
!***************************************************************************

!> @brief
!> - Extension module for simulation in 2D-slit (planar pore): switches, parameters & functions
!> @usage 
!> - @stdusage
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @modulefor simulation in 2D-slit (planar pore): control switches, parameters & functions

module slit_module

    use kinds_f90
    !use constants_module
    !use control_type

    implicit none

        !> minimum allowed number of bins on `Z`-grid for MFA calculations (`Z`-layers)
    integer, parameter :: minbins = 101

        !> maximum allowed number of bins on `Z`-grid for MFA calculations (`Z`-layers)
    integer, parameter :: maxbins = 1001

        !> internal flag to store `<control_type>%%is_slit` \n
        !> for checking if the cell type is slit (planar pore), default: .false. \n
        !> see `control_type.f90` for full info
    logical, save :: in_slit = .false. ! default is bulk

        !> the opposite of `in_slit` (not to invert in `if`-checks within the main loop)
    logical, save :: in_bulk = .true. ! default is bulk

        !> flag for explicit Coulomb treatment (no Ewald summation)
    logical, save :: coul_explicit = .false. ! default is bulk with Ewald sums

    logical, save :: is_slit_gcmc_z = .false. ! default

    logical, save :: is_slit_frac_z = .false. ! default

        !> integer alternative to `in_slit` 
        !> preferred where distinction via calculation is possible, as opposed to `if( in_slit )` ...
    integer, save :: im_slit = 0 ! default is bulk

        !> integer alternative to `in_bulk` 
        !> preferred where distinction via calculation is possible, as opposed to `if( in_bulk )` ...
    integer, save :: im_bulk = 1 ! default is bulk

        !> internal switch to store `<control_type>%%cell_type`, default: 0 \n
        !> see `control_type.f90` & `constants_module.f90` for details \n
        !> \b 2D-slit case (planar pore): subject to `is_slit = .true.` & `cell_type < 0`\n
    integer, save :: slit_type = 0 ! default is bulk

        !> internal switch to combine with `<control_type>%%coultype == -1` \n
        !> see `control_type.f90` & `constants_module.f90` for details \n
        !> \b 2D-slit case (planar pore): subject to `is_slit = .true.` & `cell_type < 0` \n
    integer, save :: mfa_type = 0 ! default is bulk

        !> number of soft walls
    integer, save :: walls_soft = 0 ! default is bulk (no walls)

        !> number of charged walls
    integer, save :: walls_charged = 0 ! default is bulk (no walls)

        !> density unit to use for output: Molar (1=Default), Number density (2=N/V), Volume fraction (3)
    integer, save :: den_unit = 1 ! default is bulk

        !> number of `Z`-layers for the largest slit width
    integer, save :: nbins_max = 0

        !> number of samples taken for `Z`-layers (assuming uniform sampling over replicas, i.e. same number for all)
    !integer, save :: nsamples_mfa = 0

        !> number of MFA cycles
    integer, save :: mfa_ncycles = 0

        !> surface charge density on the walls
    real( kind = wp ), save :: SCD1 = 0.0_wp
    real( kind = wp ), save :: SCD2 = 0.0_wp

        !> bin/layer size for MFA calculations on `Z`-grid (Angstroem; default)
    real( kind = wp ), save :: deltaz = 0.01_wp
    real( kind = wp ), save :: dzfrac = 0.0_wp
    real( kind = wp ), save :: zdimax = 0.0_wp

        !> inwards z-shift for GCMC (+ for bottom and - for top)
    real(kind = wp), save :: slit_gcmc_z = 0.0_wp

        !> inwards z-shift for MOVEs (+ for bottom and - for top)
    real(kind = wp), save :: slit_frac_z = 0.0_wp
    real(kind = wp), save :: slit_zfrac1 =-0.5_wp
    real(kind = wp), save :: slit_zfrac2 = 0.5_wp

        !> damping factor for MFA iteration on `Z`-grid (default)
    real( kind = wp ), save :: damp_mfa = 0.0_wp

        !> switch for spherical cut-off (defaults: 1 when `in_bulk = .true.`; 0 when `in_slit = .true.`)
    real(kind = wp), save :: onoff_rc = 1.0_wp

        !> switch for cylindrical cut-off (default: 0; can be 1 only when `onoff_rc = 0`, `in_slit=.true.`)
    real(kind = wp), save :: onoff_xy = 0.0_wp

        !> '`Z`'-layer numbers array on `Z`-grid (due to several configs/replicas)
    integer, allocatable, dimension(:), save :: nbinz

        !> number of samples taken for `Z`-layers (different/separate for each replica)
    integer, allocatable, dimension(:), save :: nsamples_mfa

        !> inverse volume array (due to several configs/replicas)
    real( kind = wp ), allocatable, dimension(:), save :: vinvs

        !> '`Z`'-dim array (due to several configs/replicas)
    real( kind = wp ), allocatable, dimension(:), save :: zdims

        !> density arrays for on `Z`-grid
    real( kind = wp ), allocatable, dimension(:,:), save :: zden_chpos, zden_chneg

        !> charge arrays for MFA calculations on `Z`-grid (are all needed - ???)
    real( kind = wp ), allocatable, dimension(:,:), save :: charge_pos, charge_neg, charge_new, charge_old, charge_mfa

        !> potential arrays for MFA calculations on `Z`-grid (are all needed - ???)
    real( kind = wp ), allocatable, dimension(:,:), save :: pot_mfa, pot_corr, pot_cora, pot_new, pot_old


contains


!> check and initialise the control switches and parameters for 2D-slit case (if any)
subroutine initialise_slit(this_job, dens_unit)

    !use kinds_f90
    use constants_module
    use control_type
    use species_module, only: charges_present
    use comms_mpi_module, only : master !, exit_mpi_comms

    implicit none

        !> global simulation control switches & parameters
    type(control), intent(inout) :: this_job

        !> density unit for output
    integer, intent(in) :: dens_unit

    integer :: mfa_act

    in_slit = this_job%is_slit
    mfa_act = abs(mfa_type)

    slit_type = this_job%cell_type
    den_unit  = dens_unit

! AB: check the consistency of input in CONTROL (switches) & FIELD (charges) files

    if ( in_slit ) then
       ! in_slit <-> slit_type := cell_type < 0 (set while reading CONTROL file)

        im_slit = 1
        im_bulk = 0

        if ( all(this_job%coultype == 1) .and. mfa_act==0 ) then !.and. is_slit_frac_z ) then
            
               if( master ) &
               write(uout,"(/,1x,'*** NOTE *** cell (box) type [ < 0 -> 2D-slit ] = '&
                   &,i10,' coultype = ',10i10)") &
                   & slit_type, this_job%coultype, mfa_type
        
        else if ( any(this_job%coultype > 0) ) then
            ! AB: for 2D-slit coultype MUST BE less than or equal 0

            call error(21)

        else if ( any(this_job%coultype == 0) ) then
            ! AB: coultype == 0 -> NO electrostatics whatsoever, no charges, no charged walls!
            ! AB: MUST BE complemented by slit_type > -10 & mfa = 0

            if ( slit_type <= SLIT_EXT1_MFA0 .or. mfa_type /= MFA_OFF ) then 

               if( master ) &
               write(uout,"(/,1x,'NOTE: cell (box) type [ < 0 -> 2D-slit ] = ',i10,' coultype = ',10i10)") &
                   & slit_type, this_job%coultype, mfa_type

               call error(22)
            end if

        else 
            ! AB: coultype < 0 -> Coulomb interactions MUST BE treated one way or another, provided there are charges!
            ! AB: MUST BE complemented by slit_type <= -10 (no matter if any wall is charged)

            !AB: NOTE: here slit_type =/= cell_type
            slit_type = this_job%cell_type*10-mfa_act

            if ( slit_type > SLIT_EXT1_MFA0 ) call error(23)

            if ( mfa_type /= MFA_OFF ) then
            ! AB: mfa_type =/= 0 -> MFA correction for "explicit" in-box Coulomb
            ! AB: MUST BE complemented by slit_type < -10/-20/-30

                if ( slit_type == SLIT_EXT1_MFA0 &
                .or. slit_type == SLIT_EXT2_MFA0 &
                .or. slit_type == SLIT_HARD_MFA0 ) call error(25)

                if ( mfa_act > 3 ) call error(26)

            else if ( slit_type /= SLIT_EXT1_MFA0 &
                .and. slit_type /= SLIT_EXT2_MFA0 &
                .and. slit_type /= SLIT_HARD_MFA0 ) then
            ! AB: mfa_type == 0 -> NO MFA correction for "explicit" in-box Coulomb
            ! AB: MUST BE complemented by slit_type == -10/-20/-30

                call error(24)

            end if

        end if

    else if ( mfa_type /= MFA_OFF ) then
        ! AB: for 3D-bulk mfa_type MUST BE MFA_OFF = 0

        im_slit = 0
        im_bulk = 1

        call error(21)

    end if

    in_bulk = .not. in_slit

    this_job%is_slit   = in_slit

    coul_explicit = ( this_job%coultype(1) == -1 )
    !coul_explicit = ( mfa_type < -1 )

    ! AB: bulk/slit cut-off treatment:

    ! AB: onoff_rc == 1 -> enable spherical cut-off (either bulk with Ewald, or slit with explicit Coulomb)
    ! AB: onoff_rc == 0 -> ignore spherical cut-off (explicit all-in-box Coulomb)
    onoff_rc = real(im_bulk,wp)
    
    !AB: allow extra electrostatic treatments
    !if( mfa_act == 0 .and. .not.coul_explicit ) onoff_rc = 1.0_wp
    if( mfa_act == 0 ) onoff_rc = 1.0_wp

    ! AB: onoff_xy == 0 -> ignore cylindrical cut-off (onoff prevails)
    ! AB: onoff_xy == 1 -> enable cylindrical cut-off (onoff == 0, explicit Coulomb)
    onoff_xy = 0.0_wp

    if( mfa_act == 2 ) then

        onoff_xy = 1.0_wp
        if( slit_type /= -12 .and. slit_type /= -22  .and. slit_type /= -32 ) call error(27)

    end if

    ! AB: specific treatment for 2D-slit case - Coulomb with spherical cut-off
    if( mfa_act == 3 ) then

        onoff_rc = 1.0_wp
        if( slit_type /= -13 .and. slit_type /= -23  .and. slit_type /= -33 ) call error(27)

    end if

    ! AB: the final check for consistency with species specs (charges <-> Coulomb calc.) 
    if ( charges_present .and. any(this_job%coultype == 0) ) then

        if( master ) &
            write(uout,"(/,1x,'NOTE: cell (box) type [ < 0 -> 2D-slit ] = ',i10,' coultype = ',10i10)") &
                  & slit_type, this_job%coultype, mfa_type

        call warning(23)

        slit_type = this_job%cell_type

        if ( slit_type <= SLIT_EXT1_MFA0 ) call error(22)

        this_job%coultype = 0

        mfa_type = 0

    else if (.not.charges_present .and. any(this_job%coultype /= 0) ) then 

        if( master ) &
            write(uout,"(/,1x,'NOTE: cell (box) type [ < 0 -> 2D-slit ] = ',i10,' coultype = ',10i10)") &
                 & slit_type, this_job%coultype, mfa_type

        call warning(24)

        slit_type = this_job%cell_type

        if ( slit_type <= SLIT_EXT1_MFA0 ) call error(22)

        this_job%coultype = 0

        mfa_type = 0

    end if

end subroutine


!> allocating arrays for MFA calculus on `Z`-grid over `'nbins'` layers (in `'nconfig'` replicas)
subroutine allocate_MFA_arrays(nconfs, configs)

    use kinds_f90
    use constants_module, only : uout
    use config_type
    use comms_mpi_module, only : master !, exit_mpi_comms

    implicit none

        !> number of configurations (replicas)
    integer, intent(in) :: nconfs

        !> array storing configurations read from input (read-only here!)
    type(config), intent(in) :: configs(:)

    real (kind = wp) :: zdim

    integer, dimension(6) :: fail

        ! current number of `Z`-layers
    integer :: nbins

    integer :: icfg, i_max

    logical :: is_reset = .false.

    fail(:) = 0

    allocate( vinvs(nconfs), stat = fail(1) )
    allocate( zdims(nconfs), stat = fail(2) )
    allocate( nbinz(nconfs), stat = fail(3) )

    if ( any(fail > 0) ) call error (31)

    nbins_max = 0

    i_max = 0

    ! AB: find the largest number of bins (Z-layers) out of all configurations/replicas
    do icfg = 1,nconfs

       zdim = configs(icfg)%vec%latvector(3,3)

       zdims(icfg) = zdim

       nbins = int(zdim/deltaz)+1

       nbinz(icfg) = nbins

       if( nbins > nbins_max ) then 

          i_max = icfg

          nbins_max = nbins

          if( nbins_max < minbins ) then

              is_reset = .true.

              call warning(22)

              if( master ) &
                  write(uout,'(1x,2(a,i6))') '                : ',nbins_max,' -> ',minbins

              nbins_max = minbins

          else if( nbins_max > maxbins ) then

              is_reset = .true.

              call warning(22)

              if( master ) &
                  write(uout,'(1x,2(a,i6))') '                : ',nbins_max,' -> ',maxbins

              nbins_max = maxbins

          end if

       end if

    end do

    flush(uout)

    zdimax = zdims(i_max)

!AB: assume the same max number of bins in all replicas
!AB: make sure the number of bins is odd (allow one extra bin)!
    nbins = (nbins_max-1)/2
    nbins_max = 2*nbins+1

    dzfrac = 1.0_wp/real(nbins_max-1,wp)
    deltaz = zdimax/real(nbins_max-1,wp)

    nbins = nbins_max
    call zero_MFA_arrays(nbins,nconfs)

end subroutine


!> re-allocating arrays for MFA calculus on `Z`-grid (for all replicas)
subroutine zero_MFA_arrays(nbins,nconfs)

    use kinds_f90
    use constants_module, only : uout
    use comms_mpi_module, only : master

        !> number of `Z`-layers
    integer, intent(in)   :: nbins

        !> number of replicas
    integer, intent(in)   :: nconfs

    integer :: ic

    integer, dimension(6) :: fail

    real(kind = wp)       :: dzfrc0

    logical, save         :: do_alloc = .true.

    dzfrc0 = 0.0_wp

    if( nbins /= nbins_max ) then

        call deallocate_MFA_arrays()

        !AB: make sure the number of bins is odd (allow one extra bin)!
        nbins_max = (nbins-1)/2
        nbins_max = 2*nbins_max+1

        do_alloc  = .true.

    end if

    if( nbins_max > maxbins ) then

        call warning(22)

        if( master ) &
            write(uout,'(1x,2(a,i10))') '                : ',nbins_max,' -> ',maxbins
            !write(uout,'(/,/,1x,2(a,i10))') '                : ',nbins_max,' -> ',maxbins

        nbins_max = maxbins

    end if

    if( do_alloc ) then

        fail = 0

        if (.not.allocated(vinvs)) allocate( vinvs(nconfs), stat = fail(1) )

        if ( any(fail > 0) ) call error (31)

        allocate( zden_chpos(0:nbins_max,nconfs), stat = fail(1) )
        allocate( zden_chneg(0:nbins_max,nconfs), stat = fail(2) )
        allocate( charge_pos(0:nbins_max,nconfs), stat = fail(3) )
        allocate( charge_neg(0:nbins_max,nconfs), stat = fail(4) )
        allocate( charge_new(0:nbins_max,nconfs), stat = fail(5) )
        allocate( charge_old(0:nbins_max,nconfs), stat = fail(6) )

        if( any(fail > 0) ) call error (31)

        allocate( charge_mfa(0:nbins_max,nconfs), stat = fail(1) )
        allocate( pot_mfa(0:nbins_max,nconfs),  stat = fail(2) )
        allocate( pot_corr(0:nbins_max,nconfs), stat = fail(3) )
        allocate( pot_cora(0:nbins_max,nconfs), stat = fail(3) )
        allocate( pot_new(0:nbins_max,nconfs),  stat = fail(4) )
        allocate( pot_old(0:nbins_max,nconfs),  stat = fail(5) )
        allocate( nsamples_mfa(nconfs),  stat = fail(6) )

        if( any(fail > 0) ) call error (33)

        charge_old = 0.0_wp
        pot_old    = 0.0_wp

    end if

    do_alloc = .false.

    vinvs = 0.0_wp

    zden_chpos = 0.0_wp
    zden_chneg = 0.0_wp

    charge_pos = 0.0_wp
    charge_neg = 0.0_wp
    charge_new = 0.0_wp
    !charge_old = 0.0_wp

    charge_mfa = 0.0_wp
    pot_mfa    = 0.0_wp
    pot_corr   = 0.0_wp
    pot_cora   = 0.0_wp
    pot_new    = 0.0_wp
    !pot_old    = 0.0_wp

    nsamples_mfa = 0

!AB: assume the same max number of bins in all replicas

    dzfrc0 = 1.0_wp/real(nbins_max-1,wp)
    deltaz = zdimax/real(nbins_max-1,wp)

    if( abs(dzfrc0-dzfrac) > 1.0e-10_wp ) then

        call warning(25)

        if( master ) &
            write(uout,'(1x,2(a,f10.6))') '                : ',dzfrac,' -> ',dzfrc0

        dzfrac = dzfrc0

    end if

!    if( mfa_type > 0 ) then
!        call read_mfa_data(1,nbins_max)
!        call read_zch_dens(1,nbins_max)
!
!        do ic=2,nconfs
!           pot_corr(ic,:)   = pot_corr(1,:)
!           charge_new(ic,:) = charge_new(1,:)
!           charge_old(ic,:) = charge_old(1,:)
!        end do
!    end if

end subroutine


!> de-allocating arrays after MFA calculus on `Z`-grid (for all replicas)
subroutine deallocate_MFA_arrays()

    implicit none

    integer, dimension(6) :: fail

    fail = 0

    if (allocated(vinvs)) deallocate( vinvs, stat = fail(1) )
    if (allocated(zdims)) deallocate( zdims, stat = fail(2) )
    if (allocated(nbinz)) deallocate( nbinz, stat = fail(3) )

    if ( any(fail > 0) ) call error (32)

    if (allocated(zden_chpos)) deallocate( zden_chpos, stat = fail(1) )
    if (allocated(zden_chneg)) deallocate( zden_chneg, stat = fail(2) )
    if (allocated(charge_pos)) deallocate( charge_pos, stat = fail(3) )
    if (allocated(charge_neg)) deallocate( charge_neg, stat = fail(4) )
    if (allocated(charge_new)) deallocate( charge_new, stat = fail(5) )
    if (allocated(charge_old)) deallocate( charge_old, stat = fail(6) )

    if ( any(fail > 0) ) call error (32)

    if (allocated(charge_mfa)) deallocate( charge_mfa, stat = fail(1) )
    if (allocated(pot_mfa))  deallocate( pot_mfa,  stat = fail(2) )
    if (allocated(pot_corr)) deallocate( pot_corr, stat = fail(3) )
    if (allocated(pot_cora)) deallocate( pot_cora, stat = fail(3) )
    if (allocated(pot_new))  deallocate( pot_new,  stat = fail(4) )
    if (allocated(pot_old))  deallocate( pot_old,  stat = fail(5) )

    if ( any(fail > 0) ) call error (34)

end subroutine


!> read in ZMFAPOT
subroutine read_mfa_data(ib,nbins,beta)

    use constants_module     ! due to module dependence
    use parallel_loop_module ! due to module dependence

    use parse_module, only : get_line, get_word, word_2_real
    use comms_mpi_module, only : gcheck, master

    integer, intent(in) :: ib, nbins

    real(kind = wp), intent(in) :: beta

    real(kind=wp) :: bin_in, mfa_in !, chd_in

    integer :: kinp, kerr
    integer :: umfa!, uchd

    character :: record*80, word1*40, word2*40

    logical :: safe, is_float

    umfa = 40

    safe = .false.

    if( master ) then
        open( umfa, file='ZMFAPOT', status='old', err=999 )
        safe = .true.
        call gcheck(safe)
    else
        safe = .true.
        call gcheck(safe)
        if( .not.safe ) goto 999
    endif

    write(word1,'(i4)')nbins

    call cry(uout,"(1x,a,/,1x,100('-'))", &
             "reading MFA input file 'ZMFAPOT'"//&
             "' - two columns with expected number of rows (bins): "//trim(word1),0)

    !beta = 1.0_wp / (job % systemp * BOLTZMAN)

    kinp = 0
    kerr = 0

    do while( safe .and. kinp < nbins )

       call get_line(safe, umfa, record)
       if (.not.safe) then 

           if( master .and. kinp < nbins) &
               write(uout,"(1x,a,i4,a,/,1x,100('-'))") &
                     "WARNING: could only read ZMFAPOT input up to row (bin): ",kinp, &
                     " - padding the rest with zeros!"

           exit

       end if

       call get_word(record, word1)

       if( word1 == "" .or. word1(1:1) == "#" ) cycle

       call get_word(record, word2)

       bin_in = word_2_real(word1)
       mfa_in = word_2_real(word2)

       pot_corr(kinp,ib) = mfa_in/beta

       kinp = kinp+1

    end do
    pot_corr(nbins_max,ib) = pot_corr(nbins_max-1,ib)

    if( master ) close(umfa)

    !if( (nbins-kinp)>3 ) &
    if( kinp < 2 ) &
        call cry(uout,'', &
                 "ERROR: MFA input file 'ZMFAPOT' contains insufficient data!!!",999)

    return

999 if( master ) call gcheck(safe)

    call cry(uout,'', &
             "ERROR: failed reading MFA input file 'ZMFAPOT'!!!",999)

    STOP

    return

end subroutine read_mfa_data


!> read in ZCHARGE
subroutine read_zch_dens(ib,nbins)

    use constants_module     ! due to module dependence
    use parallel_loop_module ! due to module dependence

    use parse_module, only : get_line, get_word, word_2_real
    use comms_mpi_module, only : gcheck, master

    integer, intent(in) :: ib, nbins

    real(kind=wp) :: bin_in, mfa_in, chd_in

    integer :: kinp, kerr
    integer :: uchd

    character :: record*80, word1*40, word2*40

    logical :: safe, is_float

    if( damp_mfa < 1.e-10_wp ) return

    uchd = 41

    safe = .false.

    if( master ) then
        open( uchd, file='ZCHARGE', status='old', err=999 )
        safe = .true.
        call gcheck(safe)
    else
        safe = .true.
        call gcheck(safe)
        if( .not.safe ) goto 999
    endif

    write(word1,'(i4)')nbins

    call cry(uout,"(1x,a,/,1x,100('-'))", &
             "reading Z-charge input file 'ZCHARGE"//&
             "' - two columns with expected number of rows (bins): "//trim(word1),0)

    kinp = 0
    kerr = 0

    do while( safe .and. kinp < nbins )

       call get_line(safe, uchd, record)
       if (.not.safe) then 

           if( master .and. kinp < nbins) &
               write(uout,"(1x,a,i4,a,/,1x,100('-'))") &
                     "WARNING: could only read ZCHARGE input up to row (bin): ",kinp, &
                     " - padding the rest with zeros!"

           exit

       end if

       call get_word(record, word1)

       if( word1 == "" .or. word1(1:1) == "#" ) cycle

       call get_word(record, word2)

       kinp = kinp+1

       bin_in = word_2_real(word1)
       chd_in = word_2_real(word2)

       charge_old(kinp,ib) = chd_in
       charge_new(kinp,ib) = chd_in

       call get_word(record, word2)
       chd_in = word_2_real(word2)
       charge_pos(kinp,ib) = chd_in

       call get_word(record, word2)
       chd_in = word_2_real(word2)
       charge_neg(kinp,ib) = chd_in

    end do

    if( master ) close(uchd)

    !if( (nbins-kinp)>3 ) &
    if( kinp < 2 ) &
        call cry(uout,'', &
                 "ERROR: Z-charge input file 'ZCHARGE' contains insufficient data!!!",999)

    return

999 if( master ) call gcheck(safe)

    call cry(uout,'', &
             "ERROR: failed reading Z-charge input file 'ZCHARGE'!!!",999)

    STOP

    return

end subroutine read_zch_dens


!> accumulating samples for charge Z-density profiles (for MFA)
subroutine sample_MFA_charge_zdens(ib,cfgs,vol)

    use kinds_f90
    use config_type
    use constants_module, only : uout, HALF, DHALF
    use latticevectors_module, only : cart_to_frac, frac_to_cart

    implicit none

        !> index of configuration
    integer, intent(in) :: ib

        !> configuration to work on
    type(config), intent(inout) :: cfgs(:)

    real(kind = wp), intent(in) :: vol

    real(kind = wp) :: tpos,charge
    integer :: i, im, n, nbins, typ

    call cart_to_frac(cfgs(ib))

    nbins = nbins_max-1

    do im = 1, cfgs(ib)%num_mols

        do i = 1 , cfgs(ib)%mols(im)%natom

            charge = cfgs(ib)%mols(im)%atms(i)%charge

            if( charge == 0.0_wp ) cycle

            tpos = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            !AB: applying PBC
            if( abs(tpos) > HALF ) tpos = tpos - anint(tpos)

            tpos = tpos + HALF

            n = int( tpos * real(nbins,wp) ) + 1

#ifdef DEBUG
            !AB: debugging
            if( n < 1 ) then
                call cry(uout,'',"WARNING: charge Z-bin outside the cell, < 0",0)
                n = nbins_max
            else if( n > nbins ) then
                call cry(uout,'',"WARNING: charge Z-bin outside the cell, > nbins (+1)",0)
                n = nbins_max
            else
                charge_new(n,ib) = charge_new(n,ib) + 1.0_wp
            end if
#else
            if( n < 1 .or. n > nbins ) n = nbins_max
#endif

            if( charge > 0.0_wp ) then
                zden_chpos(n,ib) = zden_chpos(n,ib) + 1.0_wp
                charge_pos(n,ib) = charge_pos(n,ib) + charge
            else
                zden_chneg(n,ib) = zden_chneg(n,ib) + 1.0_wp
                charge_neg(n,ib) = charge_neg(n,ib) - charge
            end if

        end do

    end do

    vinvs(ib) = vinvs(ib) + 1.0_wp/vol

    nsamples_mfa(ib) = nsamples_mfa(ib) + 1

#ifdef DEBUG
    !AB: debugging
    if( nsamples_mfa(ib) == huge(nsamples_mfa(ib)) ) &
        call cry(uout,'(/,1x,a)', &
             "ERROR: 'integer' overflow - number of charge Z-density samples > upper bound for 'integer'!!!",999)
#endif

    call frac_to_cart(cfgs(ib))

end subroutine

!> incrementing charge Z-density histograms (for MFA)
subroutine add_MFA_charge_zdens(ib,nbins,zpos,charge)

    use kinds_f90
    use constants_module, only : uout, HALF, DHALF

    implicit none

    integer, intent(in)         :: ib,nbins
    real(kind = wp), intent(in) :: zpos,charge

    integer :: n

    n = int(zpos * real(nbins,wp) ) + 1

    if( n < 1 .or. n > nbins ) n = nbins_max

    if( charge > 0.0_wp ) then
        zden_chpos(n,ib) = zden_chpos(n,ib) + 1.0_wp
        charge_pos(n,ib) = charge_pos(n,ib) + charge
    else
        zden_chneg(n,ib) = zden_chneg(n,ib) + 1.0_wp
        charge_neg(n,ib) = charge_neg(n,ib) - charge
    end if

end subroutine


subroutine print_MFA_charge_zdens(job, cfgs, ib, iter, stats)

    use kinds_f90
    use control_type
    use config_type
    use constants_module, only : uout, uzch, HALF, BOLTZMAN, CTOINTERNAL
    use species_module, only : number_of_elements, element
    use latticevectors_module, only : dcell
    use parallel_loop_module, only : open_nodefiles
    use statistics_type

    implicit none

        !> configuration to work on
    type(control), intent(in)   :: job
    type(config), intent(inout) :: cfgs(:)
    integer, intent(in)         :: ib, iter

    type(statistics), intent(in) :: stats

    real(kind = wp) :: dens(number_of_elements)
    real(kind = wp) :: celprp(10), ChCurZ(nbins_max)
    real(kind = wp) :: beta, blng, numtyp
    real(kind = wp) :: zz, tmp1, tmp2, gsum1, gsum2, gsum, qsum1, qsum2, qsum, vol

    integer :: nsamples, typ, nbins, ierr, i, j, n

    logical :: is_neutral

    character(4) :: atype
    character(8) :: chbox

    write(chbox,'(i8)')ib

    !AB: this is not quite correct in 3D NPT due to possible large variations in Z-dimension of the cell

    call dcell(cfgs(ib)%vec%latvector, celprp)
    vol = celprp(10)

    if( iter == 0 ) then
        call open_nodefiles('ZCHARGE_INI-'//trim(adjustL(chbox)), uzch, ierr)

        write(uzch,"('# ',a,/,'# Charge densities along Z axis in cell ',i4,'   at MC step ',i10)") &
              cfgs(ib)%cfg_title, ib,iter

        if( damp_mfa < 1.0e-10_wp ) call sample_MFA_charge_zdens(ib,cfgs,vol)
    else
        call open_nodefiles('ZCHARGE-'//trim(adjustL(chbox)), uzch, ierr)

        write(uzch,"('# ',a,/,'# Charge densities along Z axis in cell ',i4,'   at MC step ',i10)") &
              cfgs(ib)%cfg_title, ib,iter

        if( nsamples_mfa(ib) == 0 ) then

            write(uzch,"('#',/,'# No data - number of samples taken is zero')")
            close(uzch)

            return
        end if
    end if

    nsamples = nsamples_mfa(ib)
    if( nsamples == 0 ) nsamples = 1

    write(uzch,"(a,i10,2f24.10)")'# Grid: ',nbins_max-1,celprp(9),dzfrac*celprp(9)

    if( vinvs(ib) > 0.0_wp ) then

        vol = real(nsamples,wp) / vinvs(ib)

    else if( stats%nsample > 0 ) then

        vol = stats%aveeng(13) / real(stats%nsample,wp)

    end if

    dens  = 0.0_wp
    gsum1 = 0.0_wp
    gsum2 = 0.0_wp
    qsum1 = 0.0_wp
    qsum2 = 0.0_wp
    do i = 1, cfgs(ib)%num_mols
       do j = 1, cfgs(ib)%mols(i)%natom

          typ = cfgs(ib)%mols(i)%atms(j)%atlabel
          dens(typ) = dens(typ) + 1.0_wp

          if( cfgs(ib)%mols(i)%atms(j)%charge > 0.0_wp ) then

              gsum1 = gsum1  + 1
              qsum1 = qsum1  + cfgs(ib)%mols(i)%atms(j)%charge

          else if( cfgs(ib)%mols(i)%atms(j)%charge < 0.0_wp ) then

              gsum2 = gsum2  + 1
              qsum2 = qsum2  - cfgs(ib)%mols(i)%atms(j)%charge

          end if

       end do
    end do

    do typ = 1, number_of_elements

       if( stats%nsamp_typ > 1 ) then
           numtyp = dens(typ)
       else 
           numtyp = stats%ave_nspc(typ) / real(stats%nsamp_typ,wp)
       end if
 
       dens(typ) = numtyp / vol

       write(uzch,"('#',/,'# Number density for element',2x,a8,' (',i4,') : ',e15.7,' = ',f15.5,' / ',e15.7)") &
             element(typ),typ,dens(typ),numtyp,vol

    end do

    write(uzch,"(/,'# Charge Z-densities: total, positive & negative')")

    ChCurZ(:) = 0.0_wp

    nbins = nbins_max-1

    beta = 1.0_wp / (job % systemp * BOLTZMAN)
    blng = CTOINTERNAL*beta / job%dielec

    qsum = qsum1-qsum2

    is_neutral = ( abs((SCD1+SCD2)*celprp(10)/celprp(9)+qsum) < 1.0e-10_wp )

    if( iter == 0 .and. .not.is_neutral ) then

        write(uout,'(/,1x,5(a,e15.7))')"Check: Area*(SCD1+SCD2) + Qsum = ",celprp(10)/celprp(9),&
             " * (",SCD1," + ",SCD2,") + ",qsum," = ",(SCD1+SCD2)*celprp(10)/celprp(9)+qsum

        call cry(uout,'',"WARNING: Charges do not sum up to zero !!!",0)
        !call cry(uout,'',"ERROR: Charges do not sum up to zero !!!",999)

    end if

    tmp1 = qsum1 / real(nbins,wp)
    tmp2 = qsum2 / real(nbins,wp)
    gsum = 0.0_wp

    zz = -HALF

    do n = 1, nbins

       if( iter == 0 ) then

           if( mfa_type < 0 ) then

               charge_new(n,ib) = tmp1 - tmp2

           else

               tmp1 = charge_pos(n,ib)
               tmp2 = charge_neg(n,ib)

               charge_pos(n,ib) = 0.0_wp
               charge_neg(n,ib) = 0.0_wp

           end if

       else

           tmp1 = charge_pos(n,ib) / real(nsamples,wp)
           tmp2 = charge_neg(n,ib) / real(nsamples,wp)

           charge_new(n,ib) = tmp1 - tmp2

       end if

       gsum = gsum + tmp1-tmp2

       write(uzch,"(5(' ',f15.7))") (zz + HALF*dzfrac)*celprp(9), tmp1-tmp2, tmp1, tmp2, gsum

       zz = zz + dzfrac

    end do

    close(uzch)

    if( abs(gsum - qsum) > 1.0e-10_wp ) then

        write(uout,'(/,/,1x,3(a,f15.7))') &
              'case of 2D-slit - Check the charge sums: n_tot = n_pos + n_neg = ', &
              gsum,' =?= ',qsum1, ' + ',-qsum2

        if( mfa_type < 0 .and. abs(gsum - qsum) > 1.0e-5_wp ) &
            call cry(uout,'',"ERROR: Charge density sums are inconsistent !!!",999)

        if( zden_chpos(nbins_max,ib) > 0 ) &
            call cry(uout,'',"WARNING: Positive charge Z-density data outside the cell !!!",0)

        if( zden_chneg(nbins_max,ib) > 0 ) &
            call cry(uout,'',"WARNING: Negative charge Z-density data outside the cell !!!",0)

    end if

    if( iter > 0 ) then
        call update_MFA_potential(job, cfgs, ib, iter, celprp, vol)
    else if( damp_mfa > 0.0_wp ) then
        call update_MFA_potential(job, cfgs, ib, iter, celprp, vol)
    end if

end subroutine


subroutine update_MFA_potential(job, cfgs, ib, iter, celprp, vol)

    use kinds_f90
    use control_type
    use config_type
    use constants_module, only : BOLTZMAN, CTOINTERNAL, INTERNALTOEV, PI, TWOPI, HALF, uout, uzch
    use latticevectors_module, only : dcell
    use parallel_loop_module,  only : open_nodefiles

    implicit none

        !> configuration to work on
    type(control), intent(in)   :: job
    type(config), intent(in)    :: cfgs(:)
    integer, intent(in)         :: ib, iter
    real(kind = wp), intent(in) :: celprp(:)
    real(kind = wp), intent(in) :: vol

    real(kind = wp), dimension(1:nbins_max) :: ChCurZ
    real(kind = wp), dimension(0:nbins_max) :: Umfa0
    real(kind = wp), dimension(0:nbins_max) :: Umfa1
    real(kind = wp), dimension(0:nbins_max) :: Umfa2

    real(kind = wp) :: beta, blng, ch_damp, cden
    real(kind = wp) :: UextW, UextA, UextC, UextT, UmfaM0, UmfaM1, UmfaM2, ChDist
    real(kind = wp) :: PotOld, PotNew, PotMFA
    real(kind = wp) :: SumPtOld, SumPtNew, SumPtMFA, SumPtMFA2, SumDpot1, SumDpot2
    real(kind = wp) :: xdim, ydim, zdim, dzdim, zcart, zfrac, ldim, rcut2

    integer :: i, j, n, nbins, nbinh, ierr
    logical :: is_symmetric
    character(8) :: chbox

    integer, save :: mfa_cycle = 0

    write(chbox,'(i8)')ib
    
    !chbox = trim(adjustL(chbox))

    if( mfa_cycle == mfa_ncycles ) mfa_type = abs(mfa_type)

    if( ib == 1 ) mfa_cycle = mfa_cycle + 1

    beta = 1.0_wp / (job % systemp * BOLTZMAN)
    blng = CTOINTERNAL*beta / job%dielec

    is_symmetric = ( abs(SCD1-SCD2) < 1.0e-10_wp )
    !is_symmetric = ( walls_charged > 1 .and. abs(SCD1-SCD2) < 1.0e-10_wp )

    xdim  = celprp(7)
    ydim  = celprp(8)
    zdim  = celprp(9)
    dzdim = dzfrac*zdim

    rcut2 = job%shortrangecut**2
    ldim  = min(xdim,ydim)
    nbins = nbins_max - 1 

    ch_damp = abs(damp_mfa)
    if( mfa_cycle <= mfa_ncycles ) then 

        do n = 1, nbins
           ChCurZ(n) = (charge_new(n,ib)*(1.0_wp-ch_damp) &
                       +charge_old(n,ib)*ch_damp)*zdim/(dzdim*vol)
           !ChCurZ(n) = (charge_new(n,ib)*(1.0_wp-damp_mfa) &
           !            +charge_old(n,ib)*damp_mfa)*zdim/(dzdim*vol)
        end do

#ifdef DEBUG
        write(uout,'(/,/,1x,2(a,f15.7),i6)') &
             'case of 2D-slit - MFA charge mixing: new * ',(1.0_wp-damp_mfa), &
                                                 ' old * ',damp_mfa,ib
#endif

    else

        do n = 1, nbins
           ChCurZ(n) = charge_new(n,ib)*zdim/(dzdim*vol)
        end do

#ifdef DEBUG
        write(uout,'(/,/,1x,a,i6)') &
             'case of 2D-slit - no MFA charge mixing',ib
#endif

    end if

    if( iter == 0 ) then

        call open_nodefiles('ZMFAPOT_INI-'//trim(adjustL(chbox)), uzch, ierr)

        write(uzch,"('# ',a,/,'# MFA potential along Z axis in cell ',i4,'   at MC step ',2i10)") &
              cfgs(ib)%cfg_title, ib,iter,mfa_cycle

    else if( mfa_cycle <= mfa_ncycles ) then

        call open_nodefiles('ZMFAPOT-'//trim(adjustL(chbox)), uzch, ierr)

        write(uzch,"('# ',a,/,'# MFA potential along Z axis in cell ',i4,'   at MC step ',2i10)") &
              cfgs(ib)%cfg_title, ib,iter,mfa_cycle

        if( nsamples_mfa(ib) == 0 ) then

            write(uzch,"('#',/,'# No data - number of samples taken is zero')")
            close(uzch)

            return
        end if

    else 

        call open_nodefiles('ZMFAPOT_AVR-'//trim(adjustL(chbox)), uzch, ierr)

        write(uzch,"('# ',a,/,'# MFA potential along Z axis in cell ',i4,'   at MC step ',2i10)") &
              cfgs(ib)%cfg_title, ib,iter,mfa_cycle

        if( nsamples_mfa(ib) == 0 ) then

            write(uzch,"('#',/,'# No data - number of samples taken is zero')")
            close(uzch)

            return
        end if

    end if

    write(uzch,"(a,i10,2f24.10)")'# Grid: ',nbins,zdim,dzfrac*zdim

    write(uzch,"(/,'# MFA Coulomb Z-profiles: total correction & charge (no SCD)')")

    UextW = 0.0_wp
    UextA = 0.0_wp
    UextC = 0.0_wp
    UextT = 0.0_wp

    UmfaM0 = 0.0_wp
    UmfaM1 = 0.0_wp
    UmfaM2 = 0.0_wp

    SumPtOld = 0.0_wp
    SumPtNew = 0.0_wp
    SumPtMFA = 0.0_wp
    SumPtMFA2= 0.0_wp
    SumDpot1 = 0.0_wp
    SumDpot2 = 0.0_wp

    Umfa0(:) = 0.0_wp
    Umfa1(:) = 0.0_wp
    Umfa2(:) = 0.0_wp

    zcart = -dzdim
    zfrac = -HALF*(1.0_wp+dzfrac)

    do n = 0, nbins

       zfrac = zfrac + dzfrac
       zcart = zcart + dzdim

       UextW = -TWOPI*(SCD1*zcart+SCD2*(zdim-zcart))

       if( is_symmetric ) then !AB: symmetrised version

         if( abs(mfa_type) < 2 ) then !AB: XY-box correction
             UextA = UelscdFs(zcart,dzdim,zdim,ChCurZ,nbins)*dzdim
             UextC = UelsxcFs(zcart,dzdim,zdim,ldim,ChCurZ,nbins)*dzdim
         else !AB: cylindrical correction
             !UextA = UelscdFCs(zcart,dzdim,zdim,rcut2,ChCurZ,nbins)*dzdim 
             UextA = UelscdFs(zcart,dzdim,zdim,ChCurZ,nbins)*dzdim
             UextC = UelscdFCs(zcart,dzdim,zdim,rcut2,ChCurZ,nbins)*dzdim-UextA
         end if

       else !AB: non-symmetrised version

         if( abs(mfa_type) < 2 ) then !AB: XY-box correction
             UextA = UelscdF(zcart,dzdim,zdim,ChCurZ,nbins)*dzdim
             UextC = UelsxcF(zcart,dzdim,ldim,ChCurZ,nbins)*dzdim
         else !AB: cylindrical correction
             !UextA = UelscdFC(zcart,dzdim,zdim,rcut2,ChCurZ,nbins)*dzdim 
             UextA = UelscdF(zcart,dzdim,zdim,ChCurZ,nbins)*dzdim
             UextC = UelscdFC(zcart,dzdim,zdim,rcut2,ChCurZ,nbins)*dzdim-UextA
         end if

       end if

       UextT = UextA+UextC+UextW

       Umfa0(n) = blng*(UextT-UextC)
       Umfa1(n) = UextT
       Umfa2(n) = UextC

       UextA = UextA*blng
       UextC = UextC*blng

       ChDist = 0.0_wp
       if( n > 0 ) then 
           ChDist = ChCurZ(n)*dzdim*vol/zdim
           charge_old(n,ib) = charge_new(n,ib)
       end if

    end do

    UmfaM0 = 0.5_wp*( Umfa0(nbins/2) + Umfa0((nbins+1)/2) )
    UmfaM1 = 0.5_wp*( Umfa1(nbins/2) + Umfa1((nbins+1)/2) )
    UmfaM2 = 0.5_wp*( Umfa2(nbins/2) + Umfa2((nbins+1)/2) )

    zcart = -dzdim
    zfrac = -HALF*(1.0_wp+dzfrac)

    do n = 0, nbins

       zfrac = zfrac + dzfrac
       zcart = zcart + dzdim

       ChDist = 0.0_wp
       if( n > 0 ) then 
           ChDist = ChCurZ(n)*dzdim*vol/zdim
           charge_old(n,ib) = charge_new(n,ib)
       end if

       Umfa0(n) = Umfa0(n)-UmfaM0
       Umfa1(n) = Umfa1(n)-UmfaM1
       Umfa2(n) = Umfa2(n)-UmfaM2

       PotMFA = pot_mfa(n,ib)
       PotNew = Umfa0(n)
       PotOld = pot_new(n,ib)
       pot_old(n,ib) = PotOld
       pot_new(n,ib) = PotNew

       !AB: estimate deviation & variance:
       SumPtOld = SumPtOld + PotOld**2
       SumPtNew = SumPtNew + PotNew**2
       SumPtMFA = SumPtMFA + abs(PotMFA)
       SumPtMFA2= SumPtMFA2+ PotNew**2-PotMFA**2
       SumDpot1 = SumDpot1 + (PotNew-PotMFA)**2
       SumDpot2 = SumDpot2 + (PotNew-PotOld)**2

       if( mfa_cycle > mfa_ncycles ) then
       !if( mfa_type > 0 ) then

           charge_mfa(n,ib) = (charge_mfa(n,ib)*real(mfa_cycle-mfa_ncycles-1,wp) &
                            + ChDist)/real(mfa_cycle-mfa_ncycles,wp)

           pot_corr(n,ib) = Umfa1(n)*CTOINTERNAL/job%dielec
           pot_cora(n,ib) = (pot_cora(n,ib)*real(mfa_cycle-mfa_ncycles-1,wp) &
                          + Umfa1(n)*CTOINTERNAL/job%dielec)/real(mfa_cycle-mfa_ncycles,wp)

           pot_mfa(n,ib)  = (pot_mfa(n,ib)*real(mfa_cycle-mfa_ncycles-1,wp) &
                          + Umfa0(n))/real(mfa_cycle-mfa_ncycles,wp)

           write(uzch,'(7e15.7)') zcart, beta*pot_corr(n,ib), beta*pot_cora(n,ib), &
                                  Umfa0(n), pot_mfa(n,ib), ChDist, charge_mfa(n,ib)

       else !if( mfa_type < 0 ) then

           charge_mfa(n,ib) = (charge_mfa(n,ib)*real(mfa_cycle-1,wp)+ChDist)/real(mfa_cycle,wp)

           pot_corr(n,ib) = Umfa1(n)*CTOINTERNAL/job%dielec
           pot_cora(n,ib) = (pot_cora(n,ib)*real(mfa_cycle-1,wp) &
                          + Umfa1(n)*CTOINTERNAL/job%dielec)/real(mfa_cycle,wp)

           pot_mfa(n,ib)  = (pot_mfa(n,ib)*real(mfa_cycle-1,wp) + Umfa0(n))/real(mfa_cycle,wp) 

           write(uzch,'(7e15.7)') zcart, beta*pot_corr(n,ib), beta*pot_cora(n,ib), &
                                  Umfa0(n), pot_mfa(n,ib), ChDist, charge_mfa(n,ib)

       end if

    end do
    pot_corr(nbins_max,ib) = pot_corr(nbins_max-1,ib)

    Umfa0(nbins_max) = Umfa0(nbins_max-1)
    Umfa1(nbins_max) = Umfa1(nbins_max-1)
    Umfa2(nbins_max) = Umfa2(nbins_max-1)

    close(uzch)

    if( mfa_cycle <= mfa_ncycles .and. mod(iter,job%sysprint) == 0 ) then

        nsamples_mfa(ib) = 0
        vinvs(ib)        = 0.0_wp

        zden_chpos(:,ib) = 0.0_wp
        zden_chneg(:,ib) = 0.0_wp
        charge_pos(:,ib) = 0.0_wp
        charge_neg(:,ib) = 0.0_wp

    end if

    if( mfa_cycle > 1 ) then

        if( iter > 0 .and. (SumPtMFA > 1.0e-5_wp) ) write(uout,'(/,/,1x,2(a,2e15.7),3i6)') &
        '2D-slit - MFA convergence: var/sgm = ',sqrt(SumDpot1/abs(SumPtMFA2)) &
                                               ,sqrt(SumDpot2/abs(SumPtNew-SumPtOld)) &
                                ,', var/avr = ',sqrt(SumDpot1)/SumPtMFA &
                                               ,sqrt(SumDpot2)/SumPtMFA &
                                               ,mfa_cycle,mfa_type,ib

        blng = CTOINTERNAL*INTERNALTOEV/(blng*job%dielec)

        write(uout,'(/,1x,a,4e15.7,i6)') '2D-slit - running MFA voltages (V): ', &
             Umfa0(0)*blng, Umfa0(nbins)*blng, (Umfa0(nbins)-Umfa0(0))*blng, &
             (Umfa0(nbins*2/3)-Umfa0(nbins/3))*blng,ib

        write(uout,'(/,1x,a,4e15.7,i6)') '2D-slit - average MFA voltages (V): ', &
             (pot_mfa(0,ib)-pot_mfa(nbins/2,ib))*blng, &
             (pot_mfa(nbins,ib)-pot_mfa(nbins/2,ib))*blng, &
             (pot_mfa(nbins,ib)-pot_mfa(0,ib))*blng, &
             (pot_mfa(nbins*2/3,ib)-pot_mfa(nbins/3,ib))*blng,ib

    end if

end subroutine


function uel_slit(ib,zcrt,cell_zz)

    !use kinds_f90
    use constants_module, only : TWOPI, HALF, DHALF, uout

    integer, intent(in) :: ib
    real(kind = wp), intent(in) :: zcrt,cell_zz

    real(kind = wp) :: uel_slit
    real(kind = wp) :: zzz
    integer :: nzz

    uel_slit = 0.0_wp

    if( mfa_type /= 0 ) then

        zzz = (zcrt/cell_zz+DHALF)*real(nbins_max-1,wp)
        nzz = int(zzz)+1

        if( nzz < 0 .or. nzz > nbins_max-1 ) &
            write(uout,'(/,/,1x,a,i10,3e15.7)') &
                 'WARNING: got outside the slit in Uel_slit(): bin index = ',nzz,zcrt,cell_zz,zzz

        uel_slit = ( pot_corr(nzz,ib) + (zzz-float(nzz))*(pot_corr(nzz+1,ib) - pot_corr(nzz,ib)) )

    else if( walls_charged > 0 ) then

        zzz = zcrt+cell_zz*DHALF
        uel_slit = -SCD1*zzz &
                   -SCD2*(cell_zz-zzz)

        uel_slit = uel_slit*TWOPI

    endif

end function

function UelscdF(Hz,dHz,cell_zz,SCDz,Nlay)

!AB: MFA for the electrostatic potential in Hz (A) position 
!AB: due to the total charge distribution in the system

    !use kinds_f90
    use constants_module, only : TWOPI

    real(kind = wp), intent(in) :: Hz,dHz,cell_zz
    real(kind = wp), dimension(:), intent(in) :: SCDz
    integer, intent(in) :: Nlay

    real(kind = wp) :: UelscdF
    real(kind = wp) :: Zcri, dZcr, Uelc
    integer :: i

    UelscdF = 0.0_wp

    Zcri =-dHz*0.5_wp
    dZcr = 0.0_wp;

    do i = 1, Nlay

       Zcri = Zcri+dHz
       dZcr = Hz-Zcri

       UelscdF = UelscdF-SCDz(i)*TWOPI*dabs(dZcr)

    end do

end function

function UelscdFC(Hz,dHz,cell_zz,rcut2,SCDz,Nlay)

!AB: MFA for the electrostatic potential in Hz (A) position 
!AB: due to the total charge distribution in the system

    !use kinds_f90
    use constants_module, only : TWOPI

    real(kind = wp), intent(in) :: Hz,dHz,cell_zz,rcut2
    real(kind = wp), dimension(:), intent(in) :: SCDz
    integer, intent(in) :: Nlay

    real(kind = wp) :: UelscdFC
    real(kind = wp) :: Zcri, dZcr, Uelc
    integer :: i

    UelscdFC = 0.0_wp

    Zcri =-dHz*0.5_wp
    dZcr = 0.0_wp;

    do i = 1, Nlay

       Zcri = Zcri+dHz
       dZcr = Hz-Zcri

       UelscdFC = UelscdFC-SCDz(i)*TWOPI*sqrt(rcut2+dZcr*dZcr)

    end do

end function

function UelscdFs(Hz,dHz,cell_zz,SCDz,Nlay)

!AB: Symmetrised MFA for the electrostatic potential in Hz (A) position 
!AB: due to the total charge distribution in the system

    !use kinds_f90
    use constants_module, only : PI

    real(kind = wp), intent(in) :: Hz,dHz,cell_zz
    real(kind = wp), dimension(:), intent(in) :: SCDz
    integer, intent(in) :: Nlay

    real(kind = wp) :: UelscdFs
    real(kind = wp) :: Zcri, dZcr
    integer :: Nlp1, Nlo2, i, j

    UelscdFs = 0.0_wp

    Nlp1 = Nlay+1
    Nlo2 = Nlay/2
    Zcri =-dHz*0.5_wp
    dZcr = 0.0_wp;

    do i = 1, Nlo2
       j = Nlp1-i

       Zcri = Zcri+dHz
       dZcr = Hz-Zcri

       UelscdFs = UelscdFs-(SCDz(i)+SCDz(j))*PI*dabs(dZcr)

       dZcr = Hz-(cell_zz-Zcri)

       UelscdFs = UelscdFs-(SCDz(i)+SCDz(j))*PI*dabs(dZcr)

    end do

end function

function UelscdFCs(Hz,dHz,cell_zz,rcut2,SCDz,Nlay)

!AB: Symmetrised MFA for the electrostatic potential in Hz (A) position 
!AB: due to the total charge distribution in the system

    !use kinds_f90
    use constants_module, only : PI

    real(kind = wp), intent(in) :: Hz,dHz,cell_zz,rcut2
    real(kind = wp), dimension(:), intent(in) :: SCDz
    integer, intent(in) :: Nlay

    real(kind = wp) :: UelscdFCs
    real(kind = wp) :: Zcri, dZcr
    integer :: Nlp1, Nlo2, i, j

    UelscdFCs = 0.0_wp

    Nlp1 = Nlay+1
    Nlo2 = Nlay/2
    Zcri =-dHz*0.5_wp
    dZcr = 0.0_wp;

    do i = 1, Nlo2
       j = Nlp1-i

       Zcri = Zcri+dHz
       dZcr = Hz-Zcri

       UelscdFCs = UelscdFCs-(SCDz(i)+SCDz(j))*PI*sqrt(rcut2+dZcr*dZcr)

       dZcr = Hz-(cell_zz-Zcri)

       UelscdFCs = UelscdFCs-(SCDz(i)+SCDz(j))*PI*sqrt(rcut2+dZcr*dZcr)

    end do

end function

function UelxC(zb, cell_sh)

!AB: MFA in-box correction for the external els. potential from a planar layer
!AB: zb (A) - the z-distance to the layer from the current position

    !use kinds_f90

    real(kind = wp), intent(in) :: zb, cell_sh

    real(kind = wp) :: UelxC
    real(kind = wp) :: a, a2, zb2, apz, amz

    a   = cell_sh*0.5_wp
    a2  = a*a
    zb2 = zb*zb
    apz = a2+zb2
    amz = a2-zb2

    UelxC = 4.0_wp*abs(zb)*atan(a2/(zb*sqrt(a2+apz)))
    UelxC = UelxC-8.0_wp*a*dlog((sqrt(a2+apz)+a)/sqrt(apz))

end function

function UelsxcF(Hz,dHz,cell_sz,SCDz,Nlay)

!AB: MFA in-box correction for the electrostatic potential in Hz (A) position 
!AB: due to the total charge distribution in the system

    !use kinds_f90

    real(kind = wp), intent(in) :: Hz,dHz,cell_sz
    real(kind = wp), dimension(:), intent(in) :: SCDz
    integer, intent(in) :: Nlay

    real(kind = wp) :: UelsxcF
    real(kind = wp) :: Zcri, dZcr
    integer :: i

    UelsxcF = 0.0_wp

    Zcri =-dHz*0.5_wp
    dZcr = 0.0_wp;

    do i = 1, Nlay

       Zcri = Zcri+dHz
       dZcr = dabs(Hz-Zcri)

       UelsxcF = UelsxcF+SCDz(i)*UelxC(dZcr,cell_sz)

    end do

end function

function UelsxcFs(Hz,dHz,cell_zz,cell_sz,SCDz,Nlay)

!AB: Symmetrised MFA in-box correction for the electrostatic potential in Hz (A) position 
!AB: due to the total charge distribution in the system

    !use kinds_f90

    real(kind = wp), intent(in) :: Hz,dHz,cell_zz,cell_sz
    real(kind = wp), dimension(:), intent(in) :: SCDz
    integer, intent(in) :: Nlay

    real(kind = wp) :: UelsxcFs
    real(kind = wp) :: Zcri, dZcr
    integer :: Nlp1, Nlo2, i, j

    UelsxcFs = 0.0_wp

    Nlp1 = Nlay+1
    Nlo2 = Nlay/2
    Zcri =-dHz*0.5_wp
    dZcr = 0.0_wp;

    do i = 1, Nlo2
       j = Nlp1-i

       Zcri = Zcri+dHz
       dZcr = dabs(Hz-Zcri)

       UelsxcFs = UelsxcFs+(SCDz(i)+SCDz(j))*UelxC(dZcr,cell_sz)

       dZcr = dabs(Hz-(cell_zz-Zcri))

       UelsxcFs = UelsxcFs+(SCDz(i)+SCDz(j))*UelxC(dZcr,cell_sz)

    end do

    UelsxcFs = UelsxcFs*0.5_wp

end function

!> some other routine
!subroutine some_routine(params)
!
!end subroutine


end module
