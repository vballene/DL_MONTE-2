! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module threebodylist_type

    use kinds_f90

    implicit none

type threebodylist

    integer :: maxtrip  ! the maximum number of triplets per atom

    integer, dimension(:), allocatable :: numtrip  ! the number of angles for this atom
    integer, dimension(:,:,:), allocatable :: thb_triplets ! stores connected atoms and potential number

end type

end module
