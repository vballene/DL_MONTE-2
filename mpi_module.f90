! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   T.L.Underwood - Lattice/Phase-Switch MC method & optimizations        *
! *   t.l.Underwood[@]bath.ac.uk                                            *
! ***************************************************************************

!*!!!!!!!!!!!!!!!!!!!!!
! Originally based on !
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 module for faking MPI calls needed for serial compilation
!
! copyright - daresbury laboratory
! author    - i.t.todorov december 2005
!
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Module mpi_module

  Use kinds_f90

  Implicit None

  Public :: MPI_INIT, MPI_FINALIZE, MPI_ABORT, MPI_COMM_RANK, &
            MPI_COMM_SIZE, MPI_COMM_DUP, MPI_COMM_SPLIT,      &
            MPI_COMM_FREE, MPI_BARRIER, MPI_WAIT, MPI_WTIME,  &
            MPI_ALLREDUCE, MPI_BCAST, MPI_SEND, MPI_ISSEND,   &
            MPI_RECV, MPI_IRECV

  Interface MPI_ALLREDUCE
     Module Procedure MPI_ALLREDUCE_log_s
     Module Procedure MPI_ALLREDUCE_log_v
     Module Procedure MPI_ALLREDUCE_int_s
     Module Procedure MPI_ALLREDUCE_int_v
     Module Procedure MPI_ALLREDUCE_rwp_s
     Module Procedure MPI_ALLREDUCE_rwp_v
  End Interface !MPI_ALLREDUCE

  Interface MPI_BCAST
     Module Procedure MPI_BCAST_chr_s
     Module Procedure MPI_BCAST_chr_v
     Module Procedure MPI_BCAST_int_s
     Module Procedure MPI_BCAST_int_v
     Module Procedure MPI_BCAST_rwp_s
     Module Procedure MPI_BCAST_rwp_v
     Module Procedure MPI_BCAST_log_s
     !TU: I added the following procedure...
     Module Procedure MPI_BCAST_log_v
  End Interface !MPI_BCAST

  Interface MPI_SEND
     Module Procedure MPI_SEND_chr_s
     Module Procedure MPI_SEND_chr_v
     Module Procedure MPI_SEND_int_s
     Module Procedure MPI_SEND_int_v
     Module Procedure MPI_SEND_rwp_s
     Module Procedure MPI_SEND_rwp_v
     Module Procedure MPI_SEND_rwp_m
     Module Procedure MPI_SEND_rwp_c
     Module Procedure MPI_SEND_cwp_s
     Module Procedure MPI_SEND_cwp_v
     Module Procedure MPI_SEND_cwp_m
     Module Procedure MPI_SEND_cwp_c
  End Interface !MPI_SEND

  Interface MPI_ISSEND
     Module Procedure MPI_ISSEND_chr_s
     Module Procedure MPI_ISSEND_chr_v
     Module Procedure MPI_ISSEND_int_s
     Module Procedure MPI_ISSEND_int_v
     Module Procedure MPI_ISSEND_rwp_s
     Module Procedure MPI_ISSEND_rwp_v
     Module Procedure MPI_ISSEND_rwp_m
     Module Procedure MPI_ISSEND_rwp_c
     Module Procedure MPI_ISSEND_cwp_s
     Module Procedure MPI_ISSEND_cwp_v
     Module Procedure MPI_ISSEND_cwp_m
     Module Procedure MPI_ISSEND_cwp_c
  End Interface !MPI_ISSEND

  Interface MPI_RECV
     Module Procedure MPI_RECV_chr_s
     Module Procedure MPI_RECV_chr_v
     Module Procedure MPI_RECV_int_s
     Module Procedure MPI_RECV_int_v
     Module Procedure MPI_RECV_rwp_s
     Module Procedure MPI_RECV_rwp_v
     Module Procedure MPI_RECV_rwp_m
     Module Procedure MPI_RECV_rwp_c
     Module Procedure MPI_RECV_cwp_s
     Module Procedure MPI_RECV_cwp_v
     Module Procedure MPI_RECV_cwp_m
     Module Procedure MPI_RECV_cwp_c
  End Interface !MPI_RECV

  Interface MPI_IRECV
     Module Procedure MPI_IRECV_chr_s
     Module Procedure MPI_IRECV_chr_v
     Module Procedure MPI_IRECV_int_s
     Module Procedure MPI_IRECV_int_v
     Module Procedure MPI_IRECV_rwp_s
     Module Procedure MPI_IRECV_rwp_v
     Module Procedure MPI_IRECV_rwp_m
     Module Procedure MPI_IRECV_rwp_c
     Module Procedure MPI_IRECV_cwp_s
     Module Procedure MPI_IRECV_cwp_v
     Module Procedure MPI_IRECV_cwp_m
     Module Procedure MPI_IRECV_cwp_c
  End Interface !MPI_IRECV

Contains

  Subroutine MPI_INIT(ierr)

    Implicit None

    Integer, Intent(   Out ) :: ierr
 
    ierr = 0

  End Subroutine MPI_INIT


  Subroutine MPI_FINALIZE(ierr)

    Implicit None

    Integer, Intent(   Out ) :: ierr

    ierr = 0

  End Subroutine MPI_FINALIZE


  Subroutine MPI_ABORT(ierr)

    Implicit None

    Integer, Intent(   Out ) :: ierr

    ierr = 99
    Stop

  End Subroutine MPI_ABORT


  Subroutine MPI_COMM_RANK(MPI_COMM_WORLD,idnode,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_COMM_WORLD
    Integer, Intent(   Out ) :: idnode,ierr

    idnode = 0
    ierr = 0

  End Subroutine MPI_COMM_RANK


  Subroutine MPI_COMM_SIZE(MPI_COMM_WORLD,mxnode,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_COMM_WORLD
    Integer, Intent(   Out ) :: mxnode,ierr

    mxnode = 1
    ierr = 0

  End Subroutine MPI_COMM_SIZE


  Subroutine MPI_COMM_DUP(COMM,NEW_COMM,ierr)

    Implicit None

    Integer, Intent( In    ) :: COMM
    Integer, Intent(   Out ) :: NEW_COMM,ierr

    NEW_COMM=COMM+1
    ierr = 0

  End Subroutine MPI_COMM_DUP


  Subroutine MPI_COMM_SPLIT(COMM,COLOR,KEY,NEW_COMM,ierr)

    Implicit None

    Integer, Intent( In    ) :: COMM,COLOR,KEY
    Integer, Intent(   Out ) :: NEW_COMM,ierr

    NEW_COMM=COMM+1
    ierr = 0

  End Subroutine MPI_COMM_SPLIT

    !create the two dimensional grid communicator
subroutine mpi_cart_create(comm1, n ,dims, periods, reorder, new_comm, ierr)

    integer, intent(in) :: n, dims(n), periods(n), comm1, reorder
    integer, intent(out) :: new_comm, ierr


    new_comm = comm1 + 1
    ierr = 0

end subroutine

subroutine mpi_cart_coords(comm1, ip, n, cart_coords, ierr)

    integer, intent(in) :: ip, n, comm1
    integer, intent(out) :: cart_coords(n), ierr

    cart_coords = 0
    ierr = 0

end subroutine

subroutine mpi_cart_shift(comm1, i, j, down, up, ierr)

    integer, intent(in) :: comm1, i, j
    integer, intent(out) :: down, up, ierr

    down = 0
    up = 0
    ierr = 0

end subroutine


  Subroutine MPI_COMM_FREE(COMM,ierr)

    Implicit None

    Integer, Intent( InOut ) :: COMM
    Integer, Intent(   Out ) :: ierr

    ierr = 0

  End Subroutine MPI_COMM_FREE


  Subroutine MPI_BARRIER(MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    ierr = 0

  End Subroutine MPI_BARRIER


  Subroutine MPI_WAIT(request,status,ierr)

    Implicit None

    Integer, Intent( In    ) :: request
    Integer, Intent(   Out ) :: status(:),ierr

    ierr = 0
    status = 0

  End Subroutine MPI_WAIT


  Function MPI_WTIME()

    Implicit None

    Real( Kind = wp ) :: MPI_WTIME

    Logical,     Save :: newjob
    Data                 newjob / .true. /
    Character,   Save :: date*8
    Data                 date / ' ' /
    Integer,     Save :: days
    Data                 days / 0 /

    Character         :: date1*8,time*10,zone*5
    Integer           :: value(1:8)

    If (newjob) Then
       newjob = .false.

       Call date_and_time(date,time,zone,value)

       MPI_WTIME = Real(value(5),wp)*3600.0_wp + Real(value(6),wp)*60.0_wp   + &
                   Real(value(7),wp)           + Real(value(8),wp)/1000.0_wp
    Else
       Call date_and_time(date1,time,zone,value)

! time-per-timestep & start-up and close-down times
! are assumed to be shorter than 24h

       If (date /= date1) Then
          date = date1
          days = days + 1
       End If

       MPI_WTIME = Real(value(5),wp)*3600.0_wp + Real(value(6),wp)*60.0_wp   + &
                   Real(value(7),wp)           + Real(value(8),wp)/1000.0_wp + &
                   Real(days,wp)*86400.0_wp

    End If

  End Function MPI_WTIME


  Subroutine MPI_ALLREDUCE_log_s(aaa,bbb,n,MPI_LOGICAL,MPI,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_LOGICAL,MPI,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    Logical, Intent( In    ) :: aaa
    Logical, Intent( InOut ) :: bbb

    ierr = 0
    If (n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ALLREDUCE_log_s
  Subroutine MPI_ALLREDUCE_log_v(aaa,bbb,n,MPI_LOGICAL,MPI,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_LOGICAL,MPI,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    Logical, Intent( In    ) :: aaa(:)
    Logical, Intent( InOut ) :: bbb(:)

    ierr = 0
    If (Size(aaa) /= Size(bbb) .or. Size(aaa) /= n) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ALLREDUCE_log_v
  Subroutine MPI_ALLREDUCE_int_s(aaa,bbb,n,MPI_INTEGER,MPI,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,MPI,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa
    Integer, Intent( InOut ) :: bbb

    ierr = 0
    If (n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ALLREDUCE_int_s
  Subroutine MPI_ALLREDUCE_int_v(aaa,bbb,n,MPI_INTEGER,MPI,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,MPI,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa(:)
    Integer, Intent( InOut ) :: bbb(:)

    ierr = 0
    If (Size(aaa) /= Size(bbb) .or. Size(aaa) /= n) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ALLREDUCE_int_v
  Subroutine MPI_ALLREDUCE_rwp_s(aaa,bbb,n,MPI_WP,MPI,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,MPI,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa
    Real( Kind = wp ), Intent( InOut ) :: bbb 

    ierr = 0
    If (n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ALLREDUCE_rwp_s
  Subroutine MPI_ALLREDUCE_rwp_v(aaa,bbb,n,MPI_WP,MPI,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,MPI,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:)
    Real( Kind = wp ), Intent( InOut ) :: bbb(:)

    ierr = 0
    If (Size(aaa) /= Size(bbb) .or. Size(aaa) /= n) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ALLREDUCE_rwp_v


  Subroutine MPI_BCAST_chr_s(aaa,n,MPI_CHARACTER,idnode,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa

    Integer :: length

    length = Len(aaa)

    ierr = 0
    If (idnode /= 0 .or. n /= length) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_BCAST_chr_s
  Subroutine MPI_BCAST_chr_v(aaa,n,MPI_CHARACTER,idnode,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa(:)

    ierr = 0
    If (idnode /= 0 .or. n /= Size(aaa)*Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_BCAST_chr_v
  Subroutine MPI_BCAST_int_s(aaa,n,MPI_INTEGER,idnode,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa

    ierr = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_BCAST_int_s
  Subroutine MPI_BCAST_int_v(aaa,n,MPI_INTEGER,idnode,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa(:)

    ierr = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_BCAST_int_v
  Subroutine MPI_BCAST_rwp_s(aaa,n,MPI_WP,idnode,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_BCAST_rwp_s
  Subroutine MPI_BCAST_rwp_v(aaa,n,MPI_WP,idnode,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_BCAST_rwp_v

  Subroutine MPI_BCAST_log_s(aaa,n,MPI_LOGICAL,idnode,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_LOGICAL,idnode,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    LOGICAL, Intent( In    ) :: aaa

    ierr = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_BCAST_log_s

  !TU: I added the following procedure...
  Subroutine MPI_BCAST_log_v(aaa,n,MPI_LOGICAL,idnode,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_LOGICAL,idnode,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    LOGICAL, Intent( In    ) :: aaa(:)

    ierr = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_BCAST_log_v

  Subroutine MPI_SEND_chr_s(aaa,n,MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa

    ierr = 0
    If (idnode /= 0 .or. n /= Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_chr_s
  Subroutine MPI_SEND_chr_v(aaa,n,MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa(:)

    ierr = 0
    If (idnode /= 0 .or. n /= Size(aaa)*Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_chr_v
  Subroutine MPI_SEND_int_s(aaa,n,MPI_INTEGER,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,tag,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa

    ierr = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_int_s
  Subroutine MPI_SEND_int_v(aaa,n,MPI_INTEGER,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,tag,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa(:)

    ierr = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_int_v
  Subroutine MPI_SEND_rwp_s(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_rwp_s
  Subroutine MPI_SEND_rwp_v(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_rwp_v
  Subroutine MPI_SEND_rwp_m(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:,:)

    ierr = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_rwp_m
  Subroutine MPI_SEND_rwp_c(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:,:,:)

    ierr = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_rwp_c
  Subroutine MPI_SEND_cwp_s(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    If (idnode /= 0 .or. n /= 2) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_cwp_s
  Subroutine MPI_SEND_cwp_v(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_cwp_v
  Subroutine MPI_SEND_cwp_m(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:,:)

    ierr = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_cwp_m
  Subroutine MPI_SEND_cwp_c(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:,:,:)

    ierr = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_SEND_cwp_c


  Subroutine MPI_ISSEND_chr_s(aaa,n,MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_chr_s
  Subroutine MPI_ISSEND_chr_v(aaa,n,MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa(:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)*Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_chr_v
  Subroutine MPI_ISSEND_int_s(aaa,n,MPI_INTEGER,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,tag,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: request,ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_int_s
  Subroutine MPI_ISSEND_int_v(aaa,n,MPI_INTEGER,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,tag,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: request,ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa(:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_int_v
  Subroutine MPI_ISSEND_rwp_s(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: request,ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_rwp_s
  Subroutine MPI_ISSEND_rwp_v(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: request,ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_rwp_v
  Subroutine MPI_ISSEND_rwp_m(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: request,ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:,:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_rwp_m
  Subroutine MPI_ISSEND_rwp_c(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: request,ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:,:,:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_rwp_c
  Subroutine MPI_ISSEND_cwp_s(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 2) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_cwp_s
  Subroutine MPI_ISSEND_cwp_v(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_cwp_v
  Subroutine MPI_ISSEND_cwp_m(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:,:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_cwp_m
  Subroutine MPI_ISSEND_cwp_c(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:,:,:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_ISSEND_cwp_c


  Subroutine MPI_RECV_chr_s(aaa,n,MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: status(:),ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_chr_s
  Subroutine MPI_RECV_chr_v(aaa,n,MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: status(:),ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa(:)

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= Size(aaa)*Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_chr_v
  Subroutine MPI_RECV_int_s(aaa,n,MPI_INTEGER,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,tag,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: status(:),ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_int_s
  Subroutine MPI_RECV_int_v(aaa,n,MPI_INTEGER,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,tag,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: status(:),ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa(:)

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_int_v
  Subroutine MPI_RECV_rwp_s(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: status(:),ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_rwp_s
  Subroutine MPI_RECV_rwp_v(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: status(:),ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_rwp_v
  Subroutine MPI_RECV_rwp_m(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: status(:),ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:,:)

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_rwp_m
  Subroutine MPI_RECV_rwp_c(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: status(:),ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:,:,:)

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_rwp_c
  Subroutine MPI_RECV_cwp_s(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: status(:),ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= 2) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_cwp_s
  Subroutine MPI_RECV_cwp_v(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: status(:),ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_cwp_v
  Subroutine MPI_RECV_cwp_m(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: status(:),ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:,:)

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_cwp_m
  Subroutine MPI_RECV_cwp_c(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,status,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: status(:),ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:,:,:)

    ierr = 0
    status = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_RECV_cwp_c


  Subroutine MPI_IRECV_chr_s(aaa,n,MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_chr_s
  Subroutine MPI_IRECV_chr_v(aaa,n,MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_CHARACTER,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Character( Len = * ), Intent( In    ) :: aaa(:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)*Len(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_chr_v
  Subroutine MPI_IRECV_int_s(aaa,n,MPI_INTEGER,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,tag,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: request,ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       print*,'mpi_irecv',idnode,n,aaa
       Stop 'mpi_irecv'
    End If

  End Subroutine MPI_IRECV_int_s
  Subroutine MPI_IRECV_int_v(aaa,n,MPI_INTEGER,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer, Intent( In    ) :: MPI_INTEGER,idnode,tag,MPI_COMM_WORLD
    Integer, Intent(   Out ) :: request,ierr

    Integer, Intent( In    ) :: n
    Integer, Intent( In    ) :: aaa(:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop 'mpi_irecv'
    End If

  End Subroutine MPI_IRECV_int_v
  Subroutine MPI_IRECV_rwp_s(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: request,ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 1) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_rwp_s
  Subroutine MPI_IRECV_rwp_v(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: request,ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_rwp_v
  Subroutine MPI_IRECV_rwp_m(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: request,ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:,:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_rwp_m
  Subroutine MPI_IRECV_rwp_c(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,           Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,           Intent(   Out ) :: request,ierr

    Integer,           Intent( In    ) :: n
    Real( Kind = wp ), Intent( In    ) :: aaa(:,:,:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_rwp_c
  Subroutine MPI_IRECV_cwp_s(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 2) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_cwp_s
  Subroutine MPI_IRECV_cwp_v(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_cwp_v
  Subroutine MPI_IRECV_cwp_m(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:,:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_cwp_m
  Subroutine MPI_IRECV_cwp_c(aaa,n,MPI_WP,idnode,tag,MPI_COMM_WORLD,request,ierr)

    Implicit None

    Integer,              Intent( In    ) :: MPI_WP,idnode,tag,MPI_COMM_WORLD
    Integer,              Intent(   Out ) :: request,ierr

    Integer,              Intent( In    ) :: n
    Complex( Kind = wp ), Intent( In    ) :: aaa(:,:,:)

    ierr = 0
    request = 0
    If (idnode /= 0 .or. n /= 2*Size(aaa)) Then
       ierr = 1
       Stop
    End If

  End Subroutine MPI_IRECV_cwp_c

End Module mpi_module
