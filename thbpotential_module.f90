! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module thbpotential_module

    use kinds_f90

    implicit none

    integer, parameter :: npar = 7

    integer :: numthb = 0   ! number of thb potentials
    integer, dimension(:,:), allocatable :: thbtypes

    real(kind = wp), allocatable, dimension(:,:) :: prmthb
    real(kind = wp), allocatable, dimension(:) :: ltpthb

contains

subroutine alloc_thbpar_arrays(num)

    use kinds_f90
    use constants_module, only : uout

    implicit none

    integer, intent(in) :: num

    integer :: fail(3)

    fail = 0
    numthb = num

    allocate(prmthb(num, npar), stat = fail(1))
    allocate(thbtypes(num, 3), stat = fail(2))
    allocate(ltpthb(num), stat = fail(3))

    if(any(fail > 0)) then

        call error(161)

    endif

    prmthb(:,:) = 0.0
    thbtypes(:,:) = 0
    ltpthb(:) = 0

end subroutine

subroutine read_thb_par(unit)

    use kinds_f90
    use constants_module, only : uout, ufld, TORADIANS
    use parse_module
    use species_module, only : number_of_elements, element, eletype, get_species_type
    use comms_mpi_module, only : idnode

    implicit none

    real(kind = wp), intent(in) :: unit

    integer :: nread

    integer :: i, j, typi, typj, typk, typthb

    real(kind = wp) :: fc, cz, rij, rik, rjk, rho1, rho2
    character :: line*100, word*40, atmi*8, atmj*8, atmk*8, keyword*4

    logical safe, found

    safe = .true.

    nread = ufld

    if (idnode == 0) write(uout,"(/,/,1x,'three-body potentials ',/)")

    do i = 1, numthb

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        call get_word(line, keyword)

        if (keyword(1:3) == 'cos') then

            ltpthb(i) = 1

        elseif (keyword(1:4) == 'harm') then

            ltpthb(i) = 2

        elseif (keyword(1:4) == 'thrm') then

            ltpthb(i) = 3

        elseif (keyword(1:4) == 'shrm') then

            ltpthb(i) = 4

        else

            call error(162)

        endif

        typthb = ltpthb(i)

        !TU: Note that the atoms are read in order such that the middle atom in a triplet is the middle atom in
        !TU: the list of three read in
        
        call get_word(line,atmj)
        call get_word(line,word)
        typj = get_species_type(word)
        !typi = get_species_type(word) !AB: DL_POLY convention is to have the central atom index (i) in the middle

        call get_word(line,atmi)
        call get_word(line,word)
        typi = get_species_type(word)
        !typj = get_species_type(word) !AB: DL_POLY convention is to have the central atom index (i) in the middle

        call get_word(line,atmk)
        call get_word(line,word)
        typk = get_species_type(word)


        found = .false.
        do j = 1, number_of_elements

            if(atmi == element(j) .and. typi == eletype(j)) then

                thbtypes(i,1) = j
                found = .true.

            endif

        enddo

        if(.not.found) then

            call error(163)
            
        endif

        found = .false.
        do j = 1, number_of_elements

            if(atmj == element(j) .and. typj == eletype(j)) then

                thbtypes(i,2) = j
                found = .true.

            endif

        enddo

        if(.not.found) then

            call error(164)
            
        endif

        found = .false.
        do j = 1, number_of_elements

            if(atmk == element(j) .and. typj == eletype(j)) then

                thbtypes(i,3) = j
                found = .true.

            endif

        enddo

        if(.not.found) then

            call error(164)
            
        endif


        select case(typthb)

            case(1)

                call get_word(line,word)
                fc = word_2_real(word)
                call get_word(line,word)
                cz = word_2_real(word)
                call get_word(line,word)
                rij = word_2_real(word)
                call get_word(line,word)
                rik = word_2_real(word)
                call get_word(line,word)
                rjk = word_2_real(word)

                if (idnode == 0) then
                        
                    write(uout,'(1x,"Harmonic Cosine Three body Potential")')
                    write(uout,"(1x,3a8,3x,10f15.6)") &
                        atmi, atmj, atmk, fc, cz, rij, rik, rjk

                endif

                !convert force constant to internal units
                fc = fc * unit

                ! convert angle to radians then to cos(angle)
                cz = cz * TORADIANS
                cz = cos(cz)

                prmthb(i,1) = fc
                prmthb(i,2) = cz
                prmthb(i,5) = rij  !5,6,7 are always the distance for the cutoff
                prmthb(i,6) = rik
                prmthb(i,7) = rjk

            case(2)

                call get_word(line,word)
                fc = word_2_real(word)
                call get_word(line,word)
                cz = word_2_real(word)
                call get_word(line,word)
                rij = word_2_real(word)
                call get_word(line,word)
                rik = word_2_real(word)
                call get_word(line,word)
                rjk = word_2_real(word)

                if (idnode == 0) then

                    write(uout,'(1x,"Harmonic Three body Potential")')
                    write(uout,"(1x,3a8,3x,10f15.6)") &
                        atmi, atmj, atmk, fc, cz, rij, rik, rjk

                endif

                !convert force constant to internal units
                fc = fc * unit
                ! convert angle_0 to radians
                cz = cz * TORADIANS

                prmthb(i,1) = fc
                prmthb(i,2) = cz
                prmthb(i,5) = rij
                prmthb(i,6) = rik
                prmthb(i,7) = rjk

            case(3)

                call get_word(line,word)
                fc = word_2_real(word)
                call get_word(line,word)
                cz = word_2_real(word)
                call get_word(line,word)
                rho1 = word_2_real(word)
                call get_word(line,word)
                rij = word_2_real(word)
                call get_word(line,word)
                rik = word_2_real(word)
                call get_word(line,word)
                rjk = word_2_real(word)

                if (idnode == 0) then

                    write(uout,'(1x,"Truncated Harmonic Three body Potential")')
                    write(uout,"(1x,3a8,1x,3x,10f15.6)") &
                        atmi, atmj, atmk, fc, cz, rho1

                    call warning(26)


                endif

                !convert force constant to internal units
                fc = fc * unit
                ! convert angle_0 to radians
                cz = cz * TORADIANS

                prmthb(i,1) = fc
                prmthb(i,2) = cz
                prmthb(i,3) = rho1
                prmthb(i,5) = rij
                prmthb(i,6) = rik
                prmthb(i,7) = rjk

            case(4)

                call get_word(line,word)
                fc = word_2_real(word)
                call get_word(line,word)
                cz = word_2_real(word)
                call get_word(line,word)
                rho1 = word_2_real(word)
                call get_word(line,word)
                rho2 = word_2_real(word)
                call get_word(line,word)
                rij = word_2_real(word)
                call get_word(line,word)
                rik = word_2_real(word)
                call get_word(line,word)
                rjk = word_2_real(word)

                if (idnode == 0) then

                    write(uout,'(1x,"Screened Harmonic Three body Potential")')
                    write(uout,"(1x,3(a8,1x),3x,10f15.6)") &
                        atmi, atmj, atmk, fc, cz, rho1, rho2

                    call warning(26)


                endif

                !convert force constant to internal units
                fc = fc * unit
                ! convert angle_0 to radians
                cz = cz * TORADIANS

                prmthb(i,1) = fc
                prmthb(i,2) = cz
                prmthb(i,3) = rho1
                prmthb(i,4) = rho2
                prmthb(i,5) = rij
                prmthb(i,6) = rik
                prmthb(i,7) = rjk

        end select


    enddo

    return

1000 call error(166)

end subroutine

!> calculates the force for a triplet
subroutine calc_thb_energy(pot, theta, rij, rik, rjk, eng)

    use kinds_f90

    implicit none

    integer, intent(in) :: pot
    real(kind = wp), intent(in) :: theta, rij, rik, rjk
    real(kind = wp), intent(out) :: eng

    integer :: typ
    real(kind = wp) :: rijmax, rikmax, rjkmax, fc, cz, costh, scrn, dtheta, rho1, rho2

    eng = 0.0_wp

    if(pot == 0) return

    typ = ltpthb(pot)

    select case(typ)

        case(1)

            fc = prmthb(pot, 1)
            cz = prmthb(pot, 2)
            rijmax = prmthb(pot, 5)
            rikmax = prmthb(pot, 6)
            rjkmax = prmthb(pot, 7)

            if (rij <= rijmax .and. rik <= rikmax .and. rjk <= rjkmax) then

                !cos
                costh = cos(theta)
                eng = 0.5_wp * fc * (costh - cz)**2

            else

                call error(131)

            endif

        case(2)

            fc = prmthb(pot, 1)
            cz = prmthb(pot, 2)
            rijmax = prmthb(pot, 5)
            rikmax = prmthb(pot, 6)
            rjkmax = prmthb(pot, 7)

            if (rij <= rijmax .and. rik <= rikmax .and. rjk <= rjkmax) then
                !TU: Code for 'harm'. Note that (theta-cz) is in radians, which is the conventiona
                !TU: (despite angles being read in degrees in FIELD)
            
                eng = 0.5_wp * fc * (theta - cz)**2

            else

                call error(131)

            endif

        case(3)

            ! truncated Harmonic potential

            fc = prmthb(pot,1)
            cz = prmthb(pot,2)
            rho1 = prmthb(pot,3)
            dtheta = theta - cz
            scrn = ((rij**8 + rik**8) / rho1)
            eng = fc * dtheta * dtheta * exp(-scrn)

        case(4)

            ! screened Harmonic potential
            fc = prmthb(pot,1)
            cz = prmthb(pot,2)
            rho1 = prmthb(pot,3)
            rho2 = prmthb(pot,4)
            dtheta = theta - cz
            scrn = rij / rho1 + rik / rho2
            eng = fc * dtheta * dtheta * exp(-scrn)

    end select

end subroutine


end module
