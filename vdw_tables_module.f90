! *******************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                                   *
! *   john.purton[@]stfc.ac.uk                                                  *
! *                                                                             *
! *   Contributors:                                                             *
! *   -------------                                                             *
! *   A.V.Brukhno (C) 2015-2016                                                 *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                          *
! *   - overall optimizations (general MC scheme, energy calculus and MC steps) *
! *   - Free Energy Difference (FED) & order parameters (fed_*_module.f90)      *
! *   - Replica Exchange (RE) algorithm (rep_exchange_module.f90)               *
! *   - VdW and general potential forms (vdw_*_module.f90 etc)                  *
! *   - planar pore constraint (slit_module.f90)                                *
! *                                                                             *
! *   T.L.Underwood (C) 2015-2016                                               *
! *   t.l.Underwood[@]bath.ac.uk                                                *
! *   - Lattice/Phase-Switch MC methodology                                     *
! *   - algorithm optimizations (gcmc_*_module.90, MC steps, random generator)  *
! *                                                                             *
! *******************************************************************************

module vdw_module

    use kinds_f90

    implicit none

    !> the number of parameters for vdw
    integer, parameter :: mxpvdw = 6

    !> number of vdw potentials
    integer, save :: ntpvdw

    !> flag for the implementation branch: 'direct'/'tables' (1/0)
    logical, save :: is_direct = .false.
    logical, save :: is_tables = .true.
    logical, save :: is_lrcorr = .false.
    logical, save :: is_shift  = .false.

    !AB: HC_VDW is the core diameter to avoid a VdW energy overflow beyond the range of Real(kind=wp)
    !AB: due to steep repulsive sholder (otherwise inf or spooky negative energy may occur)
    !AB: used only in VdW calculations and resulting in definite rejection of an MC move
    !AB: should be replaced by a VdW term dependendent definition in vdw_read(..)

    real(kind = wp), save ::  HC_MIN    = 0.001_wp     ! Angstrom
    real(kind = wp), save ::  HC_MIN2   = 0.000001_wp  ! Angstrom^2
    real(kind = wp), save ::  HC_VDW    = 0.5_wp       ! Angstrom
    real(kind = wp), save ::  HC_VDW2   = 0.25_wp      ! Angstrom^2
    real(kind = wp), save ::  HS_ENERGY = 1.e7_wp      ! Internal units

    !TU: Hard-sphere overlap energy cost
    !real(kind = wp), save :: HS_ENERGY = 1.0e7_wp   ! Internal units

    !> look up table for vdw + types of vdw potential
    integer, allocatable, save :: lstvdw(:,:)
    integer, allocatable, save :: ltpvdw(:)

    !> Matrix of coefficients for calculating long-range corrections between pairs of atomic species
    real(kind = wp), allocatable, save :: lrcvdw(:,:)

    !> vdw parameters
    real( Kind = wp ), allocatable, save :: prmvdw(:,:)

    !> potential and virial arrays for tabulated force-fields
    real( Kind = wp ), allocatable, save :: vvdw(:,:),gvdw(:,:)
    real( Kind = wp )                    :: dlrpot,rdr


contains


!> allocate vdw arrays and zero
subroutine allocate_vdw_arrays(nvdw)

    use kinds_f90
    use constants_module, only : maxmesh, uout
    use species_module, only : number_of_elements

    implicit none

    integer, intent(in) :: nvdw

    integer, dimension( 1:6 ) :: fail
    integer :: maxint, ntpatm

    fail = 0

    ntpatm = number_of_elements
    maxint = (ntpatm * (ntpatm + 1)) / 2

    allocate (lstvdw(ntpatm, ntpatm),    stat = fail(1))
    allocate (ltpvdw(nvdw),              stat = fail(2))
    allocate (prmvdw(-1:mxpvdw, maxint), stat = fail(3))
    allocate (vvdw(maxmesh, maxint),     stat = fail(4))
    allocate (gvdw(maxmesh, maxint),     stat = fail(5))

    allocate (lrcvdw(ntpatm,ntpatm),     stat = fail(6))

    if (any(fail > 0)) call error(122)

    ntpvdw = nvdw

    lstvdw = 0
    ltpvdw = 0

    lrcvdw = 0.0_wp

    prmvdw = 0.0_wp
    vvdw   = 0.0_wp
    gvdw   = 0.0_wp

end subroutine allocate_vdw_arrays


!> read in vdw potentials and convert to internal units, also generate the look-up tables
!AB: reading VDW section from FIELD file - unified routine for 'direct' and 'tabulated' versions

include "vdw_read_field.f90"


subroutine fh_correct_vdw(temperature, rvdw)

    use constants_module, only : maxmesh, uout, HBAR, AVAG, BOLT
    use kinds_f90
    use species_module
    use comms_mpi_module, only : master

    implicit none

    real (kind = wp), intent(in) :: temperature, rvdw

    integer   :: i, itab, ityp, jtyp, ntpatm

    real (kind = wp) :: mass1, mass2, sig1, sig2, rrr, r14, r8, red_mass, dlrpot, d2u, fact

    ntpatm = number_of_elements

    dlrpot=rvdw/real(maxmesh-4,wp)

    do ityp = 1, ntpatm

        do jtyp = ityp, ntpatm

            itab = lstvdw(ityp, jtyp)

            if (itab /= 0) then

                if (ltpvdw(itab) == 1) then !is a lennard jones potential

                    if (master) then

                        write(uout,"(/,1x,'Feynman-Hibbs correction for potential ',i3,1x,a8,1x,a8)") &
                              itab,element(ityp), element(jtyp)

                    endif

                    mass1 = atm_mass(ityp)
                    mass2 = atm_mass(jtyp)

                    red_mass = (mass1 * mass2) / (mass1 + mass2)

                    fact = (10.0_wp * AVAG * HBAR * HBAR) / (BOLT * temperature * 24.0_wp * red_mass)

                    sig1 = 132.0_wp * prmvdw(2,itab)**12
                    sig2 = 30.0_wp * prmvdw(2,itab)**6

                    do i=1,maxmesh

                        rrr=real(i,wp)*dlrpot

                        r14 = rrr**14
                        r8 = rrr**8
                        d2u = 4.0_wp * prmvdw(1,itab) * (sig1 / r14 - sig2 / r8)

                        vvdw(i,itab) = vvdw(i,itab) + fact * d2u

                    end do

                endif

            endif


        enddo

    enddo

end subroutine


subroutine set_vdw_grid(rvdw)

   use kinds_f90
   use constants_module, only : maxmesh

   implicit none

   real(kind=wp), intent(in) :: rvdw

   ! define grid resolution for potential arrays and interpolation spacing

   dlrpot = rvdw/real(maxmesh-4)
   rdr    = 1.0_wp/dlrpot

end subroutine set_vdw_grid


!> convert potentials to look up tables
!> also factors in coulomb sum parts
subroutine vdw_generate(rvdw)

    use kinds_f90
    use constants_module
    use species_module

    implicit None

    real(kind = wp), intent(in) :: rvdw

    integer           :: i, itab, ityp, jtyp, ll, ntpatm

    real( Kind = wp ) :: rrr, rsq, chgea, chgeb, chgprd, coul, ppp, &
                         vk0, vk1, vk2, gk0, gk1, gk2, t1, t2, gamma,egamma

    !AB: << potential form definitions

    include "potential_forms.inc"

    !AB: >> potential form definitions


    ! define grid resolution for potential arrays

    dlrpot=rvdw/real(maxmesh-4,wp)

    call set_vdw_grid(rvdw)

    ! construct arrays for all types of vdw potential

    ntpatm = number_of_elements

    do ityp = 1, ntpatm

        do jtyp = ityp, ntpatm

            itab = lstvdw(ityp, jtyp)

            if (itab /= 0) then
                
                if (ltpvdw(itab) == 1) then
                   ! Normal LJ

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv1(rrr,prmvdw(1,itab),prmvdw(2,itab)) !-prmvdw(-1,itab)
                      gvdw(i,itab)=gg1(rrr,prmvdw(1,itab),prmvdw(2,itab))
                   end do

                else if (ltpvdw(itab) == 2) then
                   ! Simple Powers 12-6

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv2(rrr,prmvdw(1,itab),prmvdw(2,itab)) !-prmvdw(-1,itab)
                      gvdw(i,itab)=gg2(rrr,prmvdw(1,itab),prmvdw(2,itab))
                   end do

                else if (ltpvdw(itab) == 3) then
                   ! Simple Powers 9 - 3

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv3(rrr,prmvdw(1,itab),prmvdw(2,itab))
                      gvdw(i,itab)=gg3(rrr,prmvdw(1,itab),prmvdw(2,itab))
                      !vvdw(i,itab)=vv10(rrr,prmvdw(1,itab),prmvdw(2,itab),9.0_wp,3.0_wp)
                      !gvdw(i,itab)=gg10(rrr,prmvdw(1,itab),prmvdw(2,itab),9.0_wp,3.0_wp)
                   end do

                else if (ltpvdw(itab) == 4) then
                   ! Simple Powers 10 - 4

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv4(rrr,prmvdw(1,itab),prmvdw(2,itab))
                      gvdw(i,itab)=gg4(rrr,prmvdw(1,itab),prmvdw(2,itab))
                      !vvdw(i,itab)=vv10(rrr,prmvdw(1,itab),prmvdw(2,itab),10.0_wp,4.0_wp)
                      !gvdw(i,itab)=gg10(rrr,prmvdw(1,itab),prmvdw(2,itab),10.0_wp,4.0_wp)
                   end do

                else if (ltpvdw(itab) == 5) then
                   ! WCA-LJ (by I.T.Todorov - as in DL_POLY-4)

                   !AB: now this is done while reading FIELD
                   !if (prmvdw(4,itab) == 0.0_wp) prmvdw(4,itab) = rvdw

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot

                      if (rrr < prmvdw(4,itab)) then 
                         vvdw(i,itab)=vv5(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab))
                         gvdw(i,itab)=gg5(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab))
                      end if

                   end do

                else if (ltpvdw(itab) == 6) then
                   ! Hydrohen-bonding 12-10

                   do i = 1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv6(rrr,prmvdw(1,itab),prmvdw(2,itab))
                      gvdw(i,itab)=gg6(rrr,prmvdw(1,itab),prmvdw(2,itab))
                   end do

                else if (ltpvdw(itab) == 7) then
                   ! E-W

                   !AB: now this is done while reading FIELD
                   !if (prmvdw(2,itab) == 0.0_wp) prmvdw(2,itab) = rvdw

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot

                      if(rrr < prmvdw(2,itab)) then
                          vvdw(i,itab) = vv7(rrr, prmvdw(1,itab), prmvdw(2,itab))
                          gvdw(i,itab) = gg7(rrr, prmvdw(1,itab), prmvdw(2,itab))
                      endif

                      !vvdw(i,itab) = vv10(rrr, prmvdw(1,itab), rvdw)
                      !gvdw(i,itab) = gg10(rrr, prmvdw(1,itab), rvdw)
                   end do

                else if (ltpvdw(itab) == 8) then

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv8(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab))
                      gvdw(i,itab)=gg8(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab))

                   end do

                else if (ltpvdw(itab) == 9) then
                   ! Morse (+ LJ-rep)

                   do i = 1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv9(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab))
                      gvdw(i,itab)=gg9(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab))

                      d = prmvdw(4,itab)
                      if( d > 0.0_wp ) then 
                          sri12 = d/rrr**12
                          vvdw(i,itab) = vvdw(i,itab) + sri12
                          gvdw(i,itab) = gvdw(i,itab) + 12.0_wp*sri12
                      end if
                   end do

                else if (ltpvdw(itab) == 10) then
                   ! Simple Powers n > m

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv10(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab),prmvdw(4,itab))
                      gvdw(i,itab)=gg10(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab),prmvdw(4,itab))
                   end do

                else if (ltpvdw(itab) == 11) then
                   ! Powers n > m

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv11(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab),prmvdw(4,itab))
                      gvdw(i,itab)=gg11(rrr,prmvdw(1,itab),prmvdw(2,itab),prmvdw(3,itab),prmvdw(4,itab))
                   end do

                else if (ltpvdw(itab) == 12) then
                   ! Shifted & corrected n-m (by W.Smith)

                   eps = prmvdw(1,itab)
                   an  = prmvdw(2,itab)
                   am  = prmvdw(3,itab)
                   r0  = prmvdw(4,itab)

                   !if (an <= am) write(uout, *)' par 2 less than par 3 ' !AB: now checked while reading FIELD

                   gamma = rvdw/r0

                   !if (gamma < 1.0_wp) then !AB: now checked while reading FIELD
                   !    write(uout, *) 'r0 > cutoff'
                   !    stop
                   !endif

                   beta = gamma*((gamma**(am+1.0_wp)-1.0_wp) /                       &
                         (gamma**(an+1.0_wp)-1.0_wp))**(1.0_wp/(an-am))

                   alpha= -(an-am) /                                                 &
                           ( am*(beta**an)*(1.0_wp+(an/gamma-an-1.0_wp)/gamma**an)   &
                           - an*(beta**am)*(1.0_wp+(am/gamma-am-1.0_wp)/gamma**am) )

                   eps = eps*alpha

                   do i = 1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv12(rrr,eps,an,am,r0,beta,gamma)
                      gvdw(i,itab)=gg12(rrr,eps,an,am,r0,beta,gamma)
                   end do

                else if (ltpvdw(itab) == 13) then
                   ! Born-Huggins-Meyer exp-6-8

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab)=vv13(rrr,prmvdw(1,itab),prmvdw(2,itab),&
                                            prmvdw(3,itab),prmvdw(4,itab),prmvdw(5,itab))
                      gvdw(i,itab)=gg13(rrr,prmvdw(1,itab),prmvdw(2,itab),&
                                            prmvdw(3,itab),prmvdw(4,itab),prmvdw(5,itab))
                   end do

                else if (ltpvdw(itab) == 14) then
                   ! A-O

                   !AB: now this is done while reading FIELD
                   !if (prmvdw(2,itab) == 0.0_wp) prmvdw(2,itab) = rvdw

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot

                      if(rrr < prmvdw(2,itab)) then
                          vvdw(i,itab) = vv14(rrr, prmvdw(1,itab), prmvdw(2,itab), prmvdw(3,itab))
                          gvdw(i,itab) = gg14(rrr, prmvdw(1,itab), prmvdw(2,itab), prmvdw(3,itab))
                      endif

                   end do

                else if (ltpvdw(itab) == 15) then
                   ! Yukawa

                   do i=1,maxmesh
                      rrr=real(i,wp)*dlrpot
                      vvdw(i,itab) = vv15(rrr, prmvdw(1,itab), prmvdw(2,itab), prmvdw(3,itab))
                      gvdw(i,itab) = gg15(rrr, prmvdw(1,itab), prmvdw(2,itab), prmvdw(3,itab))
                   end do

                end if

            endif

        enddo

    end do

    itab = 0

!   do ityp = 1, ntpatm
!       do jtyp = ityp, ntpatm
!
!           itab = itab + 1
!           lstvdw(ityp,jtyp) = itab
!           lstvdw(jtyp,ityp) = itab
!
!       enddo
!   enddo

end subroutine vdw_generate


!TU: Note that atom orientation variables have been added as arguments here.
!TU: However atom orientations are not supported with tabulated potentials!
subroutine vdw_energy(ai, aj, rrr, rsq, rcut, eunit, evdw, volinv, rvec, spini, spinj)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine for calculating vdw energy and force terms using
! verlet neighbour list
!
! copyright - daresbury laboratory
! author    - w.smith august 1998
! amended   - i.t.todorov september 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  use kinds_f90
  use constants_module, only : uout, RCORE

  implicit None

  integer, intent(in) :: ai, aj
  real(kind = wp), intent(in) :: rrr, rsq, rcut, eunit, volinv
  real( Kind = wp ), intent( out ) :: evdw
  !real( Kind = wp ), intent( inout ) :: evdw

  !TU: Additional parameters required for anisotropic VdW interactions
    
      !> Separation vector between the particles (after accounting for PBCs)
  real(kind = wp), intent(in) :: rvec(3)

      !> Orientation/spin of atom 1
  real(kind = wp), intent(in) :: spini(3)

      !> Orientation/spin of atom 2
  real(kind = wp), intent(in) :: spinj(3)


  integer           :: k,keyvdw,l,i
  real( Kind = wp ) :: dhc,dsh,ppp,gk,gk1,gk2,vk,vk1,vk2,t1,t2

  evdw = 0.0_wp
  if (rrr > rcut) return 
  !AB: above is NOT a duplicate check! 
  !AB: the check outside (in field.f90) is for global cutoff, and here we check for VDW cutoff

  ! atomic and potential function indices

  k=lstvdw(ai,aj)
  if (k == 0) return

  keyvdw = ltpvdw(k)

  !AB: the check for a too small separation (RCORE) is done externally! (see field.f90)
  !AB: as a matter of fact, it must be redundant due to the check for dhc = HC_VDW below
  !AB: this is where one can improve on the efficiency!

  !if (k > 0 .and. rrr > RCORE .and. rrr <= rcut) then

  !AB: note that hard potentials (k < 0) are only available in vdw_direct_module.f90
  if( k > 0 ) then

     !AB: the hard core diameter to avoid an overflow of VdW energy term beyond the range of Real(kind=wp)
     !AB: due to steep repulsive sholder (otherwise inf or spooky negative energy may occur)
     !dhc = HC_VDW

     dhc = prmvdw(0,k)

!     if( keyvdw < 0 ) then 
!         !AB: non-analytical forms (stepwise potentials)
!         !TU: Code for hard-sphere potential ('hs')
!
!         if( rrr < dhc ) then
!
!             evdw = HS_ENERGY
!
!             return
!
!         else if ( rrr < prmvdw(2,k) ) then
!
!             evdw = prmvdw(3,k)
!
!             return
!
!         end if
!
!     else 
     if( keyvdw < 12 ) then 

         !AB: NOTE FOR MOST TYPES
         !AB: enabling potentials starting at the effective particle surfaces
         !AB: rather than from the particle centers (this is a new feature!)
         !AB: although the surface distance definition is a little complicated...

         !AB: dsh = prmvdw(max+1,ivdw) ! where max is the number of compulsory parameters
         !AB: dhc = dhc+dsh            ! this is effective hard-core / overlap distance

         !AB: parpot(i) sets the distance from the particle centre to the origin
         !AB: so the 12-6 and LJ interactions are shifted to the particle surface(s)

!         select case (keyvdw)
!             case (9,10,11)  ! case (9,10,11,12) / previously: case (3,8,11) ! case (3,7,8,11)
!                 i = 5
!             case (8)  ! case (4)
!                 i = 4
!             case default
!                 i = 3
!         end select

         i = 3
         if( keyvdw > 8 ) then
             i = 5
         else if( keyvdw == 8 ) then
             i = 4
         end if

         dsh = prmvdw(i,k)
         dhc = dhc+dsh

         !AB: check for overlapping cores due to steep short-range repulsion
         if( rrr < dhc ) then

             !AB: set the energy very high but below the upper bound of Real(kind=wp)
             !AB: so that an MC move would be definitely rejected

             evdw = HS_ENERGY

             return

         end if

         dsh = rrr-dsh

         if( dsh < 0.0_wp ) then
             write(uout,*)'SOS: negative distance to the 12-6/LJ surface!!!',rrr,dsh
             call error(999)
         end if

         l   = int(dsh*rdr)
         ppp = dsh*rdr - real(l,wp)

     else

         if( rrr < dhc ) then

             !AB: set the energy very high but below the upper bound of Real(kind=wp)
             !AB: so that an MC move would be definitely rejected

             evdw = HS_ENERGY

             return

         end if

         l   = int(rrr*rdr)
         ppp = rrr*rdr - real(l,wp)

     endif

! calculate forces using 3-point interpolation

!    gk  = gvdw(l,k)
!    gk1 = gvdw(l+1,k)
!    gk2 = gvdw(l+2,k)

!    t1 = gk  + (gk1 - gk )*ppp
!    t2 = gk1 + (gk2 - gk1)*(ppp - 1.0_wp)

!    gamma = (t1 + (t2-t1)*ppp*0.5_wp) / rsq

!       if (jatm <= natms(icell) .or. idi < ltg(jatm,icell)) then

! calculate interaction energy using 3-point interpolation

     vk  = vvdw(l,k)
     vk1 = vvdw(l+1,k)
     vk2 = vvdw(l+2,k)

     t1 = vk  + (vk1 - vk )*ppp
     t2 = vk1 + (vk2 - vk1)*(ppp - 1.0_wp)

     evdw = t1 + (t2-t1)*ppp*0.5_wp

     if( keyvdw > 2 ) return

     !TU: The below commented out code is for AB's long-range VdW correction, which
     !TU: is no longer in use
     !AB: LJ/12-6 pairwise shift or long-range dispersion correction 
     !if( keyvdw > 0 ) evdw = evdw + prmvdw(-1,k)*volinv

  end if !( k > 0 )

end subroutine vdw_energy


!> Returns the long-range correction energy given a specified inverse volume and
!> array containing the number of atoms of each atomic species in the system.
!> Returns 0 if long-range corrections are not in use or if VdW potentials are
!> not in use.
real(kind=wp) function vdw_lrc_energy(volinv, natomtypes)

    use kinds_f90
    
    implicit none

        !> Inverse volume of the system
    real (kind = wp), intent(in) :: volinv

        !> Array containing the number of atoms of each species in the system
    integer, dimension(:), intent(in) :: natomtypes

    vdw_lrc_energy = 0.0_wp

    if( ntpvdw > 0 ) then

        vdw_lrc_energy = volinv * dot_product( natomtypes, matmul( lrcvdw, natomtypes) )

    end if


end function vdw_lrc_energy


end module vdw_module
