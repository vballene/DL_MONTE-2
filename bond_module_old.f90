! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Bond storage, manipulation and energy calculation
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @modulefor bonds in a molecule

module bond_module

    use kinds_f90

    implicit none

        !> total (max) number of bonds allowed
    integer, save :: mxbond

        !> number of bond potential types
    integer, save :: ntpbond

        !> max number of parameters for bond
    integer, parameter :: mxpbond = 6

        !> look up table for bond + types of bond potential(s)
    integer, allocatable, save :: lstbond(:,:)

        !> @auxarray for potential types
    integer, allocatable, save :: ltpbond(:)

        !> array for bond potential parameters
    real( Kind = wp ), allocatable, save :: prmbond(:,:)

        !> array to store numerically potential tables
    real( Kind = wp ), allocatable, save :: vbond(:,:)

        !> array to store numerically virial tables
    real( Kind = wp ), allocatable, save :: gbond(:,:)

        !> distance bin (for atom-atom separation)
    real( Kind = wp ) :: dlrpot,rdr

contains

!> @brief
!> - allocates all arrays needed for bond potentials and forces/virials
!> @using 
!> - `kinds_f90`
!> - `constants_module, only : maxmesh, uout`
!> - `species_module, only : number_of_elements`
subroutine allocate_bondpot_arrays(nbond)

    use kinds_f90
    use constants_module, only : maxmesh, uout
    use species_module, only : number_of_elements

    implicit none

        !> total (max) number of bonds
    integer, intent(in) :: nbond

    integer, dimension( 1:5 ) :: fail
    integer :: maxint, ntpatm

    fail = 0

    ntpatm = number_of_elements
    maxint = (ntpatm * (ntpatm + 1)) / 2


    allocate (lstbond(mxpbond, 2), stat = fail(1))
    allocate (ltpbond(nbond),             stat = fail(2))
    allocate (prmbond(mxpbond, maxint),  stat = fail(3))
    allocate (vbond(maxmesh, maxint),   stat = fail(4))
    allocate (gbond(maxmesh, maxint),   stat = fail(5))

    if (any(fail > 0)) then

        call error(117)

    endif

    ntpbond = nbond

    lstbond = 0
    ltpbond = 0

    prmbond = 0.0_wp
    vbond   = 0.0_wp
    gbond   = 0.0_wp

end subroutine allocate_bondpot_arrays

!> @brief
!> - reads in, prints out, and converts bond potentials to internal units
!> - also generates the look-up tables (numerical representation on grid)
!> @using 
!> - `kinds_f90`
!> - `parse_module`
!> - `species_module`
!> - `constants_module`
!> - `comms_mpi_module, only : idnode`
subroutine read_bond(shortrangecut, energyunit)

    use kinds_f90
    use parse_module
    use species_module
    use constants_module
    use comms_mpi_module, only : idnode

    implicit none

        !> cutoff distance (for bonded[?] and non-bonded[?] interactions)
    real(kind = wp), intent(in) :: shortrangecut

        !> energy unit conversion factor
    real(kind = wp), intent(in) :: energyunit

    integer :: katom1, katom2, jtpatm, itpbond, j, i, atm1typ, atm2typ
    integer :: keypot, nread

    real(kind = wp) :: parpot(mxpbond), engunit

    character*8, atom1, atom2
    character :: line*100, word*40, keyword*4

    logical :: safe

    nread = ufld
    engunit = energyunit

    if (idnode == 0) write(uout,"(/,/,' bond potentials ',/)")

    do itpbond = 1, ntpbond

        parpot=0.0_wp

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000
        
        call get_word(line,word)
        atom1=word
        call get_word(line,word)
        atm1typ = get_species_type(word)

        call get_word(line,word)
        atom2=word
        call get_word(line,word)
        atm2typ = get_species_type(word)

        call get_word(line,word)
        call lower_case(word)
        keyword=word
        if (keyword == '12-6' .or. keyword == '-12-6') then
            keypot=1
        else if (keyword == 'lj' .or. keyword == '-lj' ) then
            keypot=2
        else if (keyword == 'nm' .or. keyword == '-nm') then
            keypot=3
        else if (keyword == 'buck' .or. keyword == '-buck') then
            keypot=4
        else if (keyword == 'bhm' .or. keyword == '-bhm') then
            keypot=5
        else if (keyword == 'hbnd'.or. keyword == '-hbnd') then
            keypot=6
        else if (keyword == 'snm' .or. keyword == '-snm') then
            keypot=7
        else if (keyword == 'hcnm' .or. keyword == '-hcnm') then
            keypot=7
        else if (keyword == 'mors' .or. keyword == '-mors') then
            keypot=8
        else if (keyword == 'wca' .or. keyword == '-wca') then
            keypot=9
        else if (keyword == 'flj' .or. keyword == '-flj') then
            keypot=10
        else if (keyword == 'fwca' .or. keyword == '-fwca') then
            keypot=11
        else if (keyword == 'fene' .or. keyword == '-fene') then
            keypot=12
        else if (keyword == 'harm' .or. keyword == '-harm') then
            keypot = 13
        else if (keyword == 'tab' ) then
            keypot=0
        else

            call error(115)

        endif

        call get_word(line,word)
        parpot(1)=word_2_real(word)
        call get_word(line,word)
        parpot(2)=word_2_real(word)
        call get_word(line,word)
        parpot(3)=word_2_real(word)
        call get_word(line,word)
        parpot(4)=word_2_real(word)
        call get_word(line,word)
        parpot(5)=word_2_real(word)
        call get_word(line,word)
        parpot(6)=word_2_real(word)

        if (idnode == 0) write(uout,"(1x,2a8,3x,a4,3x,10f15.6)") &
             atom1,atom2,keyword,(parpot(j),j=1,mxpbond)

        katom1=0
        katom2=0

        do jtpatm = 1, number_of_elements
           if (atom1 == element(jtpatm) .and. atm1typ == eletype(jtpatm)) katom1=jtpatm
           if (atom2 == element(jtpatm) .and. atm2typ == eletype(jtpatm)) katom2=jtpatm
        end do

        if (katom1 == 0 .or. katom2 == 0) then

           call error(116)

        endif

        ! store the type of atom for each bond potential- use to add coulomb terms
        lstbond(itpbond, 1) = katom1
        lstbond(itpbond, 2) = katom2

        ! convert energies to internal unit
        parpot(1) = parpot(1)*engunit

        if (keypot == 1) then
            parpot(2)=parpot(2)*engunit
        else if (keypot == 4) then
            parpot(3)=parpot(3)*engunit
        else if (keypot == 5) then
            parpot(4)=parpot(4)*engunit
            parpot(5)=parpot(5)*engunit
        else if (keypot == 6) then
            parpot(2)=parpot(2)*engunit
        else if (keypot == 9) then
            parpot(2)=Abs(parpot(2))
            if (parpot(3) > parpot(2)/2.0_wp) &
                 parpot(3)=Sign(1.0_wp,parpot(3))*parpot(2)/2.0_wp
            parpot(4)=2.0_wp**(1.0_wp/6.0_wp)*parpot(2)+parpot(3)

        else if (keypot == 10) then
            parpot(3) = parpot(3) * engunit
        else if (keypot == 11) then
            parpot(4) = parpot(4) * engunit
        end if

        ltpbond(itpbond) = keypot
        do i = 1, mxpbond
            prmbond(i,itpbond)=parpot(i)
        end do

    end do   ! end loop over potentials

    ! generate non-bonded force arrays
    if (ntpbond > 0 ) then

        call bond_generate(shortrangecut)

    end if

    return

1000 call error(114)                    

end subroutine

!> @brief
!> - converts potentials to look up tables (numerical representation on grid)
!> - also factors in coulomb sum parts
!> @using
!> - `kinds_f90`
!> - `species_module`
!> - `constants_module`
!> - `control_type`
subroutine bond_generate(rbond)

    use kinds_f90
    use constants_module
    use species_module
    use control_type

    implicit None

        !> cutoff distance for bonded interactions (max on table grid)
    real(kind = wp), intent(in) :: rbond

    integer           :: i,itab, ityp, jtyp, ll, itpbond
    real( Kind = wp ) :: gg1,gg2,gg3,gg4,gg5,gg6,gg7,gg8,gg9, &
                         vv1,vv2,vv3,vv4,vv5,vv6,vv7,vv8,vv9, &
                         vv10, gg10, vv11, gg11, vv12, gg12,  &
                         vv13, gg13,                          &
                         r,a,b,c,d,e,f,b1,c1,                 &
                         rrr,r0,am,an,alpha,beta,gamma,eps 

    real( Kind = wp ) :: vk0, vk1, vk2, gk0, gk1, gk2, t1, t2, egamma

    real( Kind = wp ) :: chgea, chgeb, chgprd, coul, rsq, ppp

    ! 12-6 potential

    vv1(r,a,b)=(a/r**6-b)/r**6      
    gg1(r,a,b)=6.0_wp*(2.0_wp*a/r**6-b)/r**6

    ! Lennard-Jones potential

    vv2(r,a,b)=4.0_wp*a*(b/r)**6*((b/r)**6-1.0_wp)
    gg2(r,a,b)=24.0_wp*a*(b/r)**6*(2.0_wp*(b/r)**6-1.0_wp)

    ! n-m potential

    vv3(r,a,b,c,d)=a/(b-c)*(c*(d/r)**b-b*(d/r)**c)
    gg3(r,a,b,c,d)=a*c*b/(b-c)*((d/r)**b-(d/r)**c)

    ! Buckingham exp-6 potential

    vv4(r,a,b,c)=a*exp(-r/b)-c/r**6
    gg4(r,a,b,c)=r*a*exp(-r/b)/b-6.0_wp*c/r**6

    ! Born-Huggins-Meyer exp-6-8 potential

    vv5(r,a,b,c,d,e)=a*exp(b*(c-r))-d/r**6-e/r**8
    gg5(r,a,b,c,d,e)=r*a*b*exp(b*(c-r))-6.0_wp*d/r**6-8.0_wp*e/r**8

    ! Hydrogen-bond 12-10 potential

    vv6(r,a,b) = a/r**12 - b/r**10
    gg6(r,a,b) = 12.0_wp*a/r**12 - 10.0_wp*b/r**10

    ! shifted and force corrected n-m potential (w.smith)

    vv7(r,a,b,c,d,b1,c1)=a/(b-c)*( c*(b1**b)*((d/r)**b-(1.0_wp/c1)**b)    &
                                -b*(b1**c)*((d/r)**c-(1.0_wp/c1)**c)    &
                       +b*c*((r/(c1*d)-1.0_wp)*((b1/c1)**b-(b1/c1)**c)) )
    gg7(r,a,b,c,d,b1,c1)=a*c*b/(b-c)*( (b1**b)*(d/r)**b-(b1**c)*(d/r)**c  &
                       -r/(c1*d)*((b1/c1)**b-(b1/c1)**c) )

    ! Morse potential

    vv8(r,a,b,c)=a*((1.0_wp-exp(-c*(r-b)))**2-1.0_wp)
    gg8(r,a,b,c)=-2.0_wp*r*a*c*(1.0_wp-exp(-c*(r-b)))*exp(-c*(r-b))

    ! Weeks-chandler-Anderson (shifted & truncated Lenard-Jones) (i.t.todorov)

    vv9(r,a,b,c)=4.0_wp*a*(b/(r-c))**6*((b/(r-c))**6-1.0_wp) + a
    gg9(r,a,b,c)=24.0_wp*a*(b/(r-c))**6*(2.0_wp*(b/(r-c))**6-1.0_wp)

    ! Lennard -Jones coupled with FENE
    vv10(r,a,b,c,d,e) = 4.0_wp*a*(b/r)**6*((b/r)**6-1.0_wp) -0.5d0* c * d**2 &
                        *log(1.d0-((r - e )/ d )**2)
    gg10(r,a,b,c,d,e)=24.0_wp*a*(b/r)**6*(2.0_wp*(b/r)**6-1.0_wp) + (1.0 / r) * c *(r - e ) &
               / (1.d0-((r - e )/ d**2))

    ! Weeks-chandler-Anderson coupled with FENE
    vv11(r,a,b,c,d,e,f) = 4.0_wp*a*(b/(r-c))**6*((b/(r-c))**6-1.0_wp) + a -0.5d0* d * e**2 &
                        *log(1.d0-((r - f )/ e )**2)
    gg11(r,a,b,c,d,e,f)= 24.0_wp*a*(b/(r-c))**6*(2.0_wp*(b/(r-c))**6-1.0_wp) + (1.0 / r) * d *(r - f ) &
               / (1.d0-((r - f )/ e**2))

    ! FENE
    vv12(r,a,b,c) = -0.5d0* a * b**2 *log(1.d0-((r - c )/ b )**2)
    gg12(r,a,b,c)= (1.0 / r) * a *(r - c ) / (1.d0-((r - c )/ b**2))

    ! harmonic
    vv13(r,a,b) = 0.5 * a * (r - b)**2
    gg13(r,a,b) = a * (r - b)

    ! define grid resolution for potential arrays

    dlrpot=rbond/real(maxmesh-4,wp)

    call set_bond_grid(rbond)

    ! construct arrays for all types of bond potential

    itab = 0

    vbond = 0.0
    gbond = 0.0

    do itpbond = 1, ntpbond

        ityp = lstbond(itpbond, 1)
        jtyp = lstbond(itpbond, 2)

        itab = itab + 1

        if (ltpbond(itab) == 1) then

            do i=1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv1(rrr,prmbond(1,itab),prmbond(2,itab))
                gbond(i,itab)=gg1(rrr,prmbond(1,itab),prmbond(2,itab))
            end do

        else if (ltpbond(itab) == 2) then

            do i=1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv2(rrr,prmbond(1,itab),prmbond(2,itab))
                gbond(i,itab)=gg2(rrr,prmbond(1,itab),prmbond(2,itab))
            end do

        else if (ltpbond(itab) == 3) then

            do i=1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv3(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab),prmbond(4,itab))
                gbond(i,itab)=gg3(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab),prmbond(4,itab))
            end do

        else if (ltpbond(itab) == 4) then

            do i=1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv4(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab))
                gbond(i,itab)=gg4(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab))
                      
            end do

        else if (ltpbond(itab) == 5) then

            do i=1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv5(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab),prmbond(4,itab),prmbond(5,itab))
                gbond(i,itab)=gg5(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab),prmbond(4,itab),prmbond(5,itab))
            end do

        else if (ltpbond(itab) == 6) then

            do i = 1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv6(rrr,prmbond(1,itab),prmbond(2,itab))
                gbond(i,itab)=gg6(rrr,prmbond(1,itab),prmbond(2,itab))
            end do

        else if (ltpbond(itab) == 7) then

            r0 = prmbond(4,itab)
            an = prmbond(2,itab)
            am = prmbond(3,itab)
            eps = prmbond(1,itab)
 
            if (an <= am) write(uout, *)' par 2 less than par 3 '

            gamma = rbond/r0
            if (gamma < 1.0_wp) then
                write(uout, *) 'r0 > cutoff'
                stop
            endif

            beta = gamma*((gamma**(am+1.0_wp)-1.0_wp) /                       &
                 (gamma**(an+1.0_wp)-1.0_wp))**(1.0_wp/(an-am))
                   alpha= -(an-am) /                                                 &
                   ( am*(beta**an)*(1.0_wp+(an/gamma-an-1.0_wp)/gamma**an)   &
                   - an*(beta**am)*(1.0_wp+(am/gamma-am-1.0_wp)/gamma**am) )
                   eps = eps*alpha

            do i = 1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv7(rrr,eps,an,am,r0,beta,gamma)
                gbond(i,itab)=gg7(rrr,eps,an,am,r0,beta,gamma)
            end do

        else if (ltpbond(itab) == 8) then

            do i = 1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv8(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab))
                gbond(i,itab)=gg8(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab))
            end do

        else if (ltpbond(itab) == 9) then

            do i=1,maxmesh
                rrr=real(i,wp)*dlrpot
                if (rrr < prmbond(4,itab)) then ! else leave them zeros
                    vbond(i,itab)=vv9(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab))
                    gbond(i,itab)=gg9(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab))
                end if
            end do

        else if (ltpbond(itab) == 10) then

            do i=1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv10(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab), &
                          prmbond(4,itab),prmbond(5,itab))
                gbond(i,itab)=gg10(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab), &
                          prmbond(4,itab),prmbond(5,itab))

            enddo

        else if (ltpbond(itab) == 11) then

            do i=1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv11(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab), &
                          prmbond(4,itab),prmbond(5,itab),prmbond(6,itab))
                gbond(i,itab)=gg11(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab), &
                          prmbond(4,itab),prmbond(5,itab),prmbond(6,itab))

            enddo

       else if (ltpbond(itab) == 12) then

            do i = 1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv12(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab))
                gbond(i,itab)=gg12(rrr,prmbond(1,itab),prmbond(2,itab),prmbond(3,itab))

            end do

       else if (ltpbond(itab) == 13) then

            do i = 1,maxmesh
                rrr=real(i,wp)*dlrpot
                vbond(i,itab)=vv13(rrr,prmbond(1,itab),prmbond(2,itab))
                gbond(i,itab)=gg13(rrr,prmbond(1,itab),prmbond(2,itab))
            enddo

       end if

   enddo

end Subroutine bond_generate

!> @brief
!> - sets up the grid for bond potential look-up table(s) 
!> @using 
!> - `kinds_f90`
!> - `constants_module, only : maxmesh`
subroutine set_bond_grid(rbond)

   use kinds_f90
   use constants_module, only : maxmesh

   implicit none

   real(kind=wp), intent(in) :: rbond

   ! define grid resolution for potential arrays and interpolation spacing

   dlrpot = rbond/real(maxmesh-4,wp)
   rdr    = 1.0_wp/dlrpot

end subroutine set_bond_grid

!> @brief
!> - calculates bond energy and force terms using Verlet neighbour list
!> - originates from `dl_poly_3`
!> @using 
!> - `kinds_f90`
subroutine bond_energy (pot, rrr, rsq, engbond, gamma)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine for calculating bond energy and force terms using
! verlet neighbour list
!
! copyright - daresbury laboratory
! author    - w.smith august 1998
! amended   - i.t.todorov september 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  use kinds_f90

  implicit None

      !> potential type identifier
  integer, intent(in)                                       :: pot

      !> bond length (atom separation) and its square
  real( Kind = wp ),                        intent( in    ) :: rrr,rsq

      !> calculated bond energy
  real( Kind = wp ),                        intent(   Out ) :: engbond

      !> calculated bond force
  real( Kind = wp ),                        intent(   Out ) :: gamma

  integer           :: l, k
  real( Kind = wp ) :: ppp,gk,gk1,gk2,vk,vk1,vk2,t1,t2


! atomic and potential function indices

  if(ntpbond == 0 .or. pot == 0) return

  k = abs(pot)

  if (vbond(1,k) /= 0.0_wp) then

     l   = int(rrr*rdr)
     ppp = rrr*rdr - real(l,wp)

! calculate forces using 3-point interpolation

     gk  = gbond(l,k)
     gk1 = gbond(l+1,k)
     gk2 = gbond(l+2,k)

     t1 = gk  + (gk1 - gk )*ppp
     t2 = gk1 + (gk2 - gk1)*(ppp - 1.0_wp)

     gamma = (t1 + (t2-t1)*ppp*0.5_wp) / rsq

!       if (jatm <= natms(icell) .or. idi < ltg(jatm,icell)) then

! calculate interaction energy using 3-point interpolation

     vk  = vbond(l,k)
     vk1 = vbond(l+1,k)
     vk2 = vbond(l+2,k)

     t1 = vk  + (vk1 - vk )*ppp
     t2 = vk1 + (vk2 - vk1)*(ppp - 1.0_wp)

     engbond = (t1 + (t2-t1)*ppp*0.5_wp)

!       end if

  end if

end subroutine bond_energy

end module
