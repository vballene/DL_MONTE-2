! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Angle storage, manipulation and energy calculation
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @modulefor angles in a molecule

module angle_module

    use kinds_f90

    implicit none

        !> total number of angles allowed
    integer :: numang

        !> @auxarray for potential types
    integer, allocatable, dimension(:) :: ltpang

        !> array for angle potential parameters
    real(kind = wp), allocatable, dimension(:,:) :: prmang

contains

!> @brief
!> - allocates arrays for potential parameters
!> @using 
!> - `kinds_f90`
!> - `constants_module, only : uout`
subroutine alloc_angpar_arrays(npar)

    use kinds_f90
    use constants_module, only : uout

    implicit none

        !> total number of angle parameter pairs
    integer, intent(in) :: npar

    integer :: fail(2)

    fail = 0
    numang = npar

    allocate(prmang(npar, 6), stat = fail(1))
    allocate(ltpang(npar), stat = fail(2))

    if(any(fail > 0)) then

        call error(128)

    endif

    prmang = 0.0_wp
    ltpang = 0

end subroutine alloc_angpar_arrays

!> @brief
!> - reads in, prints out, and converts angle potentials to internal units
!> @using 
!> - `kinds_f90`
!> - `parse_module`
!> - `comms_mpi_module, only : master`
!> - `constants_module, only : uout, ufld, TORADIANS`
subroutine read_angle_par(npar, unit)

    use kinds_f90
    use constants_module, only : uout, ufld, TORADIANS
    use parse_module
    use comms_mpi_module, only : master !,idnode

    implicit none

        !> total number of angle parameter pairs
    integer, intent(in) :: npar

        !> energy unit conversion factor
    real(kind = wp), intent(in) :: unit

        ! FIELD file unit, to read the parameters from (assumed open already!)
    integer :: nread

        ! auxiliary index
    integer :: i

        ! force constant and reference (cos)angle
    real(kind = wp) :: fc, cz, a, b, c, rho1, rho2

        ! strings for `record` and `word` for parsing
    character :: line*100, word*40

    logical safe

    safe = .true.

    nread = ufld

    !if(idnode == 0) write(uout,"(/,/,' angle potentials ',/)")

    if( master ) write(uout,"(/,/,20('-'),/,' angle potentials: ',/,20('-'))")

    do i = 1, npar

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        call get_word(line, word)
        if( word == 'hcos' .or. word == '-hcos' ) then

            ltpang(i) = 1
            if( word(1:1) == '-' ) ltpang(i) = -1

            call get_word(line,word)
            fc = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'harmonic cosine valence angle potential')")
                write(uout,"(1x,'force constant ', f10.5, ' cos zero ', f10.5)")fc, cz

            endif

            ! convert force constant to internal units 
            fc = fc * unit 
            cz = cz * TORADIANS
            cz = cos(cz)

            prmang(i,1) = fc
            prmang(i,2) = cz

        elseif( word == 'harm' .or. word == '-harm' ) then

            ltpang(i) = 2
            if( word(1:1) == '-' ) ltpang(i) = -2

            call get_word(line,word)
            fc = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'harmonic valence angle potential')")
                write(uout,"(1x,'force constant ', f10.5, ' theta zero ', f10.5)")fc, cz

            endif

            ! convert force constant to internal units
            fc = fc * unit
            cz = cz * TORADIANS

            prmang(i,1) = fc
            prmang(i,2) = cz

        elseif( word == 'quar' .or. word == '-quar' ) then

            ltpang(i) = 3
            if( word(1:1) == '-' ) ltpang(i) = -3

            call get_word(line,word)
            a = word_2_real(word)
            call get_word(line,word)
            b = word_2_real(word)
            call get_word(line,word)
            c = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'quartic valence angle potential')")
                write(uout,"(1x,'force constants ', 3f10.5, /,1x, ' theta zero ', f10.5)") &
                      a, b, c, cz

            endif

            ! convert force constant to internal units
            a = a * unit
            b = b * unit
            c = c * unit
            cz = cz * TORADIANS

            prmang(i,1) = a / 2.0_wp ! do some prep conversions on force constants
            prmang(i,2) = b / 3.0_wp
            prmang(i,3) = c / 4.0_wp
            prmang(i,4) = cz

        elseif( word == 'cos' .or. word == '-cos' ) then

            !TU: In the manual there is a parameter 'm' in the functional form, which is not read
            !TU: here and is set to 1. Need to update the manual, or bring this in alignment with DL_POLY's
            !TU: behaviour

            ltpang(i) = 4
            if( word(1:1) == '-' ) ltpang(i) = -4

            call get_word(line,word)
            fc = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'cosine valence angle potential')")
                write(uout,"(1x,'force constant ', f10.5, ' cos zero ', f10.5)")fc, cz

            endif

            ! convert force constant to internal units
            fc = fc * unit
            cz = cz * TORADIANS
            cz = cos(cz)

            prmang(i,1) = fc
            prmang(i,2) = cz

        elseif( word == 'thrm' .or. word == '-thrm' ) then

            ltpang(i) = 5
            if( word(1:1) == '-' ) ltpang(i) = -5

            call get_word(line,word)
            fc = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)
            call get_word(line,word)
            rho1 = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'truncated harmonic angle potential')")
                write(uout,"(1x,'force constant ', f10.5, ' theta_0 ', f10.5, ' rho ', f10.5)") &
                      fc, cz, rho1

            endif

            ! convert force constant to internal units
            fc = fc * unit
            cz = cz * TORADIANS

            prmang(i,1) = fc * 0.5_wp
            prmang(i,2) = cz
            prmang(i,3) = rho1**8

        elseif( word == 'shrm' .or. word == '-shrm' ) then

            ltpang(i) = 6
            if( word(1:1) == '-' ) ltpang(i) = -6

            call get_word(line,word)
            fc = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)
            call get_word(line,word)
            rho1 = word_2_real(word)
            call get_word(line,word)
            rho2 = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'screened harmonic angle potential')")
                write(uout,"(1x,'force constant ', f10.5, ' theta_0 ', f10.5, &
                     &' rho1 ', f10.5, ' rho2 ', f10.5)") fc, cz, rho1, rho2

            endif

            ! convert force constant to internal units
            fc = fc * unit
            cz = cz * TORADIANS

            prmang(i,1) = fc * 0.5_wp
            prmang(i,2) = cz
            prmang(i,3) = rho1
            prmang(i,4) = rho2

        elseif( word == 'mmsb' .or. word == '-mmsb' ) then

            ltpang(i) = 7
            if( word(1:1) == '-' ) ltpang(i) = -7

            call get_word(line,word)
            fc = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)
            call get_word(line,word)
            rho1 = word_2_real(word)
            call get_word(line,word)
            rho2 = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'MM3 stretch bend angle potential')")
                write(uout,"(1x,'force constant ', f10.5, ' theta_0 ', f10.5, &
                     &' rij_0 ', f10.5, ' rik_0 ', f10.5)") fc, cz, rho1, rho2

            endif

            ! convert force constant to internal units
            fc = fc * unit
            cz = cz * TORADIANS

            prmang(i,1) = fc 
            prmang(i,2) = cz
            prmang(i,3) = rho1
            prmang(i,4) = rho2

        elseif( word == 'stst' .or. word == '-stst' ) then

            ltpang(i) = 8
            if( word(1:1) == '-' ) ltpang(i) = -8

            call get_word(line,word)
            fc = word_2_real(word)
            call get_word(line,word)
            rho1 = word_2_real(word)
            call get_word(line,word)
            rho2 = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'Compass stretch stretch angle potential')")
                write(uout,"(1x,'force constant ', f10.5, ' rij_0 ', f10.5, &
                     &' rik_0 ', f10.5)") fc, rho1, rho2

            endif

            ! convert force constant to internal units
            fc = fc * unit

            prmang(i,1) = fc
            prmang(i,2) = rho1
            prmang(i,3) = rho2

        elseif( word == 'stbe' .or. word == '-stbe' ) then

            ltpang(i) = 9
            if( word(1:1) == '-' ) ltpang(i) = -9

            call get_word(line,word)
            fc = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)
            call get_word(line,word)
            rho1 = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'Compass stretch bend angle potential')")
                write(uout,"(1x,'force constant ', f10.5, ' theta_0 ', f10.5, &
                     &' rij_0 ', f10.5)") fc, cz, rho1

            endif

            ! convert force constant to internal units
            fc = fc * unit
            cz = cz * TORADIANS

            prmang(i,1) = fc
            prmang(i,2) = cz
            prmang(i,3) = rho1

        elseif( word == 'cmps' .or. word == '-cmps' ) then

            ltpang(i) = 10
            if( word(1:1) == '-' ) ltpang(i) = -10

            !TU: Note that the 3 force constants are read for this first, followed by theta0.
            !TU: This is the same order as DL_POLY Classic

            call get_word(line,word)
            a = word_2_real(word)
            call get_word(line,word)
            b = word_2_real(word)
            call get_word(line,word)
            c = word_2_real(word)
            call get_word(line,word)
            cz = word_2_real(word)
            call get_word(line,word)
            rho1 = word_2_real(word)
            call get_word(line,word)
            rho2 = word_2_real(word)

            if( master ) then

                write(uout,"(1x,'Compass angle potential all terms')")
                write(uout,"(1x,'force constants: a ', f6.3, ' b ', f6.3, ' c ', f6.3)") a, b, c
                write(uout,"(1x,'constraints theta_0', f10.5, ' rij_0 ', f10.5, &
                     &' rik_0 ', f10.5)") cz, rho1, rho2

            endif

            ! convert force constant to internal units
            a = a * unit
            b = b * unit
            c = c * unit
            cz = cz * TORADIANS

            prmang(i,1) = a
            prmang(i,2) = b
            prmang(i,3) = c
            prmang(i,4) = cz
            prmang(i,5) = rho1
            prmang(i,6) = rho2

        else

            call error(140)

        endif

    enddo

    return

1000 call error(130)

end subroutine

!> @brief
!> - calculates the energy for an angle triplet 
!> @using 
!> - `kinds_f90`
subroutine calc_angle_energy(pot, rij, rik, theta, cos_theta, eng)

    use kinds_f90
    use constants_module, only : uout

        !> potential type identifier
    integer, intent(in) :: pot

        !> distances of bonds
    real(kind = wp), intent(in) :: rij, rik

        !> angle in radians
    real(kind = wp), intent(in) :: theta, cos_theta

        !> energy in internal units (calculated)
    real(kind = wp), intent(out) :: eng

        ! force constant, reference (cos)angle, actual (cos)angle
    real(kind = wp) :: fc, cz, costh, f1, f2, f3, dtheta, rho1, rho2
    real(kind = wp) :: dr1, dr2, scale, a, b, c

    integer :: typ
    
    typ = abs(ltpang(pot))

    eng = 0.0_wp

    !select case(ltpang(pot))
    select case(typ)

        case(1)

            ! three-body harmonic cos(angle) energy
            fc = prmang(pot, 1)
            cz = prmang(pot, 2)
            !costh = cos(theta)
            costh = cos_theta
            eng = 0.5_wp * fc * (costh - cz)**2

            !write(uout,"(1x,'force constant ', e15.7, ' cos zero ', f10.5)")fc, cz

        case(2)

            ! three-body angle energy
            fc = prmang(pot, 1)
            cz = prmang(pot, 2)
            eng = 0.5_wp * fc * (theta - cz)**2

        case(3)

            ! quartic - force constants already divided by appropriate integer
            f1 = prmang(pot, 1)
            f2 = prmang(pot, 2)
            f3 = prmang(pot, 3)
            cz = prmang(pot, 4)
            dtheta = theta - cz
            eng = f1 * dtheta**2 + f2 * dtheta**3 + dtheta**4

        case(4)

            ! three-body cos(angle) energy
            fc = prmang(pot, 1)
            cz = prmang(pot, 2)
            costh = cos(prmang(pot, 3) * theta - cz)
            eng = fc * (1.0_wp + costh)

        case(5)

            ! truncated Harmonic potential

            fc = prmang(pot,1)
            cz = prmang(pot,2)
            rho1 = prmang(pot,3)
            dtheta = theta - cz
            scale = ((rij**8 + rik**8) / rho1)
            eng = fc * dtheta * dtheta * exp(-scale)

        case(6)

            ! screened Harmonic potential
            fc = prmang(pot,1)
            cz = prmang(pot,2)
            rho1 = prmang(pot,3)
            rho2 = prmang(pot,4)
            dtheta = theta - cz
            scale = rij / rho1 + rik / rho2
            eng = fc * dtheta * dtheta * exp(-scale)

        case(7)

            ! MM3 stretch-bend potential

            fc = prmang(pot,1)
            cz = prmang(pot,2)
            rho1 = prmang(pot,3)
            rho2 = prmang(pot,4)
            dtheta = theta - cz
            dr1 = rij - rho1
            dr2 = rik - rho2
            eng = fc * dtheta * dr1 * dr2

        case(8)

            ! compass stretch-stretch potential
            fc = prmang(pot,1)
            rho1 = prmang(pot,2)
            rho2 = prmang(pot,3)

            dr1 = rij - rho1
            dr2 = rik - rho2
            eng = fc * dr1 * dr2

        case(9)

            ! compass stretch-bend potential
            fc = prmang(pot,1)
            cz = prmang(pot,2)
            rho1 = prmang(pot,3)
            dtheta = theta - cz
            dr1 = rij - rho1
            eng = fc * dtheta * dr1

        case(10)

            ! combined compass angle potential with 3 coupling terms
            a = prmang(pot,1)
            b = prmang(pot,2)
            c = prmang(pot,3)
            cz = prmang(pot,4)
            rho1 = prmang(pot,5)
            rho2 = prmang(pot,6)
            dtheta = theta - cz
            dr1 = rij - rho1
            dr2 = rik - rho2
            eng = a * dr1 * dr2 + dtheta * (b * dr1 + c * dr2)

        case(11)

            ! AMOEBA potential
            fc = prmang(pot,1)
            cz = prmang(pot,2)
            dtheta = theta - cz

            eng = fc * dtheta**2 * (1.0_wp - 1.4e-2_wp * dtheta + 5.60e-5_wp * dtheta**2 - &
                                7.0e-7_wp * dtheta**3 + 2.20e-8_wp * dtheta**4)

    end select

end subroutine

end module
