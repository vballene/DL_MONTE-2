! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module dihlist_module

    use kinds_f90

    implicit none

contains

subroutine alloc_dihlist_arrays(list, n)

    use dihlist_type

    implicit none
    type(dihlist), intent(inout) :: list
    integer , intent(in) :: n

    list%ndih = n

    allocate (list%dipair(n,5))
    allocate (list%diexc(n))

    list%diexc = .true.

end subroutine

subroutine add_dih_quads(list, atomi, atomj, atomk, atoml, pot, n)

    use dihlist_type

    implicit none
    type(dihlist), intent(inout) :: list
    integer , intent(in) :: n
    integer , intent(in) :: atomi(n), atomj(n), atomk(n), atoml(n), pot(n)
    integer :: i

    do i = 1, n

        list%dipair(i,1) = atomi(i)
        list%dipair(i,2) = atomj(i)
        list%dipair(i,3) = atomk(i)
        list%dipair(i,4) = atoml(i)
        list%dipair(i,5) = pot(i)

    enddo

end subroutine

subroutine alloc_and_copy_dihlist_arrays(nlist, olist)

    use dihlist_type

    implicit none
    type(dihlist), intent(in) :: olist  ! old bond list
    type(dihlist), intent(inout) :: nlist  ! new bond list

    integer :: i, fail(2)

    if (olist%ndih == 0) return

    fail = 0
    if(allocated(nlist%dipair)) deallocate (nlist%dipair, stat = fail(1))
    if(allocated(nlist%diexc)) deallocate (nlist%diexc, stat = fail(2))

    if ( any( fail  > 0) ) call error(342)

    nlist%ndih = olist%ndih

    fail = 0
    allocate (nlist%dipair(nlist%ndih,5), stat = fail(1))
    allocate (nlist%diexc(nlist%ndih), stat = fail(2))

    if ( any ( fail > 0) ) call error(341)

    do i = 1, nlist%ndih

        nlist%dipair(i,:) = olist%dipair(i,:)

    enddo

    nlist%diexc(:) = olist%diexc(:)

end subroutine

subroutine copy_dihlist(nlist, olist)

    use dihlist_type

    implicit none
    type(dihlist), intent(in) :: olist  ! old bond list
    type(dihlist), intent(inout) :: nlist  ! new bond list

    integer :: i

    if (olist%ndih == 0) return

    nlist%ndih = olist%ndih


    do i = 1, nlist%ndih

        nlist%dipair(i,:) = olist%dipair(i,:)

    enddo

    nlist%diexc(:) = olist%diexc(:)

end subroutine

subroutine print_dihlist(list)

    use constants_module, only : uout
    use dihlist_type
    use comms_mpi_module, only : idnode

    implicit none
    type(dihlist), intent(inout) :: list

    integer :: i

    if (idnode /= 0) return

    do i = 1, list%ndih

        write(uout,"(/,1x,'dihedral (i,j,k,l) between atoms ',4i5,' with potential ',i5)")list%dipair(i,:)

    enddo

end subroutine

end module
