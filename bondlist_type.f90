! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief 
!> - Data type defining a bond list within a molecule
!> @usage 
!> - call as `<bond_list_container>%<attribute>`
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @typefor a bond list

module bondlist_type

    use kinds_f90

    implicit none

type bondlist

       !> the number of bonds within a molecule
    integer :: npairs = 0

       !> bpair(nang,3) stores pairs of connected atoms (indices/pointers?) and potential number (type?)
    integer, dimension(:,:), allocatable :: bpair

end type

end module
