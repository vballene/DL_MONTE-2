! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module random_module

  use kinds_f90

  implicit none
  
  logical :: lseeds ! if true then seeds are provided
  integer, dimension(4) :: seeds_ijkl
  
contains

  subroutine init_duni(job)
    
    use control_type
    use parallel_loop_module, only : idgrp
    
    implicit none
    
    type(control), intent(in) :: job
    
    lseeds = job % lseeds
    seeds_ijkl = job % seeds_ijkl

!   if (job%repexch) seeds_ijkl = seeds_ijkl + idgrp

  end subroutine init_duni

  subroutine gauss_ranf(gauss)

    ! Generates a pseudo random number with a Gaussian distribution
    ! using the Central Limit Theorem. Simple but quick way to 
    ! generate a Gaussian random variable with mean=0 and unit variance
    !
    ! Stephen Cox 24/05/2011

    implicit none

    real(wp), intent(out) :: gauss

    integer, parameter :: n=100
    integer :: i
    real(wp) :: nreal

    nreal = real(n,kind=wp) 

    gauss = 0.0_wp

    do i = 1,n
       gauss = gauss + duni()
    end do

    gauss = gauss / nreal
    gauss = gauss - 0.5_wp
    gauss = gauss*sqrt(nreal*12.0_wp)

  end subroutine gauss_ranf

  Function duni()
    
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 random number generator based on the universal random number
! generator of marsaglia, zaman and tsang
! (stats and prob. lett. 8 (1990) 35-39.)
!
! It must be called once to initialise parameters u,c,cd,cm
!
! copyright - daresbury laboratory
! author    - w.smith july 1992
! amended   - i.t.todorov august 2004
!
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    Use kinds_f90

    !sjc: giving seed call to system clock values this requires one node
    !     making the selection and broadcasting to all other nodes
    
    use control_type
    
    Implicit None
    
    Logical,           Save :: newjob
    Data                       newjob /.true./
    Integer,           Save :: ir,jr
    Integer                 :: i,ii,j,jj,k,l,m
    Real( Kind = wp ), Save :: c,cd,cm,u(1:97)
    Real( Kind = wp )       :: s,t,duni
    
    !sjc: 
    integer :: time
    integer, dimension(4) :: seed
    
    time = 0
    seed = 0
    
    ! initialise parameters u,c,cd,cm
    
    If (newjob) Then
       newjob = .false.
       
       ! initial values of i,j,k must be in range 1 to 178 (not all 1)
       ! initial value of l must be in range 0 to 168
       
          
       i = seeds_ijkl(1)
       j = seeds_ijkl(2)
       k = seeds_ijkl(3)
       l = seeds_ijkl(4)
          
       ir = 97
       jr = 33
       
       Do ii=1,97
          
          s = 0.0_wp
          t = 0.5_wp
          
          Do jj=1,24
             
             m = Mod(Mod(i*j,179)*k,179)
             i = j
             j = k
             k = m
             l = Mod(53*l+1,169)
             If (Mod(l*m,64) >= 32) s = s+t
             t = 0.5_wp*t
             
          End Do
          
          u(ii)=s
          
       End Do
       
       c  =   362436.0_wp/16777216.0_wp
       cd =  7654321.0_wp/16777216.0_wp
       cm = 16777213.0_wp/16777216.0_wp
       
    End If
    
  ! calculate random number
    
    duni=u(ir)-u(jr)
    If (duni < 0.0_wp) duni = duni + 1.0_wp
    
    u(ir)=duni
    
    ir=ir-1
    If (ir == 0) ir = 97
    
    jr=jr-1
    If (jr == 0) jr = 97
    
    c = c-cd
    If (c < 0.0_wp) c = c+cm
    
    duni = duni-c
    
    If (duni < 0.0_wp) duni = duni + 1.0_wp
    
  End Function duni
  
end module random_module
