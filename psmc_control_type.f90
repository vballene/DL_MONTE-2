!***************************************************************************
!   Copyright (C) 2015 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Data type containing control parameters for phase-switch Monte Carlo (PSMC) calculations
!> @usage 
!> - @stdspecs
!> @using
!> - `kinds_f90`
!> @typecontaining phase-switch Monte Carlo control parameters

module psmc_control_type

    use kinds_f90

    implicit none


type psmc_control

        !> Frequency of phase-switch moves
    integer :: switch_freq = 0

        !> Phase which is initially active (1 or 2) 
    integer :: init_act = 1

        !> Frequency of output to PSMC data file
    integer :: data_freq = 1000
    
        !> Flag determining whether checks on the displacements will be performed
    logical :: melt_check = .false.

        !> Threshold displacement size (Angstroms) at which a displacement is considered to be 'too large',
        !> returning an error in the simulation.
    real(kind=wp) :: melt_thresh = 10
    
        !> Frequency of displacement checks
    integer :: melt_freq = 1000

        !> Hard-core radius for soft VdW potentials used in PSMC, where here 'soft VdW potentials' excludes VdW 
        !> potentials with 'hard parts', e.g. the hard-sphere potential, Yukawa potential, A-O potential. 
        !> In DL_MONTE, the VdW energy for two atoms interacting by such a potential is set to a 'very large'
        !> value if the particles are within the 'hard-core radius', as opposed to calculating the energy using
        !> the relevant formula for the VdW potential. This is to stop overflows in the energy. PSMC simulations
        !> require a different, smaller hard-core radius than would typically be used in non-PSMC simulations.
        !> This is the default for PSMC simulations. (Angstroms)
    real(kind=wp) :: hc_vdw = 0.5


end type psmc_control


end module psmc_control_type
